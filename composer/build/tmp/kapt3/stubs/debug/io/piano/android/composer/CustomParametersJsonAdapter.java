package io.piano.android.composer;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 \u00152\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0015B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\n\u001a\u00020\u00022\u0006\u0010\u000b\u001a\u00020\fH\u0016J\u001a\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\b\u0010\u0011\u001a\u0004\u0018\u00010\u0002H\u0016J6\u0010\u0012\u001a\u0004\u0018\u00010\u000e*\u0014\u0012\u0004\u0012\u00020\b\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\t0\u00072\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0013\u001a\u00020\bH\u0082\b\u00a2\u0006\u0002\u0010\u0014R&\u0010\u0006\u001a\u001a\u0012\u0016\u0012\u0014\u0012\u0004\u0012\u00020\b\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\t0\u00070\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"}, d2 = {"Lio/piano/android/composer/CustomParametersJsonAdapter;", "Lcom/squareup/moshi/JsonAdapter;", "Lio/piano/android/composer/model/CustomParameters;", "moshi", "Lcom/squareup/moshi/Moshi;", "(Lcom/squareup/moshi/Moshi;)V", "mapAdapter", "", "", "", "fromJson", "reader", "Lcom/squareup/moshi/JsonReader;", "toJson", "", "writer", "Lcom/squareup/moshi/JsonWriter;", "value", "writeIfNotEmpty", "name", "(Ljava/util/Map;Lcom/squareup/moshi/JsonWriter;Ljava/lang/String;)Lkotlin/Unit;", "Companion", "composer_debug"})
public final class CustomParametersJsonAdapter extends com.squareup.moshi.JsonAdapter<io.piano.android.composer.model.CustomParameters> {
    private final com.squareup.moshi.JsonAdapter<java.util.Map<java.lang.String, java.util.List<java.lang.String>>> mapAdapter = null;
    @org.jetbrains.annotations.NotNull()
    private static final com.squareup.moshi.JsonAdapter.Factory FACTORY = null;
    public static final io.piano.android.composer.CustomParametersJsonAdapter.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public io.piano.android.composer.model.CustomParameters fromJson(@org.jetbrains.annotations.NotNull()
    com.squareup.moshi.JsonReader reader) {
        return null;
    }
    
    @java.lang.Override()
    public void toJson(@org.jetbrains.annotations.NotNull()
    com.squareup.moshi.JsonWriter writer, @org.jetbrains.annotations.Nullable()
    io.piano.android.composer.model.CustomParameters value) {
    }
    
    @kotlin.Suppress(names = {"NOTHING_TO_INLINE"})
    private final kotlin.Unit writeIfNotEmpty(java.util.Map<java.lang.String, ? extends java.util.List<java.lang.String>> $this$writeIfNotEmpty, com.squareup.moshi.JsonWriter writer, java.lang.String name) {
        return null;
    }
    
    public CustomParametersJsonAdapter(@org.jetbrains.annotations.NotNull()
    com.squareup.moshi.Moshi moshi) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lio/piano/android/composer/CustomParametersJsonAdapter$Companion;", "", "()V", "FACTORY", "Lcom/squareup/moshi/JsonAdapter$Factory;", "getFACTORY", "()Lcom/squareup/moshi/JsonAdapter$Factory;", "composer_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final com.squareup.moshi.JsonAdapter.Factory getFACTORY() {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}