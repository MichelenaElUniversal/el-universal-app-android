package io.piano.android.composer.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0007\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0005R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0004\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lio/piano/android/composer/model/EventModuleParams;", "", "moduleId", "", "moduleName", "(Ljava/lang/String;Ljava/lang/String;)V", "composer_debug"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class EventModuleParams {
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String moduleId = null;
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String moduleName = null;
    
    public EventModuleParams(@org.jetbrains.annotations.NotNull()
    java.lang.String moduleId, @org.jetbrains.annotations.NotNull()
    java.lang.String moduleName) {
        super();
    }
}