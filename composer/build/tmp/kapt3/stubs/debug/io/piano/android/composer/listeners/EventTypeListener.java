package io.piano.android.composer.listeners;

import java.lang.System;

/**
 * Generic event listener
 *
 * @param <T> event type
 * </T>
 */
@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\bf\u0018\u0000*\b\b\u0000\u0010\u0001*\u00020\u00022\u00020\u0003J\u0016\u0010\u0004\u001a\u00020\u00052\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00020\u0007H\u0017J\u0016\u0010\b\u001a\u00020\t2\f\u0010\u0006\u001a\b\u0012\u0004\u0012\u00028\u00000\u0007H&\u00f8\u0001\u0000\u0082\u0002\u0007\n\u0005\b\u0091(0\u0001\u00a8\u0006\n"}, d2 = {"Lio/piano/android/composer/listeners/EventTypeListener;", "T", "Lio/piano/android/composer/model/events/EventType;", "", "canProcess", "", "event", "Lio/piano/android/composer/model/Event;", "onExecuted", "", "composer_debug"})
public abstract interface EventTypeListener<T extends io.piano.android.composer.model.events.EventType> {
    
    /**
     * Processes event
     *
     * @param event event object
     */
    public abstract void onExecuted(@org.jetbrains.annotations.NotNull()
    io.piano.android.composer.model.Event<T> event);
    
    /**
     * Asks is this event type supported by listener
     *
     * @param event event
     * @return True, if this listener can process this type of events, otherwise False. Default answer is False
     */
    public boolean canProcess(@org.jetbrains.annotations.NotNull()
    io.piano.android.composer.model.Event<io.piano.android.composer.model.events.EventType> event) {
        return false;
    }
}