package io.piano.android.composer.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\b\u0007\u0018\u00002\u00020\u0001B-\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0005\u0012\u0006\u0010\u0007\u001a\u00020\u0005\u0012\u0006\u0010\b\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\tR\u0010\u0010\u0007\u001a\u00020\u00058\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u00020\u00058\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0004\u001a\u00020\u00058\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u00020\u00058\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"}, d2 = {"Lio/piano/android/composer/model/ActiveMeter;", "", "meterName", "", "views", "", "viewsLeft", "maxViews", "totalViews", "(Ljava/lang/String;IIII)V", "composer_debug"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class ActiveMeter {
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String meterName = null;
    public final int views = 0;
    public final int viewsLeft = 0;
    public final int maxViews = 0;
    public final int totalViews = 0;
    
    public ActiveMeter(@org.jetbrains.annotations.NotNull()
    java.lang.String meterName, int views, int viewsLeft, int maxViews, int totalViews) {
        super();
    }
}