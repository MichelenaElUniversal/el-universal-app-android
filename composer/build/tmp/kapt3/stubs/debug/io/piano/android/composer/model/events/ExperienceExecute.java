package io.piano.android.composer.model.events;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0087\b\u0018\u00002\u00020\u0001B\u000f\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0004J\u000b\u0010\u0005\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u0015\u0010\u0006\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001J\u0013\u0010\u0007\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\nH\u00d6\u0003J\t\u0010\u000b\u001a\u00020\fH\u00d6\u0001J\t\u0010\r\u001a\u00020\u000eH\u00d6\u0001R\u0012\u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"}, d2 = {"Lio/piano/android/composer/model/events/ExperienceExecute;", "Lio/piano/android/composer/model/events/EventType;", "user", "Lio/piano/android/composer/model/User;", "(Lio/piano/android/composer/model/User;)V", "component1", "copy", "equals", "", "other", "", "hashCode", "", "toString", "", "composer_debug"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class ExperienceExecute extends io.piano.android.composer.model.events.EventType {
    @org.jetbrains.annotations.Nullable()
    public final io.piano.android.composer.model.User user = null;
    
    public ExperienceExecute(@org.jetbrains.annotations.Nullable()
    io.piano.android.composer.model.User user) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final io.piano.android.composer.model.User component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final io.piano.android.composer.model.events.ExperienceExecute copy(@org.jetbrains.annotations.Nullable()
    io.piano.android.composer.model.User user) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object p0) {
        return false;
    }
}