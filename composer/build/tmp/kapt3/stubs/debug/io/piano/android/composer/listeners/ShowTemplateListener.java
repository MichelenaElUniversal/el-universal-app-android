package io.piano.android.composer.listeners;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u00e6\u0080\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001J\u0016\u0010\u0003\u001a\u00020\u00042\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u0006H\u0017\u00f8\u0001\u0000\u0082\u0002\u0007\n\u0005\b\u0091(0\u0001\u00a8\u0006\b"}, d2 = {"Lio/piano/android/composer/listeners/ShowTemplateListener;", "Lio/piano/android/composer/listeners/EventTypeListener;", "Lio/piano/android/composer/model/events/ShowTemplate;", "canProcess", "", "event", "Lio/piano/android/composer/model/Event;", "Lio/piano/android/composer/model/events/EventType;", "composer_debug"})
public abstract interface ShowTemplateListener extends io.piano.android.composer.listeners.EventTypeListener<io.piano.android.composer.model.events.ShowTemplate> {
    
    @java.lang.Override()
    public boolean canProcess(@org.jetbrains.annotations.NotNull()
    io.piano.android.composer.model.Event<io.piano.android.composer.model.events.EventType> event) {
        return false;
    }
}