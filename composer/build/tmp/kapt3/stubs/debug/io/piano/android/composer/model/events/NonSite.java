package io.piano.android.composer.model.events;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lio/piano/android/composer/model/events/NonSite;", "Lio/piano/android/composer/model/events/EventType;", "()V", "composer_debug"})
public final class NonSite extends io.piano.android.composer.model.events.EventType {
    public static final io.piano.android.composer.model.events.NonSite INSTANCE = null;
    
    private NonSite() {
    }
}