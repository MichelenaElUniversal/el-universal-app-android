package io.piano.android.composer;

import java.lang.System;

@androidx.annotation.RestrictTo(value = {androidx.annotation.RestrictTo.Scope.LIBRARY})
@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0001\u0018\u0000 \u00172\u00020\u0001:\u0001\u0017B!\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0007J\r\u0010\u0016\u001a\u00020\u0005*\u00020\u0003H\u0082\bR\u000e\u0010\b\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u00020\u000bX\u0080\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0016\u0010\u000e\u001a\n \u0010*\u0004\u0018\u00010\u000f0\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0011\u001a\n \u0010*\u0004\u0018\u00010\u00120\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"}, d2 = {"Lio/piano/android/composer/DependenciesProvider;", "", "context", "Landroid/content/Context;", "aid", "", "endpoint", "(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V", "api", "Lio/piano/android/composer/Api;", "composer", "Lio/piano/android/composer/Composer;", "getComposer$composer_debug", "()Lio/piano/android/composer/Composer;", "moshi", "Lcom/squareup/moshi/Moshi;", "kotlin.jvm.PlatformType", "okHttpClient", "Lokhttp3/OkHttpClient;", "prefsStorage", "Lio/piano/android/composer/PrefsStorage;", "userAgent", "deviceType", "Companion", "composer_debug"})
public final class DependenciesProvider {
    private final io.piano.android.composer.PrefsStorage prefsStorage = null;
    private final java.lang.String userAgent = null;
    private final okhttp3.OkHttpClient okHttpClient = null;
    private final com.squareup.moshi.Moshi moshi = null;
    private final io.piano.android.composer.Api api = null;
    @org.jetbrains.annotations.NotNull()
    private final io.piano.android.composer.Composer composer = null;
    private static volatile io.piano.android.composer.DependenciesProvider instance;
    public static final io.piano.android.composer.DependenciesProvider.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    public final io.piano.android.composer.Composer getComposer$composer_debug() {
        return null;
    }
    
    @kotlin.Suppress(names = {"NOTHING_TO_INLINE"})
    private final java.lang.String deviceType(android.content.Context $this$deviceType) {
        return null;
    }
    
    private DependenciesProvider(android.content.Context context, java.lang.String aid, java.lang.String endpoint) {
        super();
    }
    
    public static final void init$composer_debug(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String aid, @org.jetbrains.annotations.Nullable()
    java.lang.String endpoint) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final io.piano.android.composer.DependenciesProvider getInstance() {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0006\u001a\u00020\u0004H\u0007J\'\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\b\u0010\r\u001a\u0004\u0018\u00010\fH\u0001\u00a2\u0006\u0002\b\u000eR\u001a\u0010\u0003\u001a\u0004\u0018\u00010\u00048\u0002@\u0002X\u0083\u000e\u00a2\u0006\b\n\u0000\u0012\u0004\b\u0005\u0010\u0002\u00a8\u0006\u000f"}, d2 = {"Lio/piano/android/composer/DependenciesProvider$Companion;", "", "()V", "instance", "Lio/piano/android/composer/DependenciesProvider;", "getInstance$annotations", "getInstance", "init", "", "context", "Landroid/content/Context;", "aid", "", "endpoint", "init$composer_debug", "composer_debug"})
    public static final class Companion {
        
        @java.lang.Deprecated()
        private static void getInstance$annotations() {
        }
        
        public final void init$composer_debug(@org.jetbrains.annotations.NotNull()
        android.content.Context context, @org.jetbrains.annotations.NotNull()
        java.lang.String aid, @org.jetbrains.annotations.Nullable()
        java.lang.String endpoint) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final io.piano.android.composer.DependenciesProvider getInstance() {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}