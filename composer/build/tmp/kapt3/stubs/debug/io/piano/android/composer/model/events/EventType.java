package io.piano.android.composer.model.events;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b6\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002\u0082\u0001\u0006\u0003\u0004\u0005\u0006\u0007\b\u00a8\u0006\t"}, d2 = {"Lio/piano/android/composer/model/events/EventType;", "", "()V", "Lio/piano/android/composer/model/events/ExperienceExecute;", "Lio/piano/android/composer/model/events/Meter;", "Lio/piano/android/composer/model/events/NonSite;", "Lio/piano/android/composer/model/events/ShowLogin;", "Lio/piano/android/composer/model/events/ShowTemplate;", "Lio/piano/android/composer/model/events/UserSegment;", "composer_debug"})
public abstract class EventType {
    
    private EventType() {
        super();
    }
}