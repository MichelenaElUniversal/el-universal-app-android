package io.piano.android.composer;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010#\n\u0002\u0010\u001b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 \f2\u00020\u0001:\u0004\f\r\u000e\u000fB\u0005\u00a2\u0006\u0002\u0010\u0002J.\u0010\u0003\u001a\b\u0012\u0002\b\u0003\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u000e\u0010\u0007\u001a\n\u0012\u0006\b\u0001\u0012\u00020\t0\b2\u0006\u0010\n\u001a\u00020\u000bH\u0016\u00a8\u0006\u0010"}, d2 = {"Lio/piano/android/composer/EventJsonAdapterFactory;", "Lcom/squareup/moshi/JsonAdapter$Factory;", "()V", "create", "Lcom/squareup/moshi/JsonAdapter;", "type", "Ljava/lang/reflect/Type;", "annotations", "", "", "moshi", "Lcom/squareup/moshi/Moshi;", "Companion", "DelegateAdapter", "EventJsonAdapter", "StubAdapter", "composer_debug"})
public final class EventJsonAdapterFactory implements com.squareup.moshi.JsonAdapter.Factory {
    private static final java.lang.String EVENT_MODULE_PARAMS = "eventModuleParams";
    private static final java.lang.String EVENT_EXECUTION_CONTEXT = "eventExecutionContext";
    private static final java.lang.String EVENT_PARAMS = "eventParams";
    private static final java.lang.String EVENT_TYPE = "eventType";
    private static final java.lang.String EVENT_TYPE_SHOW_LOGIN = "showLogin";
    private static final java.lang.String EVENT_TYPE_METER_ACTIVE = "meterActive";
    private static final java.lang.String EVENT_TYPE_METER_EXPIRED = "meterExpired";
    private static final java.lang.String EVENT_TYPE_USER_SEGMENT_TRUE = "userSegmentTrue";
    private static final java.lang.String EVENT_TYPE_USER_SEGMENT_FALSE = "userSegmentFalse";
    private static final java.lang.String EVENT_TYPE_EXPERIENCE_EXECUTE = "experienceExecute";
    private static final java.lang.String EVENT_TYPE_NON_SITE = "nonSite";
    private static final java.lang.String EVENT_TYPE_SHOW_TEMPLATE = "showTemplate";
    public static final io.piano.android.composer.EventJsonAdapterFactory.Companion Companion = null;
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public com.squareup.moshi.JsonAdapter<?> create(@org.jetbrains.annotations.NotNull()
    java.lang.reflect.Type type, @org.jetbrains.annotations.NotNull()
    java.util.Set<? extends java.lang.annotation.Annotation> annotations, @org.jetbrains.annotations.NotNull()
    com.squareup.moshi.Moshi moshi) {
        return null;
    }
    
    public EventJsonAdapterFactory() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0002\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u0002B\u0013\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00000\u0004\u00a2\u0006\u0002\u0010\u0005J\u0015\u0010\u0006\u001a\u00028\u00002\u0006\u0010\u0007\u001a\u00020\bH\u0016\u00a2\u0006\u0002\u0010\tJ\u001f\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\b\u0010\u000e\u001a\u0004\u0018\u00018\u0000H\u0016\u00a2\u0006\u0002\u0010\u000fR\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00000\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"}, d2 = {"Lio/piano/android/composer/EventJsonAdapterFactory$StubAdapter;", "T", "Lcom/squareup/moshi/JsonAdapter;", "stubFunction", "Lkotlin/Function0;", "(Lkotlin/jvm/functions/Function0;)V", "fromJson", "reader", "Lcom/squareup/moshi/JsonReader;", "(Lcom/squareup/moshi/JsonReader;)Ljava/lang/Object;", "toJson", "", "writer", "Lcom/squareup/moshi/JsonWriter;", "value", "(Lcom/squareup/moshi/JsonWriter;Ljava/lang/Object;)V", "composer_debug"})
    static final class StubAdapter<T extends java.lang.Object> extends com.squareup.moshi.JsonAdapter<T> {
        private final kotlin.jvm.functions.Function0<T> stubFunction = null;
        
        @java.lang.Override()
        public T fromJson(@org.jetbrains.annotations.NotNull()
        com.squareup.moshi.JsonReader reader) {
            return null;
        }
        
        @java.lang.Override()
        public void toJson(@org.jetbrains.annotations.NotNull()
        com.squareup.moshi.JsonWriter writer, @org.jetbrains.annotations.Nullable()
        T value) {
        }
        
        public StubAdapter(@org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function0<? extends T> stubFunction) {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0002\u0018\u0000*\u0004\b\u0000\u0010\u00012\b\u0012\u0004\u0012\u0002H\u00010\u0002B,\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00000\u0002\u0012\u0017\u0010\u0004\u001a\u0013\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00000\u0005\u00a2\u0006\u0002\b\u0006\u00a2\u0006\u0002\u0010\u0007J\u0017\u0010\b\u001a\u0004\u0018\u00018\u00002\u0006\u0010\t\u001a\u00020\nH\u0016\u00a2\u0006\u0002\u0010\u000bJ\u001f\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\b\u0010\u0010\u001a\u0004\u0018\u00018\u0000H\u0016\u00a2\u0006\u0002\u0010\u0011R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00028\u00000\u0002X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001f\u0010\u0004\u001a\u0013\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00000\u0005\u00a2\u0006\u0002\b\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"}, d2 = {"Lio/piano/android/composer/EventJsonAdapterFactory$DelegateAdapter;", "T", "Lcom/squareup/moshi/JsonAdapter;", "delegateAdapter", "postProcessAction", "Lkotlin/Function1;", "Lkotlin/ExtensionFunctionType;", "(Lcom/squareup/moshi/JsonAdapter;Lkotlin/jvm/functions/Function1;)V", "fromJson", "reader", "Lcom/squareup/moshi/JsonReader;", "(Lcom/squareup/moshi/JsonReader;)Ljava/lang/Object;", "toJson", "", "writer", "Lcom/squareup/moshi/JsonWriter;", "value", "(Lcom/squareup/moshi/JsonWriter;Ljava/lang/Object;)V", "composer_debug"})
    static final class DelegateAdapter<T extends java.lang.Object> extends com.squareup.moshi.JsonAdapter<T> {
        private final com.squareup.moshi.JsonAdapter<T> delegateAdapter = null;
        private final kotlin.jvm.functions.Function1<T, T> postProcessAction = null;
        
        @org.jetbrains.annotations.Nullable()
        @java.lang.Override()
        public T fromJson(@org.jetbrains.annotations.NotNull()
        com.squareup.moshi.JsonReader reader) {
            return null;
        }
        
        @java.lang.Override()
        public void toJson(@org.jetbrains.annotations.NotNull()
        com.squareup.moshi.JsonWriter writer, @org.jetbrains.annotations.Nullable()
        T value) {
        }
        
        public DelegateAdapter(@org.jetbrains.annotations.NotNull()
        com.squareup.moshi.JsonAdapter<T> delegateAdapter, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super T, ? extends T> postProcessAction) {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\u0018\u00002\f\u0012\b\u0012\u0006\u0012\u0002\b\u00030\u00020\u0001B7\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00040\u0001\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00060\u0001\u0012\u0014\u0010\u0007\u001a\u0010\u0012\f\u0012\n\u0012\u0006\b\u0001\u0012\u00020\t0\u00010\b\u00a2\u0006\u0002\u0010\nJ\u0016\u0010\u0010\u001a\b\u0012\u0002\b\u0003\u0018\u00010\u00022\u0006\u0010\u0011\u001a\u00020\u0012H\u0016J\u001e\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00162\f\u0010\u0017\u001a\b\u0012\u0002\b\u0003\u0018\u00010\u0002H\u0016J\f\u0010\u0018\u001a\u00020\u0019*\u00020\u0012H\u0002R\u001c\u0010\u0007\u001a\u0010\u0012\f\u0012\n\u0012\u0006\b\u0001\u0012\u00020\t0\u00010\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00060\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00040\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000b\u001a\n \r*\u0004\u0018\u00010\f0\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000e\u001a\n \r*\u0004\u0018\u00010\f0\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000f\u001a\n \r*\u0004\u0018\u00010\f0\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"}, d2 = {"Lio/piano/android/composer/EventJsonAdapterFactory$EventJsonAdapter;", "Lcom/squareup/moshi/JsonAdapter;", "Lio/piano/android/composer/model/Event;", "eventModuleParamsAdapter", "Lio/piano/android/composer/model/EventModuleParams;", "eventExecutionContextAdapter", "Lio/piano/android/composer/model/EventExecutionContext;", "eventDataAdapters", "", "Lio/piano/android/composer/model/events/EventType;", "(Lcom/squareup/moshi/JsonAdapter;Lcom/squareup/moshi/JsonAdapter;Ljava/util/List;)V", "eventSubtypesOptions", "Lcom/squareup/moshi/JsonReader$Options;", "kotlin.jvm.PlatformType", "eventTypeKeyOptions", "options", "fromJson", "reader", "Lcom/squareup/moshi/JsonReader;", "toJson", "", "writer", "Lcom/squareup/moshi/JsonWriter;", "value", "findEventType", "", "composer_debug"})
    public static final class EventJsonAdapter extends com.squareup.moshi.JsonAdapter<io.piano.android.composer.model.Event<?>> {
        private final com.squareup.moshi.JsonReader.Options options = null;
        private final com.squareup.moshi.JsonReader.Options eventTypeKeyOptions = null;
        private final com.squareup.moshi.JsonReader.Options eventSubtypesOptions = null;
        private final com.squareup.moshi.JsonAdapter<io.piano.android.composer.model.EventModuleParams> eventModuleParamsAdapter = null;
        private final com.squareup.moshi.JsonAdapter<io.piano.android.composer.model.EventExecutionContext> eventExecutionContextAdapter = null;
        private final java.util.List<com.squareup.moshi.JsonAdapter<? extends io.piano.android.composer.model.events.EventType>> eventDataAdapters = null;
        
        @org.jetbrains.annotations.Nullable()
        @java.lang.Override()
        public io.piano.android.composer.model.Event<?> fromJson(@org.jetbrains.annotations.NotNull()
        com.squareup.moshi.JsonReader reader) {
            return null;
        }
        
        private final int findEventType(com.squareup.moshi.JsonReader $this$findEventType) {
            return 0;
        }
        
        @java.lang.Override()
        public void toJson(@org.jetbrains.annotations.NotNull()
        com.squareup.moshi.JsonWriter writer, @org.jetbrains.annotations.Nullable()
        io.piano.android.composer.model.Event<?> value) {
        }
        
        public EventJsonAdapter(@org.jetbrains.annotations.NotNull()
        com.squareup.moshi.JsonAdapter<io.piano.android.composer.model.EventModuleParams> eventModuleParamsAdapter, @org.jetbrains.annotations.NotNull()
        com.squareup.moshi.JsonAdapter<io.piano.android.composer.model.EventExecutionContext> eventExecutionContextAdapter, @org.jetbrains.annotations.NotNull()
        java.util.List<? extends com.squareup.moshi.JsonAdapter<? extends io.piano.android.composer.model.events.EventType>> eventDataAdapters) {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\f\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"}, d2 = {"Lio/piano/android/composer/EventJsonAdapterFactory$Companion;", "", "()V", "EVENT_EXECUTION_CONTEXT", "", "EVENT_MODULE_PARAMS", "EVENT_PARAMS", "EVENT_TYPE", "EVENT_TYPE_EXPERIENCE_EXECUTE", "EVENT_TYPE_METER_ACTIVE", "EVENT_TYPE_METER_EXPIRED", "EVENT_TYPE_NON_SITE", "EVENT_TYPE_SHOW_LOGIN", "EVENT_TYPE_SHOW_TEMPLATE", "EVENT_TYPE_USER_SEGMENT_FALSE", "EVENT_TYPE_USER_SEGMENT_TRUE", "composer_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}