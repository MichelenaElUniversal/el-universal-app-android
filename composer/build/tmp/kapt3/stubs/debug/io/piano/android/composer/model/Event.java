package io.piano.android.composer.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u0000*\b\b\u0000\u0010\u0001*\u00020\u00022\u00020\u0003B\u001d\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00028\u0000\u00a2\u0006\u0002\u0010\tJ\t\u0010\u000b\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\f\u001a\u00020\u0007H\u00c6\u0003J\u000e\u0010\r\u001a\u00028\u0000H\u00c6\u0003\u00a2\u0006\u0002\u0010\u000eJ2\u0010\u000f\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00028\u0000H\u00c6\u0001\u00a2\u0006\u0002\u0010\u0010J\u0013\u0010\u0011\u001a\u00020\u00122\b\u0010\u0013\u001a\u0004\u0018\u00010\u0003H\u00d6\u0003J\t\u0010\u0014\u001a\u00020\u0015H\u00d6\u0001J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001R\u0012\u0010\b\u001a\u00028\u00008\u0006X\u0087\u0004\u00a2\u0006\u0004\n\u0002\u0010\nR\u0010\u0010\u0006\u001a\u00020\u00078\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0004\u001a\u00020\u00058\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0018"}, d2 = {"Lio/piano/android/composer/model/Event;", "T", "Lio/piano/android/composer/model/events/EventType;", "", "eventModuleParams", "Lio/piano/android/composer/model/EventModuleParams;", "eventExecutionContext", "Lio/piano/android/composer/model/EventExecutionContext;", "eventData", "(Lio/piano/android/composer/model/EventModuleParams;Lio/piano/android/composer/model/EventExecutionContext;Lio/piano/android/composer/model/events/EventType;)V", "Lio/piano/android/composer/model/events/EventType;", "component1", "component2", "component3", "()Lio/piano/android/composer/model/events/EventType;", "copy", "(Lio/piano/android/composer/model/EventModuleParams;Lio/piano/android/composer/model/EventExecutionContext;Lio/piano/android/composer/model/events/EventType;)Lio/piano/android/composer/model/Event;", "equals", "", "other", "hashCode", "", "toString", "", "composer_debug"})
public final class Event<T extends io.piano.android.composer.model.events.EventType> {
    @org.jetbrains.annotations.NotNull()
    public final io.piano.android.composer.model.EventModuleParams eventModuleParams = null;
    @org.jetbrains.annotations.NotNull()
    public final io.piano.android.composer.model.EventExecutionContext eventExecutionContext = null;
    @org.jetbrains.annotations.NotNull()
    public final T eventData = null;
    
    public Event(@org.jetbrains.annotations.NotNull()
    io.piano.android.composer.model.EventModuleParams eventModuleParams, @org.jetbrains.annotations.NotNull()
    io.piano.android.composer.model.EventExecutionContext eventExecutionContext, @org.jetbrains.annotations.NotNull()
    T eventData) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final io.piano.android.composer.model.EventModuleParams component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final io.piano.android.composer.model.EventExecutionContext component2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final T component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final io.piano.android.composer.model.Event<T> copy(@org.jetbrains.annotations.NotNull()
    io.piano.android.composer.model.EventModuleParams eventModuleParams, @org.jetbrains.annotations.NotNull()
    io.piano.android.composer.model.EventExecutionContext eventExecutionContext, @org.jetbrains.annotations.NotNull()
    T eventData) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object p0) {
        return false;
    }
}