package io.piano.android.composer;

import java.lang.System;

@androidx.annotation.RestrictTo(value = {androidx.annotation.RestrictTo.Scope.LIBRARY})
@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u000b\n\u0002\u0010\t\n\u0002\b\r\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0001\u0018\u0000 ,2\u00020\u0001:\u0001,B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010&\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\'\u001a\u00020\u000fJ\u0018\u0010(\u001a\u00020)2\u0006\u0010\'\u001a\u00020\u000f2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fJ\u0010\u0010*\u001a\u00020)2\b\u0010\u000e\u001a\u0004\u0018\u00010+J\u000e\u0010*\u001a\u00020)2\u0006\u0010\u000e\u001a\u00020\u001bR\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\t\u001a\u00020\b2\u0006\u0010\u0007\u001a\u00020\b8F@FX\u0086\u000e\u00a2\u0006\f\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\rR(\u0010\u0010\u001a\u0004\u0018\u00010\u000f2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000f8F@FX\u0086\u000e\u00a2\u0006\f\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R(\u0010\u0015\u001a\u0004\u0018\u00010\u000f2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000f8F@FX\u0086\u000e\u00a2\u0006\f\u001a\u0004\b\u0016\u0010\u0012\"\u0004\b\u0017\u0010\u0014R(\u0010\u0018\u001a\u0004\u0018\u00010\u000f2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000f8F@FX\u0086\u000e\u00a2\u0006\f\u001a\u0004\b\u0019\u0010\u0012\"\u0004\b\u001a\u0010\u0014R$\u0010\u001c\u001a\u00020\u001b2\u0006\u0010\u0007\u001a\u00020\u001b8F@FX\u0086\u000e\u00a2\u0006\f\u001a\u0004\b\u001d\u0010\u001e\"\u0004\b\u001f\u0010 R\u0011\u0010!\u001a\u00020\u001b8F\u00a2\u0006\u0006\u001a\u0004\b\"\u0010\u001eR(\u0010#\u001a\u0004\u0018\u00010\u000f2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000f8F@FX\u0086\u000e\u00a2\u0006\f\u001a\u0004\b$\u0010\u0012\"\u0004\b%\u0010\u0014\u00a8\u0006-"}, d2 = {"Lio/piano/android/composer/PrefsStorage;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "prefs", "Landroid/content/SharedPreferences;", "milliseconds", "", "serverTimezoneOffset", "getServerTimezoneOffset", "()I", "setServerTimezoneOffset", "(I)V", "value", "", "tpAccessCookie", "getTpAccessCookie", "()Ljava/lang/String;", "setTpAccessCookie", "(Ljava/lang/String;)V", "tpBrowserCookie", "getTpBrowserCookie", "setTpBrowserCookie", "visitId", "getVisitId", "setVisitId", "", "visitTimeout", "getVisitTimeout", "()J", "setVisitTimeout", "(J)V", "visitTimestamp", "getVisitTimestamp", "xbuilderBrowserCookie", "getXbuilderBrowserCookie", "setXbuilderBrowserCookie", "getValueForKey", "key", "setValueForKey", "", "setVisitDate", "Ljava/util/Date;", "Companion", "composer_debug"})
public final class PrefsStorage {
    private final android.content.SharedPreferences prefs = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PREFS_NAME = "io.piano.android.composer";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String KEY_VISIT_ID = "visitId";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String KEY_VISIT_ID_TIMESTAMP = "visitIdTimestampMillis";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String KEY_TP_BROWSER_COOKIE = "tbc";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String KEY_XBUILDER_BROWSER_COOKIE = "xbc";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String KEY_TP_ACCESS_COOKIE = "tac";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String KEY_TIMEZONE_OFFSET = "timeZoneOffsetMillis";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String KEY_VISIT_TIMEOUT = "visitTimeoutMinutes";
    private static final long VISIT_TIMEOUT_FALLBACK = 0L;
    public static final io.piano.android.composer.PrefsStorage.Companion Companion = null;
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getValueForKey(@org.jetbrains.annotations.NotNull()
    java.lang.String key) {
        return null;
    }
    
    public final void setValueForKey(@org.jetbrains.annotations.NotNull()
    java.lang.String key, @org.jetbrains.annotations.Nullable()
    java.lang.String value) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getVisitId() {
        return null;
    }
    
    public final void setVisitId(@org.jetbrains.annotations.Nullable()
    java.lang.String value) {
    }
    
    public final long getVisitTimestamp() {
        return 0L;
    }
    
    public final void setVisitDate(long value) {
    }
    
    public final void setVisitDate(@org.jetbrains.annotations.Nullable()
    java.util.Date value) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTpBrowserCookie() {
        return null;
    }
    
    public final void setTpBrowserCookie(@org.jetbrains.annotations.Nullable()
    java.lang.String value) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getXbuilderBrowserCookie() {
        return null;
    }
    
    public final void setXbuilderBrowserCookie(@org.jetbrains.annotations.Nullable()
    java.lang.String value) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTpAccessCookie() {
        return null;
    }
    
    public final void setTpAccessCookie(@org.jetbrains.annotations.Nullable()
    java.lang.String value) {
    }
    
    public final int getServerTimezoneOffset() {
        return 0;
    }
    
    public final void setServerTimezoneOffset(int milliseconds) {
    }
    
    public final long getVisitTimeout() {
        return 0L;
    }
    
    public final void setVisitTimeout(long milliseconds) {
    }
    
    public PrefsStorage(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0010\t\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\u00020\rX\u0080\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000f\u00a8\u0006\u0010"}, d2 = {"Lio/piano/android/composer/PrefsStorage$Companion;", "", "()V", "KEY_TIMEZONE_OFFSET", "", "KEY_TP_ACCESS_COOKIE", "KEY_TP_BROWSER_COOKIE", "KEY_VISIT_ID", "KEY_VISIT_ID_TIMESTAMP", "KEY_VISIT_TIMEOUT", "KEY_XBUILDER_BROWSER_COOKIE", "PREFS_NAME", "VISIT_TIMEOUT_FALLBACK", "", "getVISIT_TIMEOUT_FALLBACK$composer_debug", "()J", "composer_debug"})
    public static final class Companion {
        
        public final long getVISIT_TIMEOUT_FALLBACK$composer_debug() {
            return 0L;
        }
        
        private Companion() {
            super();
        }
    }
}