package io.piano.android.composer.model.events;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\f\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\b\u0087\b\u0018\u00002\u00020\u0001:\u0001\u001cBE\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\b\u001a\u00020\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\rJ\t\u0010\u000e\u001a\u00020\u0003H\u00c6\u0003J\u000b\u0010\u000f\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0006H\u00c6\u0003J\u000b\u0010\u0011\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\t\u0010\u0012\u001a\u00020\tH\u00c6\u0003J\t\u0010\u0013\u001a\u00020\u000bH\u00c6\u0003J\u000b\u0010\u0014\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003JU\u0010\u0015\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00062\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00032\b\b\u0002\u0010\b\u001a\u00020\t2\b\b\u0002\u0010\n\u001a\u00020\u000b2\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001J\u0013\u0010\u0016\u001a\u00020\u000b2\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u00d6\u0003J\t\u0010\u0019\u001a\u00020\u001aH\u00d6\u0001J\t\u0010\u001b\u001a\u00020\u0003H\u00d6\u0001R\u0012\u0010\u0007\u001a\u0004\u0018\u00010\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u00020\t8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00068\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u00020\u000b8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\f\u001a\u0004\u0018\u00010\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"}, d2 = {"Lio/piano/android/composer/model/events/ShowTemplate;", "Lio/piano/android/composer/model/events/EventType;", "templateId", "", "templateVariantId", "displayMode", "Lio/piano/android/composer/model/events/ShowTemplate$DisplayMode;", "containerSelector", "delayBy", "Lio/piano/android/composer/model/DelayBy;", "showCloseButton", "", "url", "(Ljava/lang/String;Ljava/lang/String;Lio/piano/android/composer/model/events/ShowTemplate$DisplayMode;Ljava/lang/String;Lio/piano/android/composer/model/DelayBy;ZLjava/lang/String;)V", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "copy", "equals", "other", "", "hashCode", "", "toString", "DisplayMode", "composer_debug"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class ShowTemplate extends io.piano.android.composer.model.events.EventType {
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String templateId = null;
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String templateVariantId = null;
    @org.jetbrains.annotations.NotNull()
    public final io.piano.android.composer.model.events.ShowTemplate.DisplayMode displayMode = null;
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String containerSelector = null;
    @org.jetbrains.annotations.NotNull()
    public final io.piano.android.composer.model.DelayBy delayBy = null;
    public final boolean showCloseButton = false;
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String url = null;
    
    public ShowTemplate(@org.jetbrains.annotations.NotNull()
    java.lang.String templateId, @org.jetbrains.annotations.Nullable()
    java.lang.String templateVariantId, @org.jetbrains.annotations.NotNull()
    io.piano.android.composer.model.events.ShowTemplate.DisplayMode displayMode, @org.jetbrains.annotations.Nullable()
    java.lang.String containerSelector, @org.jetbrains.annotations.NotNull()
    io.piano.android.composer.model.DelayBy delayBy, boolean showCloseButton, @org.jetbrains.annotations.Nullable()
    java.lang.String url) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component1() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final io.piano.android.composer.model.events.ShowTemplate.DisplayMode component3() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component4() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final io.piano.android.composer.model.DelayBy component5() {
        return null;
    }
    
    public final boolean component6() {
        return false;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component7() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final io.piano.android.composer.model.events.ShowTemplate copy(@org.jetbrains.annotations.NotNull()
    java.lang.String templateId, @org.jetbrains.annotations.Nullable()
    java.lang.String templateVariantId, @org.jetbrains.annotations.NotNull()
    io.piano.android.composer.model.events.ShowTemplate.DisplayMode displayMode, @org.jetbrains.annotations.Nullable()
    java.lang.String containerSelector, @org.jetbrains.annotations.NotNull()
    io.piano.android.composer.model.DelayBy delayBy, boolean showCloseButton, @org.jetbrains.annotations.Nullable()
    java.lang.String url) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object p0) {
        return false;
    }
    
    @kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0007\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u000f\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\b\u0010\u0007\u001a\u00020\u0003H\u0016R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006j\u0002\b\bj\u0002\b\t\u00a8\u0006\n"}, d2 = {"Lio/piano/android/composer/model/events/ShowTemplate$DisplayMode;", "", "mode", "", "(Ljava/lang/String;ILjava/lang/String;)V", "getMode", "()Ljava/lang/String;", "toString", "MODAL", "INLINE", "composer_debug"})
    public static enum DisplayMode {
        @com.squareup.moshi.Json(name = "modal")
        /*public static final*/ MODAL /* = new MODAL(null) */,
        @com.squareup.moshi.Json(name = "inline")
        /*public static final*/ INLINE /* = new INLINE(null) */;
        @org.jetbrains.annotations.NotNull()
        private final java.lang.String mode = null;
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getMode() {
            return null;
        }
        
        DisplayMode(java.lang.String mode) {
        }
    }
}