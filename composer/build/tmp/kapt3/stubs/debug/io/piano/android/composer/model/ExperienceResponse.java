package io.piano.android.composer.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0007\u0018\u00002\u00020\u0001BG\u0012\n\b\u0001\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0001\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u0012\b\b\u0001\u0010\u0006\u001a\u00020\u0007\u0012\n\b\u0001\u0010\b\u001a\u0004\u0018\u00010\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\fR\u0010\u0010\n\u001a\u00020\u000b8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0005\u001a\u0004\u0018\u00010\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u00020\u00078\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\b\u001a\u0004\u0018\u00010\t8\u0006X\u0087\u0004\u00a2\u0006\u0004\n\u0002\u0010\rR\u0012\u0010\u0004\u001a\u0004\u0018\u00010\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"}, d2 = {"Lio/piano/android/composer/model/ExperienceResponse;", "", "tbCookie", "Lio/piano/android/composer/model/CookieObject;", "xbCookie", "taCookie", "timeZoneOffsetMillis", "", "visitTimeoutMinutes", "", "result", "Lio/piano/android/composer/model/EventsContainer;", "(Lio/piano/android/composer/model/CookieObject;Lio/piano/android/composer/model/CookieObject;Lio/piano/android/composer/model/CookieObject;ILjava/lang/Long;Lio/piano/android/composer/model/EventsContainer;)V", "Ljava/lang/Long;", "composer_debug"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class ExperienceResponse {
    @org.jetbrains.annotations.Nullable()
    public final io.piano.android.composer.model.CookieObject tbCookie = null;
    @org.jetbrains.annotations.Nullable()
    public final io.piano.android.composer.model.CookieObject xbCookie = null;
    @org.jetbrains.annotations.Nullable()
    public final io.piano.android.composer.model.CookieObject taCookie = null;
    public final int timeZoneOffsetMillis = 0;
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Long visitTimeoutMinutes = null;
    @org.jetbrains.annotations.NotNull()
    public final io.piano.android.composer.model.EventsContainer result = null;
    
    public ExperienceResponse(@org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "tbc")
    io.piano.android.composer.model.CookieObject tbCookie, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "xbc")
    io.piano.android.composer.model.CookieObject xbCookie, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "tac")
    io.piano.android.composer.model.CookieObject taCookie, @com.squareup.moshi.Json(name = "timezone_offset")
    int timeZoneOffsetMillis, @org.jetbrains.annotations.Nullable()
    @com.squareup.moshi.Json(name = "visit_timeout")
    java.lang.Long visitTimeoutMinutes, @org.jetbrains.annotations.NotNull()
    io.piano.android.composer.model.EventsContainer result) {
        super();
    }
}