package io.piano.android.composer.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u0001Bq\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u000e\u0010\u0006\u001a\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u0007\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u000b\u0012\u0006\u0010\f\u001a\u00020\u0003\u0012\u0006\u0010\r\u001a\u00020\u0003\u0012\u000e\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\u000f\u0018\u00010\u0007\u0012\u000e\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u0011\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\u0012R\u0018\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\u000f\u0018\u00010\u00078\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0018\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\u0011\u0018\u00010\u00078\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\t\u001a\u0004\u0018\u00010\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0004\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0018\u0010\u0006\u001a\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u00078\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0012\u0010\n\u001a\u0004\u0018\u00010\u000b8\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"}, d2 = {"Lio/piano/android/composer/model/EventExecutionContext;", "", "experienceId", "", "executionId", "trackingId", "splitTests", "", "Lio/piano/android/composer/model/SplitTest;", "currentMeterName", "user", "Lio/piano/android/composer/model/User;", "region", "countryCode", "accessList", "Lio/piano/android/composer/model/Access;", "activeMeters", "Lio/piano/android/composer/model/ActiveMeter;", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lio/piano/android/composer/model/User;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V", "composer_debug"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class EventExecutionContext {
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String experienceId = null;
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String executionId = null;
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String trackingId = null;
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<io.piano.android.composer.model.SplitTest> splitTests = null;
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String currentMeterName = null;
    @org.jetbrains.annotations.Nullable()
    public final io.piano.android.composer.model.User user = null;
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String region = null;
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String countryCode = null;
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<io.piano.android.composer.model.Access> accessList = null;
    @org.jetbrains.annotations.Nullable()
    public final java.util.List<io.piano.android.composer.model.ActiveMeter> activeMeters = null;
    
    public EventExecutionContext(@org.jetbrains.annotations.NotNull()
    java.lang.String experienceId, @org.jetbrains.annotations.NotNull()
    java.lang.String executionId, @org.jetbrains.annotations.NotNull()
    java.lang.String trackingId, @org.jetbrains.annotations.Nullable()
    java.util.List<io.piano.android.composer.model.SplitTest> splitTests, @org.jetbrains.annotations.Nullable()
    java.lang.String currentMeterName, @org.jetbrains.annotations.Nullable()
    io.piano.android.composer.model.User user, @org.jetbrains.annotations.NotNull()
    java.lang.String region, @org.jetbrains.annotations.NotNull()
    java.lang.String countryCode, @org.jetbrains.annotations.Nullable()
    java.util.List<io.piano.android.composer.model.Access> accessList, @org.jetbrains.annotations.Nullable()
    java.util.List<io.piano.android.composer.model.ActiveMeter> activeMeters) {
        super();
    }
}