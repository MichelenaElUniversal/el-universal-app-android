package io.piano.android.composer.listeners;

import java.lang.System;

/**
 * Listener for exceptions
 */
@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u00e6\u0080\u0001\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"}, d2 = {"Lio/piano/android/composer/listeners/ExceptionListener;", "", "onException", "", "exception", "Lio/piano/android/composer/ComposerException;", "composer_debug"})
public abstract interface ExceptionListener {
    
    /**
     * Processes exception
     *
     * @param exception exception thrown at processing event
     */
    public abstract void onException(@org.jetbrains.annotations.NotNull()
    io.piano.android.composer.ComposerException exception);
}