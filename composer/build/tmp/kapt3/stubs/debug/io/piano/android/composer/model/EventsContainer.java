package io.piano.android.composer.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0007\u0018\u00002\u00020\u0001B\u0019\u0012\u0012\u0010\u0002\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u0003\u00a2\u0006\u0002\u0010\u0006R\u001d\u0010\u0002\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\b\u00a8\u0006\t"}, d2 = {"Lio/piano/android/composer/model/EventsContainer;", "", "events", "", "Lio/piano/android/composer/model/Event;", "Lio/piano/android/composer/model/events/EventType;", "(Ljava/util/List;)V", "getEvents", "()Ljava/util/List;", "composer_debug"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class EventsContainer {
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<io.piano.android.composer.model.Event<io.piano.android.composer.model.events.EventType>> events = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<io.piano.android.composer.model.Event<io.piano.android.composer.model.events.EventType>> getEvents() {
        return null;
    }
    
    public EventsContainer(@org.jetbrains.annotations.NotNull()
    java.util.List<io.piano.android.composer.model.Event<io.piano.android.composer.model.events.EventType>> events) {
        super();
    }
}