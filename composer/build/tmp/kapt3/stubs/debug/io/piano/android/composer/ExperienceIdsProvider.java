package io.piano.android.composer;

import java.lang.System;

@androidx.annotation.RestrictTo(value = {androidx.annotation.RestrictTo.Scope.LIBRARY})
@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\t\n\u0002\b\u0005\b\u0001\u0018\u0000 \u001a2\u00020\u0001:\u0001\u001aB\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0015\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rH\u0000\u00a2\u0006\u0002\b\u000eJ\u0015\u0010\u000f\u001a\u00020\u000b2\u0006\u0010\u0010\u001a\u00020\u0011H\u0000\u00a2\u0006\u0002\b\u0012J\u0015\u0010\u0013\u001a\u00020\u000b2\u0006\u0010\u0010\u001a\u00020\u0011H\u0000\u00a2\u0006\u0002\b\u0014J\u0015\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0010\u001a\u00020\u0011H\u0000\u00a2\u0006\u0002\b\u0017J\u0015\u0010\u0018\u001a\u00020\u000b2\u0006\u0010\u0010\u001a\u00020\u0011H\u0000\u00a2\u0006\u0002\b\u0019R\u001e\u0010\u0007\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u0006@BX\u0080\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001b"}, d2 = {"Lio/piano/android/composer/ExperienceIdsProvider;", "", "prefsStorage", "Lio/piano/android/composer/PrefsStorage;", "(Lio/piano/android/composer/PrefsStorage;)V", "<set-?>", "", "isVisitIdGenerated", "isVisitIdGenerated$composer_debug", "()Z", "generateRandomAlphaNumString", "", "length", "", "generateRandomAlphaNumString$composer_debug", "generateVisitId", "date", "Ljava/util/Date;", "generateVisitId$composer_debug", "getPageViewId", "getPageViewId$composer_debug", "getServerMidnightTimestamp", "", "getServerMidnightTimestamp$composer_debug", "getVisitId", "getVisitId$composer_debug", "Companion", "composer_debug"})
public final class ExperienceIdsProvider {
    private boolean isVisitIdGenerated = false;
    private final io.piano.android.composer.PrefsStorage prefsStorage = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ALLOWED_RANDOM_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    private static final java.lang.String PAGE_VIEW_DATE_FORMAT = "yyyy-MM-dd-HH-mm-ss-SSS";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String VISIT_ID_PREFIX = "v-";
    public static final int RANDOM_STRING_SIZE = 16;
    public static final int HASH_SIZE = 32;
    @org.jetbrains.annotations.NotNull()
    private static final java.text.SimpleDateFormat dateFormat = null;
    public static final io.piano.android.composer.ExperienceIdsProvider.Companion Companion = null;
    
    public final boolean isVisitIdGenerated$composer_debug() {
        return false;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String generateRandomAlphaNumString$composer_debug(int length) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getPageViewId$composer_debug(@org.jetbrains.annotations.NotNull()
    java.util.Date date) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getVisitId$composer_debug(@org.jetbrains.annotations.NotNull()
    java.util.Date date) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String generateVisitId$composer_debug(@org.jetbrains.annotations.NotNull()
    java.util.Date date) {
        return null;
    }
    
    public final long getServerMidnightTimestamp$composer_debug(@org.jetbrains.annotations.NotNull()
    java.util.Date date) {
        return 0L;
    }
    
    public ExperienceIdsProvider(@org.jetbrains.annotations.NotNull()
    io.piano.android.composer.PrefsStorage prefsStorage) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0006X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u00020\u000bX\u0080\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\r\u00a8\u0006\u000e"}, d2 = {"Lio/piano/android/composer/ExperienceIdsProvider$Companion;", "", "()V", "ALLOWED_RANDOM_CHARS", "", "HASH_SIZE", "", "PAGE_VIEW_DATE_FORMAT", "RANDOM_STRING_SIZE", "VISIT_ID_PREFIX", "dateFormat", "Ljava/text/SimpleDateFormat;", "getDateFormat$composer_debug", "()Ljava/text/SimpleDateFormat;", "composer_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final java.text.SimpleDateFormat getDateFormat$composer_debug() {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}