package io.piano.android.composer.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\u0007\u0018\u00002\u00020\u0001B)\u0012\b\b\u0003\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\u0007\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\bR\u0010\u0010\u0007\u001a\u00020\u00068\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u00020\u00068\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0002\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0004\u001a\u00020\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lio/piano/android/composer/model/Access;", "", "resourceId", "", "resourceName", "expireDate", "", "daysUntilExpiration", "(Ljava/lang/String;Ljava/lang/String;II)V", "composer_debug"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class Access {
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String resourceId = null;
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String resourceName = null;
    public final int expireDate = 0;
    public final int daysUntilExpiration = 0;
    
    public Access(@org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "rid")
    java.lang.String resourceId, @org.jetbrains.annotations.NotNull()
    java.lang.String resourceName, int expireDate, int daysUntilExpiration) {
        super();
    }
}