package io.piano.android.composer.model;

import java.lang.System;

/**
 * @property isDebug "Debug" value
 * @property customVariables Custom variables value
 * @property url Url value
 * @property referer Referer value
 * @property tags Tags value
 * @property zone Zone value
 * @property contentCreated Content created value
 * @property contentAuthor Content author value
 * @property contentSection Content section value
 * @property contentIsNative Content is native flag
 * @property customParameters Custom parameters
 */
@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010$\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0015\u0018\u0000 %2\u00020\u0001:\u0002$%B\u0083\u0001\b\u0002\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0014\u0010\u0004\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u0005\u0012\b\u0010\u0007\u001a\u0004\u0018\u00010\u0006\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0006\u0012\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00060\n\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\u0006\u0012\b\u0010\f\u001a\u0004\u0018\u00010\u0006\u0012\b\u0010\r\u001a\u0004\u0018\u00010\u0006\u0012\b\u0010\u000e\u001a\u0004\u0018\u00010\u0006\u0012\b\u0010\u000f\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011\u00a2\u0006\u0002\u0010\u0012R\u0013\u0010\r\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0014R\u0013\u0010\f\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0014R\u0015\u0010\u000f\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\n\n\u0002\u0010\u0018\u001a\u0004\b\u0016\u0010\u0017R\u0013\u0010\u000e\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0014R\u0013\u0010\u0010\u001a\u0004\u0018\u00010\u0011\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u001bR\u001f\u0010\u0004\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001dR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0002\u0010\u001eR\u0013\u0010\b\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u0014R\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00060\n\u00a2\u0006\b\n\u0000\u001a\u0004\b \u0010!R\u0013\u0010\u0007\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\"\u0010\u0014R\u0013\u0010\u000b\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b#\u0010\u0014\u00a8\u0006&"}, d2 = {"Lio/piano/android/composer/model/ExperienceRequest;", "", "isDebug", "", "customVariables", "", "", "url", "referer", "tags", "", "zone", "contentCreated", "contentAuthor", "contentSection", "contentIsNative", "customParameters", "Lio/piano/android/composer/model/CustomParameters;", "(ZLjava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Lio/piano/android/composer/model/CustomParameters;)V", "getContentAuthor", "()Ljava/lang/String;", "getContentCreated", "getContentIsNative", "()Ljava/lang/Boolean;", "Ljava/lang/Boolean;", "getContentSection", "getCustomParameters", "()Lio/piano/android/composer/model/CustomParameters;", "getCustomVariables", "()Ljava/util/Map;", "()Z", "getReferer", "getTags", "()Ljava/util/List;", "getUrl", "getZone", "Builder", "Companion", "composer_debug"})
public final class ExperienceRequest {
    private final boolean isDebug = false;
    @org.jetbrains.annotations.NotNull()
    private final java.util.Map<java.lang.String, java.lang.String> customVariables = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String url = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String referer = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<java.lang.String> tags = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String zone = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String contentCreated = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String contentAuthor = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String contentSection = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.Boolean contentIsNative = null;
    @org.jetbrains.annotations.Nullable()
    private final io.piano.android.composer.model.CustomParameters customParameters = null;
    private static final java.lang.String ISO_8601 = "yyyy-MM-dd\'T\'HH:mm:ss.SSSZ";
    @org.jetbrains.annotations.NotNull()
    private static final java.text.DateFormat DATE_FORMAT = null;
    public static final io.piano.android.composer.model.ExperienceRequest.Companion Companion = null;
    
    public final boolean isDebug() {
        return false;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.Map<java.lang.String, java.lang.String> getCustomVariables() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getUrl() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getReferer() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<java.lang.String> getTags() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getZone() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getContentCreated() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getContentAuthor() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getContentSection() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Boolean getContentIsNative() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final io.piano.android.composer.model.CustomParameters getCustomParameters() {
        return null;
    }
    
    private ExperienceRequest(boolean isDebug, java.util.Map<java.lang.String, java.lang.String> customVariables, java.lang.String url, java.lang.String referer, java.util.List<java.lang.String> tags, java.lang.String zone, java.lang.String contentCreated, java.lang.String contentAuthor, java.lang.String contentSection, java.lang.Boolean contentIsNative, io.piano.android.composer.model.CustomParameters customParameters) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010%\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010!\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b%\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010$\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u001e\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u0099\u0001\b\u0007\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u0012\u0016\b\u0002\u0010\u0004\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u0005\u0012\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0006\u0012\u000e\b\u0002\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00060\n\u0012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u0011\u00a2\u0006\u0002\u0010\u0012J\u0006\u00106\u001a\u000207J\u0006\u00108\u001a\u00020\u0000J\t\u00109\u001a\u00020\u0003H\u00c6\u0003J\u0010\u0010:\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003\u00a2\u0006\u0002\u0010\u001aJ\u000b\u0010;\u001a\u0004\u0018\u00010\u0011H\u00c6\u0003J\u0017\u0010<\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u0005H\u00c6\u0003J\u000b\u0010=\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003J\u000b\u0010>\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003J\u000f\u0010?\u001a\b\u0012\u0004\u0012\u00020\u00060\nH\u00c6\u0003J\u000b\u0010@\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003J\u000b\u0010A\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003J\u000b\u0010B\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003J\u000b\u0010C\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003J\u0010\u0010\r\u001a\u00020\u00002\b\u0010\r\u001a\u0004\u0018\u00010\u0006J\u000e\u0010\f\u001a\u00020\u00002\u0006\u0010\f\u001a\u00020DJ\u0010\u0010\f\u001a\u00020\u00002\b\u0010\f\u001a\u0004\u0018\u00010\u0006J\u0015\u0010\u000f\u001a\u00020\u00002\b\u0010\u000f\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010EJ\u0010\u0010\u000e\u001a\u00020\u00002\b\u0010\u000e\u001a\u0004\u0018\u00010\u0006J\u00a0\u0001\u0010F\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u0016\b\u0002\u0010\u0004\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u00052\n\b\u0002\u0010\u0007\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00062\u000e\b\u0002\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00060\n2\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u00c6\u0001\u00a2\u0006\u0002\u0010GJ\u0010\u0010H\u001a\u00020\u00002\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011J\u0018\u0010I\u001a\u00020\u00002\u0006\u0010J\u001a\u00020\u00062\b\u0010K\u001a\u0004\u0018\u00010\u0006J\u001c\u0010\u0004\u001a\u00020\u00002\u0014\u0010\u0004\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0006\u0012\u0004\u0018\u00010\u00060LJ\u000e\u0010\u0002\u001a\u00020\u00002\u0006\u0010\u0002\u001a\u00020\u0003J\u0013\u0010M\u001a\u00020\u00032\b\u0010N\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010O\u001a\u00020PH\u00d6\u0001J\u0010\u0010\b\u001a\u00020\u00002\b\u0010\b\u001a\u0004\u0018\u00010\u0006J\u000e\u0010Q\u001a\u00020\u00002\u0006\u0010Q\u001a\u00020\u0006J\u0014\u0010\t\u001a\u00020\u00002\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00060RJ\t\u0010S\u001a\u00020\u0006H\u00d6\u0001J\u000e\u0010\u0007\u001a\u00020\u00002\u0006\u0010\u0007\u001a\u00020\u0006J\u0010\u0010\u000b\u001a\u00020\u00002\b\u0010\u000b\u001a\u0004\u0018\u00010\u0006R\u001c\u0010\r\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u001c\u0010\f\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0014\"\u0004\b\u0018\u0010\u0016R\u001e\u0010\u000f\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u001d\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001cR\u001c\u0010\u000e\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001e\u0010\u0014\"\u0004\b\u001f\u0010\u0016R\u001c\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b \u0010!\"\u0004\b\"\u0010#R(\u0010\u0004\u001a\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0006\u0012\u0004\u0018\u00010\u00060\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b$\u0010%\"\u0004\b&\u0010\'R\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010)\"\u0004\b*\u0010+R\u001c\u0010\b\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b,\u0010\u0014\"\u0004\b-\u0010\u0016R \u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00060\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b.\u0010/\"\u0004\b0\u00101R\u001c\u0010\u0007\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b2\u0010\u0014\"\u0004\b3\u0010\u0016R\u001c\u0010\u000b\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b4\u0010\u0014\"\u0004\b5\u0010\u0016\u00a8\u0006T"}, d2 = {"Lio/piano/android/composer/model/ExperienceRequest$Builder;", "", "debug", "", "customVariables", "", "", "url", "referer", "tags", "", "zone", "contentCreated", "contentAuthor", "contentSection", "contentIsNative", "customParameters", "Lio/piano/android/composer/model/CustomParameters;", "(ZLjava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Lio/piano/android/composer/model/CustomParameters;)V", "getContentAuthor", "()Ljava/lang/String;", "setContentAuthor", "(Ljava/lang/String;)V", "getContentCreated", "setContentCreated", "getContentIsNative", "()Ljava/lang/Boolean;", "setContentIsNative", "(Ljava/lang/Boolean;)V", "Ljava/lang/Boolean;", "getContentSection", "setContentSection", "getCustomParameters", "()Lio/piano/android/composer/model/CustomParameters;", "setCustomParameters", "(Lio/piano/android/composer/model/CustomParameters;)V", "getCustomVariables", "()Ljava/util/Map;", "setCustomVariables", "(Ljava/util/Map;)V", "getDebug", "()Z", "setDebug", "(Z)V", "getReferer", "setReferer", "getTags", "()Ljava/util/List;", "setTags", "(Ljava/util/List;)V", "getUrl", "setUrl", "getZone", "setZone", "build", "Lio/piano/android/composer/model/ExperienceRequest;", "clearCustomVariables", "component1", "component10", "component11", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "Ljava/util/Date;", "(Ljava/lang/Boolean;)Lio/piano/android/composer/model/ExperienceRequest$Builder;", "copy", "(ZLjava/util/Map;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Lio/piano/android/composer/model/CustomParameters;)Lio/piano/android/composer/model/ExperienceRequest$Builder;", "customParams", "customVariable", "key", "value", "", "equals", "other", "hashCode", "", "tag", "", "toString", "composer_debug"})
    public static final class Builder {
        private boolean debug;
        @org.jetbrains.annotations.NotNull()
        private java.util.Map<java.lang.String, java.lang.String> customVariables;
        @org.jetbrains.annotations.Nullable()
        private java.lang.String url;
        @org.jetbrains.annotations.Nullable()
        private java.lang.String referer;
        @org.jetbrains.annotations.NotNull()
        private java.util.List<java.lang.String> tags;
        @org.jetbrains.annotations.Nullable()
        private java.lang.String zone;
        @org.jetbrains.annotations.Nullable()
        private java.lang.String contentCreated;
        @org.jetbrains.annotations.Nullable()
        private java.lang.String contentAuthor;
        @org.jetbrains.annotations.Nullable()
        private java.lang.String contentSection;
        @org.jetbrains.annotations.Nullable()
        private java.lang.Boolean contentIsNative;
        @org.jetbrains.annotations.Nullable()
        private io.piano.android.composer.model.CustomParameters customParameters;
        
        /**
         * Sets "debug" flag for request
         * @param debug Debug value
         * @return Builder instance
         */
        @org.jetbrains.annotations.NotNull()
        public final io.piano.android.composer.model.ExperienceRequest.Builder debug(boolean debug) {
            return null;
        }
        
        /**
         * Adds custom variable to request
         * @param key Custom variable key
         * @param value Custom variable value
         * @return Builder instance
         */
        @org.jetbrains.annotations.NotNull()
        public final io.piano.android.composer.model.ExperienceRequest.Builder customVariable(@org.jetbrains.annotations.NotNull()
        java.lang.String key, @org.jetbrains.annotations.Nullable()
        java.lang.String value) {
            return null;
        }
        
        /**
         * Adds map of custom variables for request
         * @param customVariables Map of custom variables' values by their keys
         * @return Builder instance
         */
        @org.jetbrains.annotations.NotNull()
        public final io.piano.android.composer.model.ExperienceRequest.Builder customVariables(@org.jetbrains.annotations.NotNull()
        java.util.Map<java.lang.String, java.lang.String> customVariables) {
            return null;
        }
        
        /**
         * Clears added to request custom variables
         * @return Builder instance
         */
        @org.jetbrains.annotations.NotNull()
        public final io.piano.android.composer.model.ExperienceRequest.Builder clearCustomVariables() {
            return null;
        }
        
        /**
         * Sets "url" parameter for request
         * @param url Url value
         * @return Builder instance
         */
        @org.jetbrains.annotations.NotNull()
        public final io.piano.android.composer.model.ExperienceRequest.Builder url(@org.jetbrains.annotations.NotNull()
        java.lang.String url) {
            return null;
        }
        
        /**
         * Sets "referrer" parameter for request
         * @param referer Referrer value
         * @return Builder instance
         */
        @org.jetbrains.annotations.NotNull()
        public final io.piano.android.composer.model.ExperienceRequest.Builder referer(@org.jetbrains.annotations.Nullable()
        java.lang.String referer) {
            return null;
        }
        
        /**
         * Adds "tag" parameter to request
         * @param tag Tag value
         * @return Builder instance
         */
        @org.jetbrains.annotations.NotNull()
        public final io.piano.android.composer.model.ExperienceRequest.Builder tag(@org.jetbrains.annotations.NotNull()
        java.lang.String tag) {
            return null;
        }
        
        /**
         * Adds multiple "tag" parameters to request
         * @param tags Collection of tag values
         * @return Builder instance
         */
        @org.jetbrains.annotations.NotNull()
        public final io.piano.android.composer.model.ExperienceRequest.Builder tags(@org.jetbrains.annotations.NotNull()
        java.util.Collection<java.lang.String> tags) {
            return null;
        }
        
        /**
         * Sets "zone" parameter for request
         * @param zone Zone value
         * @return Builder instance
         */
        @org.jetbrains.annotations.NotNull()
        public final io.piano.android.composer.model.ExperienceRequest.Builder zone(@org.jetbrains.annotations.Nullable()
        java.lang.String zone) {
            return null;
        }
        
        /**
         * Sets "content created" parameter for request
         * @param contentCreated ISO 8601-formatted string that includes the published date and time of the content
         * @return Builder instance
         */
        @org.jetbrains.annotations.NotNull()
        public final io.piano.android.composer.model.ExperienceRequest.Builder contentCreated(@org.jetbrains.annotations.Nullable()
        java.lang.String contentCreated) {
            return null;
        }
        
        /**
         * Sets "content created" parameter for request
         * @param contentCreated Content created date value
         * @return Builder instance
         */
        @org.jetbrains.annotations.NotNull()
        public final io.piano.android.composer.model.ExperienceRequest.Builder contentCreated(@org.jetbrains.annotations.NotNull()
        java.util.Date contentCreated) {
            return null;
        }
        
        /**
         * Sets "content author" parameter for request
         * @param contentAuthor Content author value
         * @return Builder instance
         */
        @org.jetbrains.annotations.NotNull()
        public final io.piano.android.composer.model.ExperienceRequest.Builder contentAuthor(@org.jetbrains.annotations.Nullable()
        java.lang.String contentAuthor) {
            return null;
        }
        
        /**
         * Sets "content section" parameter for request
         * @param contentSection Content section value
         * @return Builder instance
         */
        @org.jetbrains.annotations.NotNull()
        public final io.piano.android.composer.model.ExperienceRequest.Builder contentSection(@org.jetbrains.annotations.Nullable()
        java.lang.String contentSection) {
            return null;
        }
        
        /**
         * Sets "content is native" flag for request
         * @param contentIsNative True, if content is native, otherwise False
         * @return Builder instance
         */
        @org.jetbrains.annotations.NotNull()
        public final io.piano.android.composer.model.ExperienceRequest.Builder contentIsNative(@org.jetbrains.annotations.Nullable()
        java.lang.Boolean contentIsNative) {
            return null;
        }
        
        /**
         * Sets custom parameters for request
         * @param customParameters Custom parameters object
         * @return Builder instance
         */
        @org.jetbrains.annotations.NotNull()
        public final io.piano.android.composer.model.ExperienceRequest.Builder customParams(@org.jetbrains.annotations.Nullable()
        io.piano.android.composer.model.CustomParameters customParameters) {
            return null;
        }
        
        /**
         * Builds request
         * @return ExperienceRequest instance
         */
        @org.jetbrains.annotations.NotNull()
        public final io.piano.android.composer.model.ExperienceRequest build() {
            return null;
        }
        
        public final boolean getDebug() {
            return false;
        }
        
        public final void setDebug(boolean p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.Map<java.lang.String, java.lang.String> getCustomVariables() {
            return null;
        }
        
        public final void setCustomVariables(@org.jetbrains.annotations.NotNull()
        java.util.Map<java.lang.String, java.lang.String> p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getUrl() {
            return null;
        }
        
        public final void setUrl(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getReferer() {
            return null;
        }
        
        public final void setReferer(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<java.lang.String> getTags() {
            return null;
        }
        
        public final void setTags(@org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getZone() {
            return null;
        }
        
        public final void setZone(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getContentCreated() {
            return null;
        }
        
        public final void setContentCreated(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getContentAuthor() {
            return null;
        }
        
        public final void setContentAuthor(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getContentSection() {
            return null;
        }
        
        public final void setContentSection(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Boolean getContentIsNative() {
            return null;
        }
        
        public final void setContentIsNative(@org.jetbrains.annotations.Nullable()
        java.lang.Boolean p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final io.piano.android.composer.model.CustomParameters getCustomParameters() {
            return null;
        }
        
        public final void setCustomParameters(@org.jetbrains.annotations.Nullable()
        io.piano.android.composer.model.CustomParameters p0) {
        }
        
        public Builder(boolean debug, @org.jetbrains.annotations.NotNull()
        java.util.Map<java.lang.String, java.lang.String> customVariables, @org.jetbrains.annotations.Nullable()
        java.lang.String url, @org.jetbrains.annotations.Nullable()
        java.lang.String referer, @org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> tags, @org.jetbrains.annotations.Nullable()
        java.lang.String zone, @org.jetbrains.annotations.Nullable()
        java.lang.String contentCreated, @org.jetbrains.annotations.Nullable()
        java.lang.String contentAuthor, @org.jetbrains.annotations.Nullable()
        java.lang.String contentSection, @org.jetbrains.annotations.Nullable()
        java.lang.Boolean contentIsNative, @org.jetbrains.annotations.Nullable()
        io.piano.android.composer.model.CustomParameters customParameters) {
            super();
        }
        
        public Builder(boolean debug, @org.jetbrains.annotations.NotNull()
        java.util.Map<java.lang.String, java.lang.String> customVariables, @org.jetbrains.annotations.Nullable()
        java.lang.String url, @org.jetbrains.annotations.Nullable()
        java.lang.String referer, @org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> tags, @org.jetbrains.annotations.Nullable()
        java.lang.String zone, @org.jetbrains.annotations.Nullable()
        java.lang.String contentCreated, @org.jetbrains.annotations.Nullable()
        java.lang.String contentAuthor, @org.jetbrains.annotations.Nullable()
        java.lang.String contentSection, @org.jetbrains.annotations.Nullable()
        java.lang.Boolean contentIsNative) {
            super();
        }
        
        public Builder(boolean debug, @org.jetbrains.annotations.NotNull()
        java.util.Map<java.lang.String, java.lang.String> customVariables, @org.jetbrains.annotations.Nullable()
        java.lang.String url, @org.jetbrains.annotations.Nullable()
        java.lang.String referer, @org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> tags, @org.jetbrains.annotations.Nullable()
        java.lang.String zone, @org.jetbrains.annotations.Nullable()
        java.lang.String contentCreated, @org.jetbrains.annotations.Nullable()
        java.lang.String contentAuthor, @org.jetbrains.annotations.Nullable()
        java.lang.String contentSection) {
            super();
        }
        
        public Builder(boolean debug, @org.jetbrains.annotations.NotNull()
        java.util.Map<java.lang.String, java.lang.String> customVariables, @org.jetbrains.annotations.Nullable()
        java.lang.String url, @org.jetbrains.annotations.Nullable()
        java.lang.String referer, @org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> tags, @org.jetbrains.annotations.Nullable()
        java.lang.String zone, @org.jetbrains.annotations.Nullable()
        java.lang.String contentCreated, @org.jetbrains.annotations.Nullable()
        java.lang.String contentAuthor) {
            super();
        }
        
        public Builder(boolean debug, @org.jetbrains.annotations.NotNull()
        java.util.Map<java.lang.String, java.lang.String> customVariables, @org.jetbrains.annotations.Nullable()
        java.lang.String url, @org.jetbrains.annotations.Nullable()
        java.lang.String referer, @org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> tags, @org.jetbrains.annotations.Nullable()
        java.lang.String zone, @org.jetbrains.annotations.Nullable()
        java.lang.String contentCreated) {
            super();
        }
        
        public Builder(boolean debug, @org.jetbrains.annotations.NotNull()
        java.util.Map<java.lang.String, java.lang.String> customVariables, @org.jetbrains.annotations.Nullable()
        java.lang.String url, @org.jetbrains.annotations.Nullable()
        java.lang.String referer, @org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> tags, @org.jetbrains.annotations.Nullable()
        java.lang.String zone) {
            super();
        }
        
        public Builder(boolean debug, @org.jetbrains.annotations.NotNull()
        java.util.Map<java.lang.String, java.lang.String> customVariables, @org.jetbrains.annotations.Nullable()
        java.lang.String url, @org.jetbrains.annotations.Nullable()
        java.lang.String referer, @org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> tags) {
            super();
        }
        
        public Builder(boolean debug, @org.jetbrains.annotations.NotNull()
        java.util.Map<java.lang.String, java.lang.String> customVariables, @org.jetbrains.annotations.Nullable()
        java.lang.String url, @org.jetbrains.annotations.Nullable()
        java.lang.String referer) {
            super();
        }
        
        public Builder(boolean debug, @org.jetbrains.annotations.NotNull()
        java.util.Map<java.lang.String, java.lang.String> customVariables, @org.jetbrains.annotations.Nullable()
        java.lang.String url) {
            super();
        }
        
        public Builder(boolean debug, @org.jetbrains.annotations.NotNull()
        java.util.Map<java.lang.String, java.lang.String> customVariables) {
            super();
        }
        
        public Builder(boolean debug) {
            super();
        }
        
        public Builder() {
            super();
        }
        
        public final boolean component1() {
            return false;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.Map<java.lang.String, java.lang.String> component2() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String component3() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String component4() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.util.List<java.lang.String> component5() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String component6() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String component7() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String component8() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String component9() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.Boolean component10() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final io.piano.android.composer.model.CustomParameters component11() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final io.piano.android.composer.model.ExperienceRequest.Builder copy(boolean debug, @org.jetbrains.annotations.NotNull()
        java.util.Map<java.lang.String, java.lang.String> customVariables, @org.jetbrains.annotations.Nullable()
        java.lang.String url, @org.jetbrains.annotations.Nullable()
        java.lang.String referer, @org.jetbrains.annotations.NotNull()
        java.util.List<java.lang.String> tags, @org.jetbrains.annotations.Nullable()
        java.lang.String zone, @org.jetbrains.annotations.Nullable()
        java.lang.String contentCreated, @org.jetbrains.annotations.Nullable()
        java.lang.String contentAuthor, @org.jetbrains.annotations.Nullable()
        java.lang.String contentSection, @org.jetbrains.annotations.Nullable()
        java.lang.Boolean contentIsNative, @org.jetbrains.annotations.Nullable()
        io.piano.android.composer.model.CustomParameters customParameters) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public java.lang.String toString() {
            return null;
        }
        
        @java.lang.Override()
        public int hashCode() {
            return 0;
        }
        
        @java.lang.Override()
        public boolean equals(@org.jetbrains.annotations.Nullable()
        java.lang.Object p0) {
            return false;
        }
    }
    
    @kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u000e\u0010\u0007\u001a\u00020\bX\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lio/piano/android/composer/model/ExperienceRequest$Companion;", "", "()V", "DATE_FORMAT", "Ljava/text/DateFormat;", "getDATE_FORMAT", "()Ljava/text/DateFormat;", "ISO_8601", "", "composer_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final java.text.DateFormat getDATE_FORMAT() {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}