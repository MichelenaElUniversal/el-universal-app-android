package io.piano.android.composer.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0007\b\u0007\u0018\u0000*\u0004\b\u0000\u0010\u00012\u00020\u0002B\u001d\u0012\b\b\u0001\u0010\u0003\u001a\u00028\u0000\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\u0002\u0010\u0007R\u0013\u0010\u0003\u001a\u00028\u0000\u00a2\u0006\n\n\u0002\u0010\n\u001a\u0004\b\b\u0010\tR\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\f\u00a8\u0006\r"}, d2 = {"Lio/piano/android/composer/model/Data;", "T", "", "data", "errors", "", "Lio/piano/android/composer/model/ErrorMessage;", "(Ljava/lang/Object;Ljava/util/List;)V", "getData", "()Ljava/lang/Object;", "Ljava/lang/Object;", "getErrors", "()Ljava/util/List;", "composer_debug"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class Data<T extends java.lang.Object> {
    private final T data = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<io.piano.android.composer.model.ErrorMessage> errors = null;
    
    public final T getData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<io.piano.android.composer.model.ErrorMessage> getErrors() {
        return null;
    }
    
    public Data(@com.squareup.moshi.Json(name = "models")
    T data, @org.jetbrains.annotations.NotNull()
    java.util.List<io.piano.android.composer.model.ErrorMessage> errors) {
        super();
    }
}