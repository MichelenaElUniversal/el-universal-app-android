package io.piano.android.composer.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\b\u0007\u0018\u00002\u00020\u0001:\u0001\u000bB\u0017\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u0011\u0010\u0007\u001a\u00020\b8F\u00a2\u0006\u0006\u001a\u0004\b\u0007\u0010\tR\u0011\u0010\n\u001a\u00020\b8F\u00a2\u0006\u0006\u001a\u0004\b\n\u0010\tR\u0012\u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0004\u001a\u00020\u00058\u0006X\u0087\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\f"}, d2 = {"Lio/piano/android/composer/model/DelayBy;", "", "type", "Lio/piano/android/composer/model/DelayBy$DelayType;", "value", "", "(Lio/piano/android/composer/model/DelayBy$DelayType;I)V", "isDelayedByScroll", "", "()Z", "isDelayedByTime", "DelayType", "composer_debug"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class DelayBy {
    @org.jetbrains.annotations.Nullable()
    public final io.piano.android.composer.model.DelayBy.DelayType type = null;
    public final int value = 0;
    
    public final boolean isDelayedByTime() {
        return false;
    }
    
    public final boolean isDelayedByScroll() {
        return false;
    }
    
    public DelayBy(@org.jetbrains.annotations.Nullable()
    io.piano.android.composer.model.DelayBy.DelayType type, int value) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0010\n\u0002\b\u0004\b\u0086\u0001\u0018\u00002\b\u0012\u0004\u0012\u00020\u00000\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002j\u0002\b\u0003j\u0002\b\u0004\u00a8\u0006\u0005"}, d2 = {"Lio/piano/android/composer/model/DelayBy$DelayType;", "", "(Ljava/lang/String;I)V", "TIME", "SCROLL", "composer_debug"})
    public static enum DelayType {
        @com.squareup.moshi.Json(name = "time")
        /*public static final*/ TIME /* = new TIME() */,
        @com.squareup.moshi.Json(name = "scroll")
        /*public static final*/ SCROLL /* = new SCROLL() */;
        
        DelayType() {
        }
    }
}