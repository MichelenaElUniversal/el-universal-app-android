package io.piano.android.composer;

import java.lang.System;

@androidx.annotation.RestrictTo(value = {androidx.annotation.RestrictTo.Scope.LIBRARY})
@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000n\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010$\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0001\u0018\u0000 -2\u00020\u0001:\u0001-B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ!\u0010\u0014\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t0\u00132\u0006\u0010\u0015\u001a\u00020\tH\u0000\u00a2\u0006\u0002\b\u0016JK\u0010\u0017\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t0\u00132\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u001a0\u00192\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\t2\b\u0010\u001e\u001a\u0004\u0018\u00010\t2\b\u0010\u001f\u001a\u0004\u0018\u00010\tH\u0000\u00a2\u0006\u0002\b J3\u0010!\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t0\u00132\u0006\u0010\"\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\t2\b\u0010\u001e\u001a\u0004\u0018\u00010\tH\u0000\u00a2\u0006\u0002\b#J\u0015\u0010$\u001a\u00020%2\u0006\u0010&\u001a\u00020\'H\u0000\u00a2\u0006\u0002\b(J1\u0010)\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t0+0**\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t0+0*H\u0082\bJ\u001e\u0010,\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\t0+0**\u00020\u001cH\u0002RJ\u0010\u000b\u001a>\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u000e \u000f*\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\r0\r \u000f*\u001e\u0012\u0018\u0012\u0016\u0012\u0004\u0012\u00020\u000e \u000f*\n\u0012\u0004\u0012\u00020\u000e\u0018\u00010\r0\r\u0018\u00010\f0\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R2\u0010\u0010\u001a&\u0012\f\u0012\n \u000f*\u0004\u0018\u00010\u00110\u0011 \u000f*\u0012\u0012\f\u0012\n \u000f*\u0004\u0018\u00010\u00110\u0011\u0018\u00010\f0\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\"\u0010\u0012\u001a\u0016\u0012\u0012\u0012\u0010\u0012\u0004\u0012\u00020\t\u0012\u0006\u0012\u0004\u0018\u00010\t0\u00130\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006."}, d2 = {"Lio/piano/android/composer/HttpHelper;", "", "experienceIdsProvider", "Lio/piano/android/composer/ExperienceIdsProvider;", "prefsStorage", "Lio/piano/android/composer/PrefsStorage;", "moshi", "Lcom/squareup/moshi/Moshi;", "userAgent", "", "(Lio/piano/android/composer/ExperienceIdsProvider;Lio/piano/android/composer/PrefsStorage;Lcom/squareup/moshi/Moshi;Ljava/lang/String;)V", "activeMetersAdapter", "Lcom/squareup/moshi/JsonAdapter;", "", "Lio/piano/android/composer/model/ActiveMeter;", "kotlin.jvm.PlatformType", "customParametersAdapter", "Lio/piano/android/composer/model/CustomParameters;", "mapAdapter", "", "buildEventTracking", "trackingId", "buildEventTracking$composer_debug", "buildShowTemplateParameters", "showTemplateEvent", "Lio/piano/android/composer/model/Event;", "Lio/piano/android/composer/model/events/ShowTemplate;", "experienceRequest", "Lio/piano/android/composer/model/ExperienceRequest;", "aid", "userToken", "gaClientId", "buildShowTemplateParameters$composer_debug", "convertExperienceRequest", "request", "convertExperienceRequest$composer_debug", "processExperienceResponse", "", "response", "Lio/piano/android/composer/model/ExperienceResponse;", "processExperienceResponse$composer_debug", "filterNotEmptyValues", "Lkotlin/sequences/Sequence;", "Lkotlin/Pair;", "toMinimalSequence", "Companion", "composer_debug"})
public final class HttpHelper {
    private final com.squareup.moshi.JsonAdapter<java.util.Map<java.lang.String, java.lang.String>> mapAdapter = null;
    private final com.squareup.moshi.JsonAdapter<java.util.List<io.piano.android.composer.model.ActiveMeter>> activeMetersAdapter = null;
    private final com.squareup.moshi.JsonAdapter<io.piano.android.composer.model.CustomParameters> customParametersAdapter = null;
    private final io.piano.android.composer.ExperienceIdsProvider experienceIdsProvider = null;
    private final io.piano.android.composer.PrefsStorage prefsStorage = null;
    private final java.lang.String userAgent = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_AID = "aid";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_DEBUG = "debug";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_USER_AGENT = "user_agent";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_PROTOCOL_VERSION = "protocol_version";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_TIMEZONE_OFFSET = "timezone_offset";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_PAGEVIEW_ID = "pageview_id";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_VISIT_ID = "visit_id";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_NEW_VISIT = "new_visit";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_SUBMIT_TYPE = "submit_type";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_SDK_VERSION = "sdk_version";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_XBUILDER_BROWSER_COOKIE = "xbc";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_TP_BROWSER_COOKIE = "tbc";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_TP_ACCESS_COOKIE = "tac";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_CUSTOM_VARIABLES = "custom_variables";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_USER_TOKEN = "user_token";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_URL = "url";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_REFERRER = "referer";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_TAGS = "tags";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_ZONE = "zone";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_CONTENT_CREATED = "content_created";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_CONTENT_AUTHOR = "content_author";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_CONTENT_SECTION = "content_section";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_CONTENT_NATIVE = "content_is_native";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_CUSTOM_PARAMS = "custom_params";
    private static final int VALUE_PROTOCOL_VERSION = 1;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String VALUE_MANUAL_SUBMIT_TYPE = "manual";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_EVENT_TRACKING_ID = "tracking_id";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_EVENT_TYPE = "event_type";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_EVENT_GROUP_ID = "event_group_id";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String VALUE_EXTERNAL_EVENT_TYPE = "EXTERNAL_EVENT";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String VALUE_CLOSE_GROUP_ID = "close";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_TEMPLATE_ID = "templateId";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_TEMPLATE_VARIANT_ID = "templateVariantId";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_ACTIVE_METERS = "activeMeters";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_DISPLAY_MODE = "displayMode";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_SHOW_TEMPLATE_TRACKING_ID = "trackingId";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_SHOW_TEMPLATE_USER_TOKEN = "userToken";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_SHOW_TEMPLATE_CUSTOM_VARIABLES = "customVariables";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_SHOW_TEMPLATE_CONTENT_AUTHOR = "contentAuthor";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_SHOW_TEMPLATE_CONTENT_SECTION = "contentSection";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_GA_CLIENT_ID = "gaClientId";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_OS = "os";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String VALUE_ANDROID_OS = "android";
    public static final io.piano.android.composer.HttpHelper.Companion Companion = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.Map<java.lang.String, java.lang.String> convertExperienceRequest$composer_debug(@org.jetbrains.annotations.NotNull()
    io.piano.android.composer.model.ExperienceRequest request, @org.jetbrains.annotations.NotNull()
    java.lang.String aid, @org.jetbrains.annotations.Nullable()
    java.lang.String userToken) {
        return null;
    }
    
    public final void processExperienceResponse$composer_debug(@org.jetbrains.annotations.NotNull()
    io.piano.android.composer.model.ExperienceResponse response) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.Map<java.lang.String, java.lang.String> buildEventTracking$composer_debug(@org.jetbrains.annotations.NotNull()
    java.lang.String trackingId) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.Map<java.lang.String, java.lang.String> buildShowTemplateParameters$composer_debug(@org.jetbrains.annotations.NotNull()
    io.piano.android.composer.model.Event<io.piano.android.composer.model.events.ShowTemplate> showTemplateEvent, @org.jetbrains.annotations.NotNull()
    io.piano.android.composer.model.ExperienceRequest experienceRequest, @org.jetbrains.annotations.NotNull()
    java.lang.String aid, @org.jetbrains.annotations.Nullable()
    java.lang.String userToken, @org.jetbrains.annotations.Nullable()
    java.lang.String gaClientId) {
        return null;
    }
    
    private final kotlin.sequences.Sequence<kotlin.Pair<java.lang.String, java.lang.String>> toMinimalSequence(io.piano.android.composer.model.ExperienceRequest $this$toMinimalSequence) {
        return null;
    }
    
    @kotlin.Suppress(names = {"NOTHING_TO_INLINE"})
    private final kotlin.sequences.Sequence<kotlin.Pair<java.lang.String, java.lang.String>> filterNotEmptyValues(kotlin.sequences.Sequence<kotlin.Pair<java.lang.String, java.lang.String>> $this$filterNotEmptyValues) {
        return null;
    }
    
    public HttpHelper(@org.jetbrains.annotations.NotNull()
    io.piano.android.composer.ExperienceIdsProvider experienceIdsProvider, @org.jetbrains.annotations.NotNull()
    io.piano.android.composer.PrefsStorage prefsStorage, @org.jetbrains.annotations.NotNull()
    com.squareup.moshi.Moshi moshi, @org.jetbrains.annotations.NotNull()
    java.lang.String userAgent) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b*\n\u0002\u0010\b\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0019\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001f\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010%\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\'\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010(\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010)\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010*\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010+\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010,\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010-\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010.\u001a\u00020/X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u00060"}, d2 = {"Lio/piano/android/composer/HttpHelper$Companion;", "", "()V", "PARAM_ACTIVE_METERS", "", "PARAM_AID", "PARAM_CONTENT_AUTHOR", "PARAM_CONTENT_CREATED", "PARAM_CONTENT_NATIVE", "PARAM_CONTENT_SECTION", "PARAM_CUSTOM_PARAMS", "PARAM_CUSTOM_VARIABLES", "PARAM_DEBUG", "PARAM_DISPLAY_MODE", "PARAM_EVENT_GROUP_ID", "PARAM_EVENT_TRACKING_ID", "PARAM_EVENT_TYPE", "PARAM_GA_CLIENT_ID", "PARAM_NEW_VISIT", "PARAM_OS", "PARAM_PAGEVIEW_ID", "PARAM_PROTOCOL_VERSION", "PARAM_REFERRER", "PARAM_SDK_VERSION", "PARAM_SHOW_TEMPLATE_CONTENT_AUTHOR", "PARAM_SHOW_TEMPLATE_CONTENT_SECTION", "PARAM_SHOW_TEMPLATE_CUSTOM_VARIABLES", "PARAM_SHOW_TEMPLATE_TRACKING_ID", "PARAM_SHOW_TEMPLATE_USER_TOKEN", "PARAM_SUBMIT_TYPE", "PARAM_TAGS", "PARAM_TEMPLATE_ID", "PARAM_TEMPLATE_VARIANT_ID", "PARAM_TIMEZONE_OFFSET", "PARAM_TP_ACCESS_COOKIE", "PARAM_TP_BROWSER_COOKIE", "PARAM_URL", "PARAM_USER_AGENT", "PARAM_USER_TOKEN", "PARAM_VISIT_ID", "PARAM_XBUILDER_BROWSER_COOKIE", "PARAM_ZONE", "VALUE_ANDROID_OS", "VALUE_CLOSE_GROUP_ID", "VALUE_EXTERNAL_EVENT_TYPE", "VALUE_MANUAL_SUBMIT_TYPE", "VALUE_PROTOCOL_VERSION", "", "composer_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}