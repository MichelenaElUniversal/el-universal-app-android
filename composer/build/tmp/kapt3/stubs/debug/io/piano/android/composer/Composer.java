package io.piano.android.composer;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000b\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0003\n\u0002\b\u0002\u0018\u0000 %2\u00020\u0001:\u0001%B+\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\tJ\u0010\u0010\n\u001a\u00020\u00002\b\u0010\n\u001a\u0004\u0018\u00010\u0007J,\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0014\u0010\u0010\u001a\u0010\u0012\f\u0012\n\u0012\u0006\b\u0001\u0012\u00020\u00130\u00120\u00112\u0006\u0010\u0014\u001a\u00020\u0015J;\u0010\u0016\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0017\u001a\u00020\u00182\u0014\u0010\u0010\u001a\u0010\u0012\f\u0012\n\u0012\u0006\b\u0001\u0012\u00020\u00130\u00120\u00112\u0006\u0010\u0014\u001a\u00020\u0015H\u0000\u00a2\u0006\u0002\b\u0019J\u000e\u0010\u001a\u001a\u00020\r2\u0006\u0010\u001b\u001a\u00020\u0007J\u0010\u0010\u000b\u001a\u00020\u00002\b\u0010\u000b\u001a\u0004\u0018\u00010\u0007J\u001d\u0010\u001c\u001a\u0002H\u001d\"\u0004\b\u0000\u0010\u001d*\b\u0012\u0004\u0012\u0002H\u001d0\u001eH\u0002\u00a2\u0006\u0002\u0010\u001fJ!\u0010 \u001a\b\u0012\u0004\u0012\u00020\u00130!*\b\u0012\u0004\u0012\u00020\u00130!2\u0006\u0010\u000e\u001a\u00020\u000fH\u0082\bJ\r\u0010\"\u001a\u00020#*\u00020$H\u0082\bR\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006&"}, d2 = {"Lio/piano/android/composer/Composer;", "", "api", "Lio/piano/android/composer/Api;", "httpHelper", "Lio/piano/android/composer/HttpHelper;", "aid", "", "customEndpoint", "(Lio/piano/android/composer/Api;Lio/piano/android/composer/HttpHelper;Ljava/lang/String;Ljava/lang/String;)V", "gaClientId", "userToken", "getExperience", "", "request", "Lio/piano/android/composer/model/ExperienceRequest;", "eventTypeListeners", "", "Lio/piano/android/composer/listeners/EventTypeListener;", "Lio/piano/android/composer/model/events/EventType;", "exceptionListener", "Lio/piano/android/composer/listeners/ExceptionListener;", "processExperienceResponse", "response", "Lio/piano/android/composer/model/ExperienceResponse;", "processExperienceResponse$composer_debug", "trackExternalEvent", "trackingId", "bodyOrThrow", "T", "Lretrofit2/Response;", "(Lretrofit2/Response;)Ljava/lang/Object;", "preprocess", "Lio/piano/android/composer/model/Event;", "toComposerException", "Lio/piano/android/composer/ComposerException;", "", "Companion", "composer_debug"})
public final class Composer {
    private java.lang.String userToken;
    private java.lang.String gaClientId;
    private final io.piano.android.composer.Api api = null;
    private final io.piano.android.composer.HttpHelper httpHelper = null;
    private final java.lang.String aid = null;
    private final java.lang.String customEndpoint = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String USER_PROVIDER_TINYPASS_ACCOUNTS = "tinypass_accounts";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String USER_PROVIDER_PIANO_ID = "piano_id";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String USER_PROVIDER_JANRAIN = "janrain";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String BASE_URL_SANDBOX = "https://sandbox.tinypass.com";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String BASE_URL_EXPERIENCE = "https://experience.tinypass.com";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String BASE_URL_BUY = "https://buy.tinypass.com";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String URL_EXPERIENCE_EXECUTE = "/xbuilder/experience/executeMobile";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String URL_TRACK_EXTERNAL_EVENT = "/api/v3/conversion/logAutoMicroConversion";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String URL_TEMPLATE = "/checkout/template/show";
    public static final io.piano.android.composer.Composer.Companion Companion = null;
    
    /**
     * Sets user token, which will be sent at each experience request
     * @param userToken User token
     * @return Composer instance
     */
    @org.jetbrains.annotations.NotNull()
    @kotlin.Suppress(names = {"unused"})
    public final io.piano.android.composer.Composer userToken(@org.jetbrains.annotations.Nullable()
    java.lang.String userToken) {
        return null;
    }
    
    /**
     * Sets Google Analytics client id
     *
     * @param gaClientId Client id
     * @return Composer instance
     */
    @org.jetbrains.annotations.NotNull()
    @kotlin.Suppress(names = {"unused"})
    public final io.piano.android.composer.Composer gaClientId(@org.jetbrains.annotations.Nullable()
    java.lang.String gaClientId) {
        return null;
    }
    
    /**
     * Gets experience from server
     *
     * @param request            Prepared experience request
     * @param eventTypeListeners Collection of event listeners
     * @param exceptionListener  Listener for exceptions
     */
    @kotlin.Suppress(names = {"unused"})
    public final void getExperience(@org.jetbrains.annotations.NotNull()
    io.piano.android.composer.model.ExperienceRequest request, @org.jetbrains.annotations.NotNull()
    java.util.Collection<? extends io.piano.android.composer.listeners.EventTypeListener<? extends io.piano.android.composer.model.events.EventType>> eventTypeListeners, @org.jetbrains.annotations.NotNull()
    io.piano.android.composer.listeners.ExceptionListener exceptionListener) {
    }
    
    /**
     * Tracks external event by id
     *
     * @param trackingId Tracking id
     */
    @kotlin.Suppress(names = {"unused"})
    public final void trackExternalEvent(@org.jetbrains.annotations.NotNull()
    java.lang.String trackingId) {
    }
    
    public final void processExperienceResponse$composer_debug(@org.jetbrains.annotations.NotNull()
    io.piano.android.composer.model.ExperienceRequest request, @org.jetbrains.annotations.NotNull()
    io.piano.android.composer.model.ExperienceResponse response, @org.jetbrains.annotations.NotNull()
    java.util.Collection<? extends io.piano.android.composer.listeners.EventTypeListener<? extends io.piano.android.composer.model.events.EventType>> eventTypeListeners, @org.jetbrains.annotations.NotNull()
    io.piano.android.composer.listeners.ExceptionListener exceptionListener) {
    }
    
    @kotlin.Suppress(names = {"NOTHING_TO_INLINE"})
    private final io.piano.android.composer.model.Event<io.piano.android.composer.model.events.EventType> preprocess(io.piano.android.composer.model.Event<io.piano.android.composer.model.events.EventType> $this$preprocess, io.piano.android.composer.model.ExperienceRequest request) {
        return null;
    }
    
    private final <T extends java.lang.Object>T bodyOrThrow(retrofit2.Response<T> $this$bodyOrThrow) {
        return null;
    }
    
    @kotlin.Suppress(names = {"NOTHING_TO_INLINE"})
    private final io.piano.android.composer.ComposerException toComposerException(java.lang.Throwable $this$toComposerException) {
        return null;
    }
    
    public Composer(@org.jetbrains.annotations.NotNull()
    io.piano.android.composer.Api api, @org.jetbrains.annotations.NotNull()
    io.piano.android.composer.HttpHelper httpHelper, @org.jetbrains.annotations.NotNull()
    java.lang.String aid, @org.jetbrains.annotations.Nullable()
    java.lang.String customEndpoint) {
        super();
    }
    
    /**
     * Initialize Composer
     * @param context Activity or Application context
     * @param aid Your AID
     * @param endpoint Custom API endpoint, null for use "production"
     */
    @kotlin.Suppress(names = {"unused"})
    public static final void init(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String aid, @org.jetbrains.annotations.Nullable()
    java.lang.String endpoint) {
    }
    
    /**
     * Initialize Composer
     * @param context Activity or Application context
     * @param aid Your AID
     * @param endpoint Custom API endpoint, null for use "production"
     */
    @kotlin.Suppress(names = {"unused"})
    public static final void init(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String aid) {
    }
    
    /**
     * Initialize Composer
     * @param context Activity or Application context
     * @param aid Your AID
     * @param sandbox Use sandbox environment if true, production otherwise
     */
    @kotlin.Suppress(names = {"unused"})
    public static final void init(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String aid, boolean sandbox) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @kotlin.Suppress(names = {"unused"})
    public static final io.piano.android.composer.Composer getInstance() {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0010\u001a\u00020\u0011H\u0007J \u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00042\u0006\u0010\u0017\u001a\u00020\u0018H\u0007J$\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00042\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u0004H\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u0014\u0010\n\u001a\u00020\u0004X\u0086T\u00a2\u0006\b\n\u0000\u0012\u0004\b\u000b\u0010\u0002R\u0014\u0010\f\u001a\u00020\u0004X\u0086T\u00a2\u0006\b\n\u0000\u0012\u0004\b\r\u0010\u0002R\u0014\u0010\u000e\u001a\u00020\u0004X\u0086T\u00a2\u0006\b\n\u0000\u0012\u0004\b\u000f\u0010\u0002\u00a8\u0006\u001a"}, d2 = {"Lio/piano/android/composer/Composer$Companion;", "", "()V", "BASE_URL_BUY", "", "BASE_URL_EXPERIENCE", "BASE_URL_SANDBOX", "URL_EXPERIENCE_EXECUTE", "URL_TEMPLATE", "URL_TRACK_EXTERNAL_EVENT", "USER_PROVIDER_JANRAIN", "getUSER_PROVIDER_JANRAIN$annotations", "USER_PROVIDER_PIANO_ID", "getUSER_PROVIDER_PIANO_ID$annotations", "USER_PROVIDER_TINYPASS_ACCOUNTS", "getUSER_PROVIDER_TINYPASS_ACCOUNTS$annotations", "getInstance", "Lio/piano/android/composer/Composer;", "init", "", "context", "Landroid/content/Context;", "aid", "sandbox", "", "endpoint", "composer_debug"})
    public static final class Companion {
        
        /**
         * Initialize Composer
         * @param context Activity or Application context
         * @param aid Your AID
         * @param endpoint Custom API endpoint, null for use "production"
         */
        @kotlin.Suppress(names = {"unused"})
        public final void init(@org.jetbrains.annotations.NotNull()
        android.content.Context context, @org.jetbrains.annotations.NotNull()
        java.lang.String aid, @org.jetbrains.annotations.Nullable()
        java.lang.String endpoint) {
        }
        
        /**
         * Initialize Composer
         * @param context Activity or Application context
         * @param aid Your AID
         * @param endpoint Custom API endpoint, null for use "production"
         */
        @kotlin.Suppress(names = {"unused"})
        public final void init(@org.jetbrains.annotations.NotNull()
        android.content.Context context, @org.jetbrains.annotations.NotNull()
        java.lang.String aid) {
        }
        
        /**
         * Initialize Composer
         * @param context Activity or Application context
         * @param aid Your AID
         * @param sandbox Use sandbox environment if true, production otherwise
         */
        @kotlin.Suppress(names = {"unused"})
        public final void init(@org.jetbrains.annotations.NotNull()
        android.content.Context context, @org.jetbrains.annotations.NotNull()
        java.lang.String aid, boolean sandbox) {
        }
        
        @org.jetbrains.annotations.NotNull()
        @kotlin.Suppress(names = {"unused"})
        public final io.piano.android.composer.Composer getInstance() {
            return null;
        }
        
        @kotlin.Suppress(names = {"unused"})
        @java.lang.Deprecated()
        public static void getUSER_PROVIDER_TINYPASS_ACCOUNTS$annotations() {
        }
        
        @kotlin.Suppress(names = {"unused"})
        @java.lang.Deprecated()
        public static void getUSER_PROVIDER_PIANO_ID$annotations() {
        }
        
        @kotlin.Suppress(names = {"unused"})
        @java.lang.Deprecated()
        public static void getUSER_PROVIDER_JANRAIN$annotations() {
        }
        
        private Companion() {
            super();
        }
    }
}