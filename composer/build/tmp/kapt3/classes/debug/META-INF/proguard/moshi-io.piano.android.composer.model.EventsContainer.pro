-if class io.piano.android.composer.model.EventsContainer
-keepnames class io.piano.android.composer.model.EventsContainer
-if class io.piano.android.composer.model.EventsContainer
-keep class io.piano.android.composer.model.EventsContainerJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
