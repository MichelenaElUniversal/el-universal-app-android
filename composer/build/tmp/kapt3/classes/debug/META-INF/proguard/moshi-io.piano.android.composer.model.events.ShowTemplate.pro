-if class io.piano.android.composer.model.events.ShowTemplate
-keepnames class io.piano.android.composer.model.events.ShowTemplate
-if class io.piano.android.composer.model.events.ShowTemplate
-keep class io.piano.android.composer.model.events.ShowTemplateJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
-if class io.piano.android.composer.model.events.ShowTemplate
-keepnames class kotlin.jvm.internal.DefaultConstructorMarker
-if class io.piano.android.composer.model.events.ShowTemplate
-keepclassmembers class io.piano.android.composer.model.events.ShowTemplate {
    public synthetic <init>(java.lang.String,java.lang.String,io.piano.android.composer.model.events.ShowTemplate$DisplayMode,java.lang.String,io.piano.android.composer.model.DelayBy,boolean,java.lang.String,int,kotlin.jvm.internal.DefaultConstructorMarker);
}
