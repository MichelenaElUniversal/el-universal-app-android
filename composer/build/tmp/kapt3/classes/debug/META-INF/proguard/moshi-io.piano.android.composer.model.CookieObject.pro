-if class io.piano.android.composer.model.CookieObject
-keepnames class io.piano.android.composer.model.CookieObject
-if class io.piano.android.composer.model.CookieObject
-keep class io.piano.android.composer.model.CookieObjectJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
