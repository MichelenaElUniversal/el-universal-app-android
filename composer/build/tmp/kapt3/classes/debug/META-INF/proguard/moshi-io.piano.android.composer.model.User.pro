-if class io.piano.android.composer.model.User
-keepnames class io.piano.android.composer.model.User
-if class io.piano.android.composer.model.User
-keep class io.piano.android.composer.model.UserJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
