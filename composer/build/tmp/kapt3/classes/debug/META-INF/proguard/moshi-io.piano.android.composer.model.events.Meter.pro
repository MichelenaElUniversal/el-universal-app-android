-if class io.piano.android.composer.model.events.Meter
-keepnames class io.piano.android.composer.model.events.Meter
-if class io.piano.android.composer.model.events.Meter
-keep class io.piano.android.composer.model.events.MeterJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
-if class io.piano.android.composer.model.events.Meter
-keepnames class kotlin.jvm.internal.DefaultConstructorMarker
-if class io.piano.android.composer.model.events.Meter
-keepclassmembers class io.piano.android.composer.model.events.Meter {
    public synthetic <init>(int,int,int,int,io.piano.android.composer.model.events.Meter$MeterState,int,kotlin.jvm.internal.DefaultConstructorMarker);
}
