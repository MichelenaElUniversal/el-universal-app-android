-if class io.piano.android.composer.model.EventExecutionContext
-keepnames class io.piano.android.composer.model.EventExecutionContext
-if class io.piano.android.composer.model.EventExecutionContext
-keep class io.piano.android.composer.model.EventExecutionContextJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
