-if class io.piano.android.composer.model.ExperienceResponse
-keepnames class io.piano.android.composer.model.ExperienceResponse
-if class io.piano.android.composer.model.ExperienceResponse
-keep class io.piano.android.composer.model.ExperienceResponseJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
