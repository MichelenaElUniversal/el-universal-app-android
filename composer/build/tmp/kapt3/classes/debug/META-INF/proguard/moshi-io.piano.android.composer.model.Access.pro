-if class io.piano.android.composer.model.Access
-keepnames class io.piano.android.composer.model.Access
-if class io.piano.android.composer.model.Access
-keep class io.piano.android.composer.model.AccessJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
-if class io.piano.android.composer.model.Access
-keepnames class kotlin.jvm.internal.DefaultConstructorMarker
-if class io.piano.android.composer.model.Access
-keepclassmembers class io.piano.android.composer.model.Access {
    public synthetic <init>(java.lang.String,java.lang.String,int,int,int,kotlin.jvm.internal.DefaultConstructorMarker);
}
