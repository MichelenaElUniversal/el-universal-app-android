-if class io.piano.android.composer.model.ErrorMessage
-keepnames class io.piano.android.composer.model.ErrorMessage
-if class io.piano.android.composer.model.ErrorMessage
-keep class io.piano.android.composer.model.ErrorMessageJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
