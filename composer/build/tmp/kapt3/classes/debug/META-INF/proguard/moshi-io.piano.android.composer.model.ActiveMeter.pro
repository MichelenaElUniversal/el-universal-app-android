-if class io.piano.android.composer.model.ActiveMeter
-keepnames class io.piano.android.composer.model.ActiveMeter
-if class io.piano.android.composer.model.ActiveMeter
-keep class io.piano.android.composer.model.ActiveMeterJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
