-if class io.piano.android.composer.model.EventModuleParams
-keepnames class io.piano.android.composer.model.EventModuleParams
-if class io.piano.android.composer.model.EventModuleParams
-keep class io.piano.android.composer.model.EventModuleParamsJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
