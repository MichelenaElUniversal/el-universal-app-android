-if class io.piano.android.composer.model.Data
-keepnames class io.piano.android.composer.model.Data
-if class io.piano.android.composer.model.Data
-keep class io.piano.android.composer.model.DataJsonAdapter {
    public <init>(com.squareup.moshi.Moshi,java.lang.reflect.Type[]);
}
