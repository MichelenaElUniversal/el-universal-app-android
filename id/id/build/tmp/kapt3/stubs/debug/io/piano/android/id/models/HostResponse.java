package io.piano.android.id.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010$\n\u0002\b\u0004\b\u0007\u0018\u00002\u00020\u0001B7\u0012\b\b\u0001\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0003\u0012\u0014\u0010\u0007\u001a\u0010\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0003\u0018\u00010\b\u00a2\u0006\u0002\u0010\tR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000b\u00a8\u0006\f"}, d2 = {"Lio/piano/android/id/models/HostResponse;", "Lio/piano/android/id/models/BaseResponse;", "host", "", "code", "", "message", "validationErrors", "", "(Ljava/lang/String;ILjava/lang/String;Ljava/util/Map;)V", "getHost", "()Ljava/lang/String;", "id_debug"})
@com.squareup.moshi.JsonClass(generateAdapter = true)
public final class HostResponse extends io.piano.android.id.models.BaseResponse {
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String host = null;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getHost() {
        return null;
    }
    
    public HostResponse(@org.jetbrains.annotations.NotNull()
    @com.squareup.moshi.Json(name = "data")
    java.lang.String host, int code, @org.jetbrains.annotations.Nullable()
    java.lang.String message, @org.jetbrains.annotations.Nullable()
    java.util.Map<java.lang.String, java.lang.String> validationErrors) {
        super(0, null, null);
    }
}