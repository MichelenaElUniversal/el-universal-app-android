package io.piano.android.id;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b`\u0018\u00002\u00020\u0001J\b\u0010\u0002\u001a\u00020\u0003H\'J\u0012\u0010\u0004\u001a\u00020\u00032\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\'J\u0012\u0010\u0007\u001a\u00020\u00032\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\'\u00a8\u0006\b"}, d2 = {"Lio/piano/android/id/PianoIdJsInterface;", "", "cancel", "", "loginSuccess", "payload", "", "socialLogin", "id_debug"})
public abstract interface PianoIdJsInterface {
    
    @android.webkit.JavascriptInterface()
    public abstract void socialLogin(@org.jetbrains.annotations.Nullable()
    java.lang.String payload);
    
    @android.webkit.JavascriptInterface()
    public abstract void loginSuccess(@org.jetbrains.annotations.Nullable()
    java.lang.String payload);
    
    @android.webkit.JavascriptInterface()
    public abstract void cancel();
}