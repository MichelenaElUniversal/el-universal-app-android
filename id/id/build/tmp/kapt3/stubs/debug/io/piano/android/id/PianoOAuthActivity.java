package io.piano.android.id;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0003\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b&\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\n\u001a\u00020\u000b2\b\u0010\f\u001a\u0004\u0018\u00010\rH\u0015J\u0010\u0010\u000e\u001a\u00020\u000b2\u0006\u0010\u000f\u001a\u00020\u0010H\u0014J\u001a\u0010\u0011\u001a\u00020\u000b2\u0006\u0010\u0012\u001a\u00020\u00132\b\u0010\u0014\u001a\u0004\u0018\u00010\u0013H\u0014R$\u0010\u0003\u001a\u00020\u00048\u0000@\u0000X\u0081.\u00a2\u0006\u0014\n\u0000\u0012\u0004\b\u0005\u0010\u0002\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\t\u00a8\u0006\u0015"}, d2 = {"Lio/piano/android/id/PianoOAuthActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "()V", "client", "Lio/piano/android/id/PianoIdClient;", "getClient$id_debug$annotations", "getClient$id_debug", "()Lio/piano/android/id/PianoIdClient;", "setClient$id_debug", "(Lio/piano/android/id/PianoIdClient;)V", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "setFailureResult", "throwable", "", "setSuccessResult", "providerName", "", "token", "id_debug"})
public abstract class PianoOAuthActivity extends androidx.appcompat.app.AppCompatActivity {
    @org.jetbrains.annotations.NotNull()
    @androidx.annotation.RestrictTo(value = {androidx.annotation.RestrictTo.Scope.LIBRARY})
    public io.piano.android.id.PianoIdClient client;
    
    @androidx.annotation.VisibleForTesting()
    @java.lang.Deprecated()
    public static void getClient$id_debug$annotations() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final io.piano.android.id.PianoIdClient getClient$id_debug() {
        return null;
    }
    
    public final void setClient$id_debug(@org.jetbrains.annotations.NotNull()
    io.piano.android.id.PianoIdClient p0) {
    }
    
    @androidx.annotation.CallSuper()
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    /**
     * Sets success data for Piano ID OAuth
     *
     * @param providerName Provider name
     * @param token        Provider token
     */
    protected void setSuccessResult(@org.jetbrains.annotations.NotNull()
    java.lang.String providerName, @org.jetbrains.annotations.Nullable()
    java.lang.String token) {
    }
    
    /**
     * Sets failure data for Piano ID OAuth
     *
     * @param throwable Throwable, that will be processed
     */
    protected void setFailureResult(@org.jetbrains.annotations.NotNull()
    java.lang.Throwable throwable) {
    }
    
    public PianoOAuthActivity() {
        super();
    }
}