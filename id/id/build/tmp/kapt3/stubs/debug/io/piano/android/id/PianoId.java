package io.piano.android.id;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"}, d2 = {"Lio/piano/android/id/PianoId;", "", "()V", "Companion", "id_debug"})
public final class PianoId {
    @androidx.annotation.RestrictTo(value = {androidx.annotation.RestrictTo.Scope.LIBRARY})
    private static io.piano.android.id.PianoIdClient client;
    
    /**
     * Default production endpoint
     */
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ENDPOINT_PRODUCTION = "https://buy.tinypass.com";
    
    /**
     * Australia production endpoint
     */
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ENDPOINT_PRODUCTION_AUSTRALIA = "https://buy-au.piano.io";
    
    /**
     * Asia/Pacific production endpoint
     */
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ENDPOINT_PRODUCTION_ASIA_PACIFIC = "https://buy-ap.piano.io";
    
    /**
     * Sandbox endpoint
     */
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ENDPOINT_SANDBOX = "https://sandbox.tinypass.com";
    
    /**
     * Widget for login screen
     */
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String WIDGET_LOGIN = "login";
    
    /**
     * Widget for registration screen
     */
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String WIDGET_REGISTER = "register";
    
    /**
     * Client ID key for OAuth providers
     */
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String KEY_CLIENT_ID = "io.piano.android.id.CLIENT_ID";
    
    /**
     * Activity result code for error
     */
    public static final int RESULT_ERROR = 1;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String KEY_OAUTH_PROVIDER_NAME = "io.piano.android.id.OAUTH_PROVIDER_NAME";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String KEY_OAUTH_PROVIDER_TOKEN = "io.piano.android.id.OAUTH_PROVIDER_TOKEN";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String KEY_TOKEN = "io.piano.android.id.PianoIdActivity.TOKEN";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String KEY_ERROR = "io.piano.android.id.PianoIdActivity.ERROR";
    private static final java.lang.String NOT_INITIALIZED_MSG = "Piano ID SDK is not initialized! Make sure that you initialize it via init()";
    public static final io.piano.android.id.PianoId.Companion Companion = null;
    
    public PianoId() {
        super();
    }
    
    /**
     * Initialize {@link PianoIdClient} singleton instance. It doesn't re-init it at next calls.
     *
     * @param endpoint Endpoint, which will be used. For example, {@link #ENDPOINT_PRODUCTION},
     *                {@link #ENDPOINT_SANDBOX} or your custom endpoint
     * @param aid      Your AID
     * @return {@link PianoIdClient} instance.
     */
    @org.jetbrains.annotations.NotNull()
    @kotlin.Suppress(names = {"unused"})
    public static final io.piano.android.id.PianoIdClient init(@org.jetbrains.annotations.NotNull()
    java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
    java.lang.String aid) {
        return null;
    }
    
    /**
     * Gets preferences for authorization process
     *
     * @return {@link PianoIdClient.SignInContext} instance
     * @throws IllegalStateException If you call it before {@link #init(String, String)}
     */
    @org.jetbrains.annotations.NotNull()
    @kotlin.Suppress(names = {"unused"})
    public static final io.piano.android.id.PianoIdClient.SignInContext signIn() throws java.lang.IllegalStateException {
        return null;
    }
    
    /**
     * Sign out user by it's token
     *
     * @param accessToken User access token
     * @param callback    callback, which will receive sign-out result
     */
    @kotlin.Suppress(names = {"unused"})
    public static final void signOut(@org.jetbrains.annotations.NotNull()
    java.lang.String accessToken, @org.jetbrains.annotations.Nullable()
    kotlin.jvm.functions.Function1<? super kotlin.Result<? extends java.lang.Object>, kotlin.Unit> callback) {
    }
    
    /**
     * Sign out user by it's token
     *
     * @param accessToken User access token
     * @param callback    callback, which will receive sign-out result
     */
    @kotlin.Suppress(names = {"unused"})
    public static final void signOut(@org.jetbrains.annotations.NotNull()
    java.lang.String accessToken) {
    }
    
    /**
     * Refresh user access token
     *
     * @param refreshToken User refresh token
     * @param callback callback, which will receive result
     */
    @kotlin.Suppress(names = {"unused"})
    public static final void refreshToken(@org.jetbrains.annotations.NotNull()
    java.lang.String refreshToken, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super kotlin.Result<io.piano.android.id.models.PianoIdToken>, kotlin.Unit> callback) {
    }
    
    /**
     * Extracts {@link PianoIdToken} from result {@link Intent}.
     * Use it in {@link android.app.Activity#onActivityResult(int, int, Intent)}
     *
     * @param intent Intent, which you get
     * @return {@link PianoIdToken} instance, if intent contains it
     * @throws PianoIdException if intent contains authorization error
     */
    @org.jetbrains.annotations.Nullable()
    @kotlin.Suppress(names = {"unused"})
    public static final io.piano.android.id.models.PianoIdToken getPianoIdTokenResult(@org.jetbrains.annotations.Nullable()
    android.content.Intent $this$getPianoIdTokenResult) throws io.piano.android.id.PianoIdException, java.lang.IllegalStateException {
        return null;
    }
    
    @kotlin.Suppress(names = {"unused"})
    public static final void parsePianoIdToken(@org.jetbrains.annotations.Nullable()
    android.net.Uri $this$parsePianoIdToken, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super kotlin.Result<io.piano.android.id.models.PianoIdToken>, kotlin.Unit> callback) {
    }
    
    @kotlin.Suppress(names = {"unused"})
    public static final boolean isPianoIdUri(@org.jetbrains.annotations.Nullable()
    android.net.Uri $this$isPianoIdUri) {
        return false;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final io.piano.android.id.PianoIdClient getClient$id_debug() {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u000f\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\r\u0010\u001d\u001a\u00020\u001bH\u0001\u00a2\u0006\u0002\b\u001eJ\u0018\u0010\u001f\u001a\u00020\u001b2\u0006\u0010 \u001a\u00020\u00042\u0006\u0010!\u001a\u00020\u0004H\u0007J7\u0010\"\u001a\u00020#2\u0006\u0010\"\u001a\u00020\u00042\"\u0010$\u001a\u001e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\'0&\u0012\u0004\u0012\u00020#0%j\b\u0012\u0004\u0012\u00020\'`(H\u0007\u00f8\u0001\u0000J\b\u0010)\u001a\u00020*H\u0007J=\u0010+\u001a\u00020#2\u0006\u0010,\u001a\u00020\u00042(\b\u0002\u0010$\u001a\"\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00010&\u0012\u0004\u0012\u00020#\u0018\u00010%j\n\u0012\u0004\u0012\u00020\u0001\u0018\u0001`(H\u0007\u00f8\u0001\u0000J\u0010\u0010-\u001a\u0004\u0018\u00010\'*\u0004\u0018\u00010.H\u0007J\u000e\u0010/\u001a\u000200*\u0004\u0018\u000101H\u0007J5\u00102\u001a\u00020#*\u0004\u0018\u0001012\"\u0010$\u001a\u001e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\'0&\u0012\u0004\u0012\u00020#0%j\b\u0012\u0004\u0012\u00020\'`(H\u0007\u00f8\u0001\u0000R\u0014\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\b\n\u0000\u0012\u0004\b\u0005\u0010\u0002R\u0014\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\b\n\u0000\u0012\u0004\b\u0007\u0010\u0002R\u0014\u0010\b\u001a\u00020\u0004X\u0086T\u00a2\u0006\b\n\u0000\u0012\u0004\b\t\u0010\u0002R\u0014\u0010\n\u001a\u00020\u0004X\u0086T\u00a2\u0006\b\n\u0000\u0012\u0004\b\u000b\u0010\u0002R\u0014\u0010\f\u001a\u00020\u0004X\u0086T\u00a2\u0006\b\n\u0000\u0012\u0004\b\r\u0010\u0002R\u000e\u0010\u000e\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0013\u001a\u00020\u0014X\u0086T\u00a2\u0006\b\n\u0000\u0012\u0004\b\u0015\u0010\u0002R\u0014\u0010\u0016\u001a\u00020\u0004X\u0086T\u00a2\u0006\b\n\u0000\u0012\u0004\b\u0017\u0010\u0002R\u0014\u0010\u0018\u001a\u00020\u0004X\u0086T\u00a2\u0006\b\n\u0000\u0012\u0004\b\u0019\u0010\u0002R\u001a\u0010\u001a\u001a\u0004\u0018\u00010\u001b8\u0002@\u0002X\u0083\u000e\u00a2\u0006\b\n\u0000\u0012\u0004\b\u001c\u0010\u0002\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u00063"}, d2 = {"Lio/piano/android/id/PianoId$Companion;", "", "()V", "ENDPOINT_PRODUCTION", "", "getENDPOINT_PRODUCTION$annotations", "ENDPOINT_PRODUCTION_ASIA_PACIFIC", "getENDPOINT_PRODUCTION_ASIA_PACIFIC$annotations", "ENDPOINT_PRODUCTION_AUSTRALIA", "getENDPOINT_PRODUCTION_AUSTRALIA$annotations", "ENDPOINT_SANDBOX", "getENDPOINT_SANDBOX$annotations", "KEY_CLIENT_ID", "getKEY_CLIENT_ID$annotations", "KEY_ERROR", "KEY_OAUTH_PROVIDER_NAME", "KEY_OAUTH_PROVIDER_TOKEN", "KEY_TOKEN", "NOT_INITIALIZED_MSG", "RESULT_ERROR", "", "getRESULT_ERROR$annotations", "WIDGET_LOGIN", "getWIDGET_LOGIN$annotations", "WIDGET_REGISTER", "getWIDGET_REGISTER$annotations", "client", "Lio/piano/android/id/PianoIdClient;", "getClient$annotations", "getClient", "getClient$id_debug", "init", "endpoint", "aid", "refreshToken", "", "callback", "Lkotlin/Function1;", "Lkotlin/Result;", "Lio/piano/android/id/models/PianoIdToken;", "Lio/piano/android/id/PianoIdFuncCallback;", "signIn", "Lio/piano/android/id/PianoIdClient$SignInContext;", "signOut", "accessToken", "getPianoIdTokenResult", "Landroid/content/Intent;", "isPianoIdUri", "", "Landroid/net/Uri;", "parsePianoIdToken", "id_debug"})
    public static final class Companion {
        
        @androidx.annotation.VisibleForTesting()
        @java.lang.Deprecated()
        private static void getClient$annotations() {
        }
        
        /**
         * Initialize {@link PianoIdClient} singleton instance. It doesn't re-init it at next calls.
         *
         * @param endpoint Endpoint, which will be used. For example, {@link #ENDPOINT_PRODUCTION},
         *                {@link #ENDPOINT_SANDBOX} or your custom endpoint
         * @param aid      Your AID
         * @return {@link PianoIdClient} instance.
         */
        @org.jetbrains.annotations.NotNull()
        @kotlin.Suppress(names = {"unused"})
        public final io.piano.android.id.PianoIdClient init(@org.jetbrains.annotations.NotNull()
        java.lang.String endpoint, @org.jetbrains.annotations.NotNull()
        java.lang.String aid) {
            return null;
        }
        
        /**
         * Gets preferences for authorization process
         *
         * @return {@link PianoIdClient.SignInContext} instance
         * @throws IllegalStateException If you call it before {@link #init(String, String)}
         */
        @org.jetbrains.annotations.NotNull()
        @kotlin.Suppress(names = {"unused"})
        public final io.piano.android.id.PianoIdClient.SignInContext signIn() throws java.lang.IllegalStateException {
            return null;
        }
        
        /**
         * Sign out user by it's token
         *
         * @param accessToken User access token
         * @param callback    callback, which will receive sign-out result
         */
        @kotlin.Suppress(names = {"unused"})
        public final void signOut(@org.jetbrains.annotations.NotNull()
        java.lang.String accessToken, @org.jetbrains.annotations.Nullable()
        kotlin.jvm.functions.Function1<? super kotlin.Result<? extends java.lang.Object>, kotlin.Unit> callback) {
        }
        
        /**
         * Sign out user by it's token
         *
         * @param accessToken User access token
         * @param callback    callback, which will receive sign-out result
         */
        @kotlin.Suppress(names = {"unused"})
        public final void signOut(@org.jetbrains.annotations.NotNull()
        java.lang.String accessToken) {
        }
        
        /**
         * Refresh user access token
         *
         * @param refreshToken User refresh token
         * @param callback callback, which will receive result
         */
        @kotlin.Suppress(names = {"unused"})
        public final void refreshToken(@org.jetbrains.annotations.NotNull()
        java.lang.String refreshToken, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super kotlin.Result<io.piano.android.id.models.PianoIdToken>, kotlin.Unit> callback) {
        }
        
        /**
         * Extracts {@link PianoIdToken} from result {@link Intent}.
         * Use it in {@link android.app.Activity#onActivityResult(int, int, Intent)}
         *
         * @param intent Intent, which you get
         * @return {@link PianoIdToken} instance, if intent contains it
         * @throws PianoIdException if intent contains authorization error
         */
        @org.jetbrains.annotations.Nullable()
        @kotlin.Suppress(names = {"unused"})
        public final io.piano.android.id.models.PianoIdToken getPianoIdTokenResult(@org.jetbrains.annotations.Nullable()
        android.content.Intent $this$getPianoIdTokenResult) throws io.piano.android.id.PianoIdException, java.lang.IllegalStateException {
            return null;
        }
        
        @kotlin.Suppress(names = {"unused"})
        public final void parsePianoIdToken(@org.jetbrains.annotations.Nullable()
        android.net.Uri $this$parsePianoIdToken, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super kotlin.Result<io.piano.android.id.models.PianoIdToken>, kotlin.Unit> callback) {
        }
        
        @kotlin.Suppress(names = {"unused"})
        public final boolean isPianoIdUri(@org.jetbrains.annotations.Nullable()
        android.net.Uri $this$isPianoIdUri) {
            return false;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final io.piano.android.id.PianoIdClient getClient$id_debug() {
            return null;
        }
        
        /**
         * Default production endpoint
         */
        @kotlin.Suppress(names = {"unused"})
        @java.lang.Deprecated()
        public static void getENDPOINT_PRODUCTION$annotations() {
        }
        
        /**
         * Australia production endpoint
         */
        @kotlin.Suppress(names = {"unused"})
        @java.lang.Deprecated()
        public static void getENDPOINT_PRODUCTION_AUSTRALIA$annotations() {
        }
        
        /**
         * Asia/Pacific production endpoint
         */
        @kotlin.Suppress(names = {"unused"})
        @java.lang.Deprecated()
        public static void getENDPOINT_PRODUCTION_ASIA_PACIFIC$annotations() {
        }
        
        /**
         * Sandbox endpoint
         */
        @kotlin.Suppress(names = {"unused"})
        @java.lang.Deprecated()
        public static void getENDPOINT_SANDBOX$annotations() {
        }
        
        /**
         * Widget for login screen
         */
        @kotlin.Suppress(names = {"unused"})
        @java.lang.Deprecated()
        public static void getWIDGET_LOGIN$annotations() {
        }
        
        /**
         * Widget for registration screen
         */
        @kotlin.Suppress(names = {"unused"})
        @java.lang.Deprecated()
        public static void getWIDGET_REGISTER$annotations() {
        }
        
        /**
         * Client ID key for OAuth providers
         */
        @kotlin.Suppress(names = {"unused"})
        @java.lang.Deprecated()
        public static void getKEY_CLIENT_ID$annotations() {
        }
        
        /**
         * Activity result code for error
         */
        @kotlin.Suppress(names = {"unused"})
        @java.lang.Deprecated()
        public static void getRESULT_ERROR$annotations() {
        }
        
        private Companion() {
            super();
        }
    }
}