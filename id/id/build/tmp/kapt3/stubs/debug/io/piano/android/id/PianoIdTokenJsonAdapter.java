package io.piano.android.id;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\t\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u001c2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001cB\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u0010\u0010\u0014\u001a\u00020\u00022\u0006\u0010\u0015\u001a\u00020\u0016H\u0016J\u001a\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001a2\b\u0010\u001b\u001a\u0004\u0018\u00010\u0002H\u0016R!\u0010\u0006\u001a\b\u0012\u0004\u0012\u00020\u00070\u00018BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\n\u0010\u000b\u001a\u0004\b\b\u0010\tR!\u0010\f\u001a\b\u0012\u0004\u0012\u00020\r0\u00018BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u000f\u0010\u000b\u001a\u0004\b\u000e\u0010\tR\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00130\u0001X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"}, d2 = {"Lio/piano/android/id/PianoIdTokenJsonAdapter;", "Lcom/squareup/moshi/JsonAdapter;", "Lio/piano/android/id/models/PianoIdToken;", "moshi", "Lcom/squareup/moshi/Moshi;", "(Lcom/squareup/moshi/Moshi;)V", "jwtAdapter", "Lio/piano/android/id/models/TokenData;", "getJwtAdapter", "()Lcom/squareup/moshi/JsonAdapter;", "jwtAdapter$delegate", "Lkotlin/Lazy;", "longAdapter", "", "getLongAdapter", "longAdapter$delegate", "options", "Lcom/squareup/moshi/JsonReader$Options;", "stringAdapter", "", "fromJson", "reader", "Lcom/squareup/moshi/JsonReader;", "toJson", "", "writer", "Lcom/squareup/moshi/JsonWriter;", "value", "Companion", "id_debug"})
public final class PianoIdTokenJsonAdapter extends com.squareup.moshi.JsonAdapter<io.piano.android.id.models.PianoIdToken> {
    private final com.squareup.moshi.JsonReader.Options options = null;
    private final com.squareup.moshi.JsonAdapter<java.lang.String> stringAdapter = null;
    private final kotlin.Lazy longAdapter$delegate = null;
    private final kotlin.Lazy jwtAdapter$delegate = null;
    private static final java.lang.String ACCESS_TOKEN = "access_token";
    private static final java.lang.String ACCESS_TOKEN_CAMEL = "accessToken";
    private static final java.lang.String REFRESH_TOKEN = "refresh_token";
    private static final java.lang.String REFRESH_TOKEN_CAMEL = "refreshToken";
    private static final java.lang.String EXPIRES_IN_CAMEL = "expiresIn";
    @org.jetbrains.annotations.NotNull()
    private static final com.squareup.moshi.JsonAdapter.Factory FACTORY = null;
    public static final io.piano.android.id.PianoIdTokenJsonAdapter.Companion Companion = null;
    
    private final com.squareup.moshi.JsonAdapter<java.lang.Long> getLongAdapter() {
        return null;
    }
    
    private final com.squareup.moshi.JsonAdapter<io.piano.android.id.models.TokenData> getJwtAdapter() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public io.piano.android.id.models.PianoIdToken fromJson(@org.jetbrains.annotations.NotNull()
    com.squareup.moshi.JsonReader reader) {
        return null;
    }
    
    @java.lang.Override()
    public void toJson(@org.jetbrains.annotations.NotNull()
    com.squareup.moshi.JsonWriter writer, @org.jetbrains.annotations.Nullable()
    io.piano.android.id.models.PianoIdToken value) {
    }
    
    public PianoIdTokenJsonAdapter(@org.jetbrains.annotations.NotNull()
    com.squareup.moshi.Moshi moshi) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0007\u001a\u00020\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u000e\u0010\u000b\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"}, d2 = {"Lio/piano/android/id/PianoIdTokenJsonAdapter$Companion;", "", "()V", "ACCESS_TOKEN", "", "ACCESS_TOKEN_CAMEL", "EXPIRES_IN_CAMEL", "FACTORY", "Lcom/squareup/moshi/JsonAdapter$Factory;", "getFACTORY", "()Lcom/squareup/moshi/JsonAdapter$Factory;", "REFRESH_TOKEN", "REFRESH_TOKEN_CAMEL", "id_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final com.squareup.moshi.JsonAdapter.Factory getFACTORY() {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}