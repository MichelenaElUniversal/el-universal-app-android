package io.piano.android.id;

import java.lang.System;

/**
 * Callback interface
 * @param <T> Type of success data
 */
@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\bf\u0018\u0000 \n*\u0004\b\u0000\u0010\u00012\u00020\u0002:\u0001\nJ\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0017J\u0015\u0010\u0007\u001a\u00020\u00042\u0006\u0010\b\u001a\u00028\u0000H\u0017\u00a2\u0006\u0002\u0010\t\u00f8\u0001\u0000\u0082\u0002\u0007\n\u0005\b\u0091(0\u0001\u00a8\u0006\u000b"}, d2 = {"Lio/piano/android/id/PianoIdCallback;", "T", "", "onFailure", "", "exception", "Lio/piano/android/id/PianoIdException;", "onSuccess", "data", "(Ljava/lang/Object;)V", "Companion", "id_debug"})
public abstract interface PianoIdCallback<T extends java.lang.Object> {
    public static final io.piano.android.id.PianoIdCallback.Companion Companion = null;
    
    /**
     * Called when operation has completed successfully.
     *
     * @param data result data
     */
    public void onSuccess(T data) {
    }
    
    /**
     * Called when operation has completed with error.
     * @param exception thrown exception
     */
    public void onFailure(@org.jetbrains.annotations.NotNull()
    io.piano.android.id.PianoIdException exception) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public static <T extends java.lang.Object>kotlin.jvm.functions.Function1<kotlin.Result<? extends T>, kotlin.Unit> asResultCallback(@org.jetbrains.annotations.NotNull()
    io.piano.android.id.PianoIdCallback<T> $this$asResultCallback) {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J7\u0010\u0003\u001a\u001e\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00060\u0005\u0012\u0004\u0012\u00020\u00070\u0004j\b\u0012\u0004\u0012\u0002H\u0006`\b\"\u0004\b\u0001\u0010\u0006*\b\u0012\u0004\u0012\u0002H\u00060\tH\u0007\u00f8\u0001\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\n"}, d2 = {"Lio/piano/android/id/PianoIdCallback$Companion;", "", "()V", "asResultCallback", "Lkotlin/Function1;", "Lkotlin/Result;", "T", "", "Lio/piano/android/id/PianoIdFuncCallback;", "Lio/piano/android/id/PianoIdCallback;", "id_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final <T extends java.lang.Object>kotlin.jvm.functions.Function1<kotlin.Result<? extends T>, kotlin.Unit> asResultCallback(@org.jetbrains.annotations.NotNull()
        io.piano.android.id.PianoIdCallback<T> $this$asResultCallback) {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}