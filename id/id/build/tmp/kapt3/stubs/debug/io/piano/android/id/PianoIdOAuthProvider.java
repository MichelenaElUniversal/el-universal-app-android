package io.piano.android.id;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0018\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bH&R\u0012\u0010\u0002\u001a\u00020\u0003X\u00a6\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0004\u0010\u0005\u00a8\u0006\f"}, d2 = {"Lio/piano/android/id/PianoIdOAuthProvider;", "", "name", "", "getName", "()Ljava/lang/String;", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "extras", "Landroid/os/Bundle;", "id_debug"})
public abstract interface PianoIdOAuthProvider {
    
    /**
     * Get OAuth-provider name
     *
     * @return OAuth-provider name
     */
    @org.jetbrains.annotations.NotNull()
    public abstract java.lang.String getName();
    
    /**
     * Gets intent for starting sign in process
     *
     * @param context Context for building instance
     * @param extras  Bundle, which will be added to intent before start. You can modify, if you want
     * @return `Intent` instance for starting
     */
    @org.jetbrains.annotations.NotNull()
    public abstract android.content.Intent buildIntent(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    android.os.Bundle extras);
}