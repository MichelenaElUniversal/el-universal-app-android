package io.piano.android.id;

import java.lang.System;

/**
 * Piano ID authorization
 */
@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\u00b8\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010%\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u0000 f2\u00020\u0001:\u0002fgB\u001f\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\u001d\u00103\u001a\u00020\u00072\u0006\u00104\u001a\u00020\u00072\u0006\u00105\u001a\u00020\u0007H\u0000\u00a2\u0006\u0002\b6J\u001d\u00107\u001a\u0002082\u0006\u00109\u001a\u00020:2\u0006\u0010;\u001a\u00020\u0007H\u0000\u00a2\u0006\u0002\b<J\u0015\u0010=\u001a\u00020\u00192\u0006\u0010;\u001a\u00020\u0007H\u0000\u00a2\u0006\u0002\b>J4\u0010?\u001a\u00020.2\"\u0010@\u001a\u001e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0-\u0012\u0004\u0012\u00020.0,j\b\u0012\u0004\u0012\u00020\n`/H\u0000\u00f8\u0001\u0000\u00a2\u0006\u0002\b\u000bJ\u0019\u0010A\u001a\u0004\u0018\u00010\u00192\b\u0010B\u001a\u0004\u0018\u000108H\u0000\u00a2\u0006\u0002\bCJF\u0010D\u001a\u00020.2\u0006\u0010E\u001a\u00020F2\b\u0010G\u001a\u0004\u0018\u00010\u00072\"\u0010@\u001a\u001e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070-\u0012\u0004\u0012\u00020.0,j\b\u0012\u0004\u0012\u00020\u0007`/H\u0000\u00f8\u0001\u0000\u00a2\u0006\u0002\bHJ\u0017\u0010I\u001a\u0004\u0018\u00010\u00112\u0006\u0010J\u001a\u00020KH\u0000\u00a2\u0006\u0002\bLJ<\u0010M\u001a\u00020.2\u0006\u0010N\u001a\u00020\u00072\"\u0010@\u001a\u001e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00190-\u0012\u0004\u0012\u00020.0,j\b\u0012\u0004\u0012\u00020\u0019`/H\u0000\u00f8\u0001\u0000\u00a2\u0006\u0002\bOJ<\u0010P\u001a\u00020.2\u0006\u0010Q\u001a\u00020R2\"\u0010@\u001a\u001e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00190-\u0012\u0004\u0012\u00020.0,j\b\u0012\u0004\u0012\u00020\u0019`/H\u0000\u00f8\u0001\u0000\u00a2\u0006\u0002\bSJ<\u0010T\u001a\u00020.2\u0006\u0010T\u001a\u00020\u00072\"\u0010@\u001a\u001e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00190-\u0012\u0004\u0012\u00020.0,j\b\u0012\u0004\u0012\u00020\u0019`/H\u0000\u00f8\u0001\u0000\u00a2\u0006\u0002\bUJ\u0015\u0010V\u001a\u00020K2\u0006\u0010W\u001a\u00020\u0011H\u0000\u00a2\u0006\u0002\bXJ\u0006\u0010Y\u001a\u00020ZJ<\u0010[\u001a\u00020.2\u0006\u0010\\\u001a\u00020\u00072\"\u0010@\u001a\u001e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00010-\u0012\u0004\u0012\u00020.0,j\b\u0012\u0004\u0012\u00020\u0001`/H\u0000\u00f8\u0001\u0000\u00a2\u0006\u0002\b]J\u0016\u0010^\u001a\u00020\u00002\u000e\u0010@\u001a\n\u0012\u0004\u0012\u00020\u0019\u0018\u00010_J1\u0010^\u001a\u00020\u00002&\u0010@\u001a\"\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00190-\u0012\u0004\u0012\u00020.\u0018\u00010,j\n\u0012\u0004\u0012\u00020\u0019\u0018\u0001`/\u00f8\u0001\u0000J\u000e\u0010^\u001a\u00020\u00002\u0006\u00104\u001a\u00020\u0014J:\u0010`\u001a\b\u0012\u0004\u0012\u0002Hb0a\"\u0006\b\u0000\u0010b\u0018\u0001*\u001e\u0012\n\u0012\b\u0012\u0004\u0012\u0002Hb0-\u0012\u0004\u0012\u00020.0,j\b\u0012\u0004\u0012\u0002Hb`/H\u0082\b\u00f8\u0001\u0000J\u0012\u0010c\u001a\u00020d*\u00020$H\u0080\b\u00a2\u0006\u0002\beR\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\t\u001a\u0004\u0018\u00010\nX\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u0014\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\u00110\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\u0012\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00140\u0013X\u0080\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R?\u0010\u0017\u001a&\u0012\f\u0012\n \u001a*\u0004\u0018\u00010\u00190\u0019 \u001a*\u0012\u0012\f\u0012\n \u001a*\u0004\u0018\u00010\u00190\u0019\u0018\u00010\u00180\u00188BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u001b\u0010\u001cR?\u0010\u001f\u001a&\u0012\f\u0012\n \u001a*\u0004\u0018\u00010 0  \u001a*\u0012\u0012\f\u0012\n \u001a*\u0004\u0018\u00010 0 \u0018\u00010\u00180\u00188BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\"\u0010\u001e\u001a\u0004\b!\u0010\u001cR?\u0010#\u001a&\u0012\f\u0012\n \u001a*\u0004\u0018\u00010$0$ \u001a*\u0012\u0012\f\u0012\n \u001a*\u0004\u0018\u00010$0$\u0018\u00010\u00180\u00188BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b&\u0010\u001e\u001a\u0004\b%\u0010\u001cR?\u0010\'\u001a&\u0012\f\u0012\n \u001a*\u0004\u0018\u00010(0( \u001a*\u0012\u0012\f\u0012\n \u001a*\u0004\u0018\u00010(0(\u0018\u00010\u00180\u00188BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b*\u0010\u001e\u001a\u0004\b)\u0010\u001cRa\u00100\u001a\"\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00190-\u0012\u0004\u0012\u00020.\u0018\u00010,j\n\u0012\u0004\u0012\u00020\u0019\u0018\u0001`/2&\u0010+\u001a\"\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00190-\u0012\u0004\u0012\u00020.\u0018\u00010,j\n\u0012\u0004\u0012\u00020\u0019\u0018\u0001`/@BX\u0086\u000e\u00f8\u0001\u0000\u00a2\u0006\b\n\u0000\u001a\u0004\b1\u00102\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006h"}, d2 = {"Lio/piano/android/id/PianoIdClient;", "", "api", "Lio/piano/android/id/models/PianoIdApi;", "moshi", "Lcom/squareup/moshi/Moshi;", "aid", "", "(Lio/piano/android/id/models/PianoIdApi;Lcom/squareup/moshi/Moshi;Ljava/lang/String;)V", "authEndpoint", "Lokhttp3/HttpUrl;", "getAuthEndpoint$id_debug", "()Lokhttp3/HttpUrl;", "setAuthEndpoint$id_debug", "(Lokhttp3/HttpUrl;)V", "exceptions", "Landroid/util/SparseArray;", "Lio/piano/android/id/PianoIdException;", "oauthProviders", "", "Lio/piano/android/id/PianoIdOAuthProvider;", "getOauthProviders$id_debug", "()Ljava/util/Map;", "pianoIdTokenAdapter", "Lcom/squareup/moshi/JsonAdapter;", "Lio/piano/android/id/models/PianoIdToken;", "kotlin.jvm.PlatformType", "getPianoIdTokenAdapter", "()Lcom/squareup/moshi/JsonAdapter;", "pianoIdTokenAdapter$delegate", "Lkotlin/Lazy;", "socialTokenDataAdapter", "Lio/piano/android/id/models/SocialTokenData;", "getSocialTokenDataAdapter", "socialTokenDataAdapter$delegate", "socialTokenResponseAdapter", "Lio/piano/android/id/models/SocialTokenResponse;", "getSocialTokenResponseAdapter", "socialTokenResponseAdapter$delegate", "tokenAdapter", "Lio/piano/android/id/models/TokenData;", "getTokenAdapter", "tokenAdapter$delegate", "<set-?>", "Lkotlin/Function1;", "Lkotlin/Result;", "", "Lio/piano/android/id/PianoIdFuncCallback;", "tokenCallback", "getTokenCallback", "()Lkotlin/jvm/functions/Function1;", "buildResultJsCommand", "provider", "token", "buildResultJsCommand$id_debug", "buildSocialAuthIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "jsPayload", "buildSocialAuthIntent$id_debug", "buildToken", "buildToken$id_debug", "getAuthEndpoint", "callback", "getResult", "intent", "getResult$id_debug", "getSignInUrl", "disableSignUp", "", "widget", "getSignInUrl$id_debug", "getStoredException", "code", "", "getStoredException$id_debug", "getTokenByAuthCode", "authCode", "getTokenByAuthCode$id_debug", "parseToken", "uri", "Landroid/net/Uri;", "parseToken$id_debug", "refreshToken", "refreshToken$id_debug", "saveException", "exc", "saveException$id_debug", "signIn", "Lio/piano/android/id/PianoIdClient$SignInContext;", "signOut", "accessToken", "signOut$id_debug", "with", "Lio/piano/android/id/PianoIdCallback;", "asRetrofitCallback", "Lretrofit2/Callback;", "T", "toBundle", "Landroid/os/Bundle;", "toBundle$id_debug", "Companion", "SignInContext", "id_debug"})
public final class PianoIdClient {
    private final kotlin.Lazy tokenAdapter$delegate = null;
    private final kotlin.Lazy pianoIdTokenAdapter$delegate = null;
    private final kotlin.Lazy socialTokenResponseAdapter$delegate = null;
    private final kotlin.Lazy socialTokenDataAdapter$delegate = null;
    @org.jetbrains.annotations.Nullable()
    private okhttp3.HttpUrl authEndpoint;
    private final android.util.SparseArray<io.piano.android.id.PianoIdException> exceptions = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.Map<java.lang.String, io.piano.android.id.PianoIdOAuthProvider> oauthProviders = null;
    @org.jetbrains.annotations.Nullable()
    private kotlin.jvm.functions.Function1<? super kotlin.Result<io.piano.android.id.models.PianoIdToken>, kotlin.Unit> tokenCallback;
    private final io.piano.android.id.models.PianoIdApi api = null;
    private final com.squareup.moshi.Moshi moshi = null;
    private final java.lang.String aid = null;
    private static final java.lang.String AUTH_PATH = "/id/api/v1/identity/vxauth/authorize";
    private static final java.lang.String SIGN_OUT_PATH = "/id/api/v1/identity/logout?response_type=code";
    private static final java.lang.String EXCHANGE_AUTH_CODE_PATH = "/id/api/v1/identity/passwordless/authorization/code";
    private static final java.lang.String REFRESH_TOKEN_PATH = "/id/api/v1/identity/vxauth/token";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String LINK_SCHEME_PREFIX = "piano.id.oauth.";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String LINK_AUTHORITY = "success";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_AUTH_CODE = "code";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_RESPONSE_TYPE = "response_type";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_CLIENT_ID = "client_id";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_FORCE_REDIRECT = "force_redirect";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_DISABLE_SIGN_UP = "disable_sign_up";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_REDIRECT_URI = "redirect_uri";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_SDK_FLAG = "is_sdk";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_SCREEN = "screen";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_OAUTH_PROVIDERS = "oauth_providers";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_GRANT_TYPE = "grant_type";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PARAM_REFRESH_TOKEN = "refresh_token";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String VALUE_RESPONSE_TYPE_TOKEN = "token";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String VALUE_FORCE_REDIRECT = "1";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String VALUE_SDK_FLAG = "true";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String VALUE_GRANT_TYPE = "refresh_token";
    public static final io.piano.android.id.PianoIdClient.Companion Companion = null;
    
    private final com.squareup.moshi.JsonAdapter<io.piano.android.id.models.TokenData> getTokenAdapter() {
        return null;
    }
    
    private final com.squareup.moshi.JsonAdapter<io.piano.android.id.models.PianoIdToken> getPianoIdTokenAdapter() {
        return null;
    }
    
    private final com.squareup.moshi.JsonAdapter<io.piano.android.id.models.SocialTokenResponse> getSocialTokenResponseAdapter() {
        return null;
    }
    
    private final com.squareup.moshi.JsonAdapter<io.piano.android.id.models.SocialTokenData> getSocialTokenDataAdapter() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final okhttp3.HttpUrl getAuthEndpoint$id_debug() {
        return null;
    }
    
    public final void setAuthEndpoint$id_debug(@org.jetbrains.annotations.Nullable()
    okhttp3.HttpUrl p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.Map<java.lang.String, io.piano.android.id.PianoIdOAuthProvider> getOauthProviders$id_debug() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final kotlin.jvm.functions.Function1<kotlin.Result<io.piano.android.id.models.PianoIdToken>, kotlin.Unit> getTokenCallback() {
        return null;
    }
    
    /**
     * Sets callback for {@link PianoIdToken} changes
     *
     * @param callback {@link PianoIdCallback} for listening changes
     * @return {@link PianoIdClient} instance
     */
    @org.jetbrains.annotations.NotNull()
    @kotlin.Suppress(names = {"unused"})
    public final io.piano.android.id.PianoIdClient with(@org.jetbrains.annotations.Nullable()
    io.piano.android.id.PianoIdCallback<io.piano.android.id.models.PianoIdToken> callback) {
        return null;
    }
    
    /**
     * Sets callback for {@link PianoIdToken} changes
     *
     * @param callback Callback with {@link kotlin.Result} for listening changes
     * @return {@link PianoIdClient} instance
     */
    @org.jetbrains.annotations.NotNull()
    @kotlin.Suppress(names = {"unused"})
    public final io.piano.android.id.PianoIdClient with(@org.jetbrains.annotations.Nullable()
    kotlin.jvm.functions.Function1<? super kotlin.Result<io.piano.android.id.models.PianoIdToken>, kotlin.Unit> callback) {
        return null;
    }
    
    /**
     * Adds OAuth provider
     *
     * @param provider {@link PianoIdOAuthProvider} instance
     * @return {@link PianoIdClient} instance
     */
    @org.jetbrains.annotations.NotNull()
    @kotlin.Suppress(names = {"unused"})
    public final io.piano.android.id.PianoIdClient with(@org.jetbrains.annotations.NotNull()
    io.piano.android.id.PianoIdOAuthProvider provider) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @kotlin.Suppress(names = {"unused"})
    public final io.piano.android.id.PianoIdClient.SignInContext signIn() {
        return null;
    }
    
    public final void getAuthEndpoint$id_debug(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super kotlin.Result<okhttp3.HttpUrl>, kotlin.Unit> callback) {
    }
    
    public final void signOut$id_debug(@org.jetbrains.annotations.NotNull()
    java.lang.String accessToken, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super kotlin.Result<? extends java.lang.Object>, kotlin.Unit> callback) {
    }
    
    public final void getTokenByAuthCode$id_debug(@org.jetbrains.annotations.NotNull()
    java.lang.String authCode, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super kotlin.Result<io.piano.android.id.models.PianoIdToken>, kotlin.Unit> callback) {
    }
    
    public final void refreshToken$id_debug(@org.jetbrains.annotations.NotNull()
    java.lang.String refreshToken, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super kotlin.Result<io.piano.android.id.models.PianoIdToken>, kotlin.Unit> callback) {
    }
    
    public final void getSignInUrl$id_debug(boolean disableSignUp, @org.jetbrains.annotations.Nullable()
    java.lang.String widget, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super kotlin.Result<java.lang.String>, kotlin.Unit> callback) {
    }
    
    public final int saveException$id_debug(@org.jetbrains.annotations.NotNull()
    io.piano.android.id.PianoIdException exc) {
        return 0;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final io.piano.android.id.PianoIdException getStoredException$id_debug(int code) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final io.piano.android.id.models.PianoIdToken getResult$id_debug(@org.jetbrains.annotations.Nullable()
    android.content.Intent intent) {
        return null;
    }
    
    public final void parseToken$id_debug(@org.jetbrains.annotations.NotNull()
    android.net.Uri uri, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super kotlin.Result<io.piano.android.id.models.PianoIdToken>, kotlin.Unit> callback) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final io.piano.android.id.models.PianoIdToken buildToken$id_debug(@org.jetbrains.annotations.NotNull()
    java.lang.String jsPayload) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Intent buildSocialAuthIntent$id_debug(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String jsPayload) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String buildResultJsCommand$id_debug(@org.jetbrains.annotations.NotNull()
    java.lang.String provider, @org.jetbrains.annotations.NotNull()
    java.lang.String token) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @kotlin.Suppress(names = {"NOTHING_TO_INLINE"})
    public final android.os.Bundle toBundle$id_debug(@org.jetbrains.annotations.NotNull()
    io.piano.android.id.models.SocialTokenResponse $this$toBundle) {
        return null;
    }
    
    public PianoIdClient(@org.jetbrains.annotations.NotNull()
    io.piano.android.id.models.PianoIdApi api, @org.jetbrains.annotations.NotNull()
    com.squareup.moshi.Moshi moshi, @org.jetbrains.annotations.NotNull()
    java.lang.String aid) {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\b\u0000\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0006\u0010\u0007\u001a\u00020\u0000J\u000e\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016J\u0010\u0010\r\u001a\u00020\u00002\b\u0010\r\u001a\u0004\u0018\u00010\u000eR\u0014\u0010\u0002\u001a\u00020\u0003X\u0080\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u001a\u0010\u0007\u001a\u00020\bX\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR\u001c\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012\u00a8\u0006\u0017"}, d2 = {"Lio/piano/android/id/PianoIdClient$SignInContext;", "", "client", "Lio/piano/android/id/PianoIdClient;", "(Lio/piano/android/id/PianoIdClient;)V", "getClient$id_debug", "()Lio/piano/android/id/PianoIdClient;", "disableSignUp", "", "getDisableSignUp$id_debug", "()Z", "setDisableSignUp$id_debug", "(Z)V", "widget", "", "getWidget$id_debug", "()Ljava/lang/String;", "setWidget$id_debug", "(Ljava/lang/String;)V", "getIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "id_debug"})
    public static final class SignInContext {
        private boolean disableSignUp = false;
        @org.jetbrains.annotations.Nullable()
        private java.lang.String widget;
        @org.jetbrains.annotations.NotNull()
        private final io.piano.android.id.PianoIdClient client = null;
        
        public final boolean getDisableSignUp$id_debug() {
            return false;
        }
        
        public final void setDisableSignUp$id_debug(boolean p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getWidget$id_debug() {
            return null;
        }
        
        public final void setWidget$id_debug(@org.jetbrains.annotations.Nullable()
        java.lang.String p0) {
        }
        
        /**
         * Turns off the registration screen
         *
         * @return {@link SignInContext} instance
         */
        @org.jetbrains.annotations.NotNull()
        @kotlin.Suppress(names = {"unused"})
        public final io.piano.android.id.PianoIdClient.SignInContext disableSignUp() {
            return null;
        }
        
        /**
         * Sets the screen when opening Piano ID. Use {@link PianoId#WIDGET_LOGIN} to open the login screen
         * or {@link PianoId#WIDGET_REGISTER} to open the registration screen.
         *
         * @param widget {@link PianoId#WIDGET_LOGIN}, {@link PianoId#WIDGET_REGISTER} or null
         * @return {@link SignInContext} instance
         */
        @org.jetbrains.annotations.NotNull()
        @kotlin.Suppress(names = {"unused"})
        public final io.piano.android.id.PianoIdClient.SignInContext widget(@org.jetbrains.annotations.Nullable()
        java.lang.String widget) {
            return null;
        }
        
        /**
         * Gets {@link Intent} for starting authorization process
         *
         * @param context {@link Context} for building {@link Intent}
         * @return {@link Intent}, which should be passed to {@link android.app.Activity#startActivityForResult(Intent, int)}
         */
        @org.jetbrains.annotations.NotNull()
        @kotlin.Suppress(names = {"unused"})
        public final android.content.Intent getIntent(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final io.piano.android.id.PianoIdClient getClient$id_debug() {
            return null;
        }
        
        public SignInContext(@org.jetbrains.annotations.NotNull()
        io.piano.android.id.PianoIdClient client) {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0016\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0003\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u001d\u0010\u0019\u001a\u0002H\u001a\"\u0004\b\u0000\u0010\u001a*\b\u0012\u0004\u0012\u0002H\u001a0\u001bH\u0002\u00a2\u0006\u0002\u0010\u001cJ\u0011\u0010\u001d\u001a\u00020\u001e*\u00020\u001fH\u0000\u00a2\u0006\u0002\b R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0017\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000\u00a8\u0006!"}, d2 = {"Lio/piano/android/id/PianoIdClient$Companion;", "", "()V", "AUTH_PATH", "", "EXCHANGE_AUTH_CODE_PATH", "LINK_AUTHORITY", "LINK_SCHEME_PREFIX", "PARAM_AUTH_CODE", "PARAM_CLIENT_ID", "PARAM_DISABLE_SIGN_UP", "PARAM_FORCE_REDIRECT", "PARAM_GRANT_TYPE", "PARAM_OAUTH_PROVIDERS", "PARAM_REDIRECT_URI", "PARAM_REFRESH_TOKEN", "PARAM_RESPONSE_TYPE", "PARAM_SCREEN", "PARAM_SDK_FLAG", "REFRESH_TOKEN_PATH", "SIGN_OUT_PATH", "VALUE_FORCE_REDIRECT", "VALUE_GRANT_TYPE", "VALUE_RESPONSE_TYPE_TOKEN", "VALUE_SDK_FLAG", "bodyOrThrow", "T", "Lretrofit2/Response;", "(Lretrofit2/Response;)Ljava/lang/Object;", "toPianoIdException", "Lio/piano/android/id/PianoIdException;", "", "toPianoIdException$id_debug", "id_debug"})
    public static final class Companion {
        
        private final <T extends java.lang.Object>T bodyOrThrow(retrofit2.Response<T> $this$bodyOrThrow) {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final io.piano.android.id.PianoIdException toPianoIdException$id_debug(@org.jetbrains.annotations.NotNull()
        java.lang.Throwable $this$toPianoIdException) {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}