package io.piano.android.id.models;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010$\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b`\u0018\u00002\u00020\u0001J,\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\u00062\b\b\u0001\u0010\b\u001a\u00020\u0006H\'J\u0018\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u00032\b\b\u0001\u0010\u0007\u001a\u00020\u0006H\'J.\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\u0014\b\u0001\u0010\f\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00060\rH\'J,\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u000f0\u00032\b\b\u0001\u0010\u0005\u001a\u00020\u00062\b\b\u0001\u0010\u0007\u001a\u00020\u00062\b\b\u0001\u0010\u0010\u001a\u00020\u0006H\'\u00a8\u0006\u0011"}, d2 = {"Lio/piano/android/id/models/PianoIdApi;", "", "exchangeAuthCode", "Lretrofit2/Call;", "Lio/piano/android/id/models/PianoIdToken;", "url", "", "aid", "authCode", "getDeploymentHost", "Lio/piano/android/id/models/HostResponse;", "refreshToken", "tokenParams", "", "signOut", "Lokhttp3/ResponseBody;", "accessToken", "id_debug"})
public abstract interface PianoIdApi {
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET()
    public abstract retrofit2.Call<okhttp3.ResponseBody> signOut(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Url()
    java.lang.String url, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "client_id")
    java.lang.String aid, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "token")
    java.lang.String accessToken);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST()
    public abstract retrofit2.Call<io.piano.android.id.models.PianoIdToken> exchangeAuthCode(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Url()
    java.lang.String url, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "aid")
    java.lang.String aid, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "passwordless_token")
    java.lang.String authCode);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST()
    @retrofit2.http.FormUrlEncoded()
    public abstract retrofit2.Call<io.piano.android.id.models.PianoIdToken> refreshToken(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Url()
    java.lang.String url, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.FieldMap()
    java.util.Map<java.lang.String, java.lang.String> tokenParams);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "api/v3/anon/mobile/sdk/id/deployment/host")
    @retrofit2.http.FormUrlEncoded()
    public abstract retrofit2.Call<io.piano.android.id.models.HostResponse> getDeploymentHost(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Field(value = "aid")
    java.lang.String aid);
}