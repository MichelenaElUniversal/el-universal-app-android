package io.piano.android.id;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0003\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 .2\u00020\u00012\u00020\u0002:\u0001.B\u0005\u00a2\u0006\u0002\u0010\u0003J\b\u0010\u000f\u001a\u00020\u0010H\u0016J\u0011\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u000eH\u0082\bJ\u0012\u0010\u0014\u001a\u00020\u00102\b\u0010\u0015\u001a\u0004\u0018\u00010\u000eH\u0016J\"\u0010\u0016\u001a\u00020\u00102\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u00182\b\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u0014J\b\u0010\u001c\u001a\u00020\u0010H\u0016J\u0012\u0010\u001d\u001a\u00020\u00102\b\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0015J\b\u0010 \u001a\u00020\u0010H\u0014J\u0010\u0010!\u001a\u00020\u00102\u0006\u0010\"\u001a\u00020\u001bH\u0014J\u0010\u0010#\u001a\u00020\u00102\u0006\u0010$\u001a\u00020\u001fH\u0014J\u0010\u0010%\u001a\u00020\u00102\u0006\u0010&\u001a\u00020\'H\u0002J\u0010\u0010(\u001a\u00020\u00102\u0006\u0010)\u001a\u00020*H\u0002J\u0012\u0010+\u001a\u00020\u00102\b\u0010\u0015\u001a\u0004\u0018\u00010\u000eH\u0016J\u0011\u0010,\u001a\u00020\u0010*\u00020\u001bH\u0000\u00a2\u0006\u0002\b-R\u0018\u0010\u0004\u001a\u00020\u00058\u0002@\u0002X\u0083.\u00a2\u0006\b\n\u0000\u0012\u0004\b\u0006\u0010\u0003R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006/"}, d2 = {"Lio/piano/android/id/PianoIdActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "Lio/piano/android/id/PianoIdJsInterface;", "()V", "binding", "Lio/piano/android/id/databinding/ActivityPianoIdBinding;", "getBinding$annotations", "client", "Lio/piano/android/id/PianoIdClient;", "disableSignUp", "", "jsInterface", "Lio/piano/android/id/PianoIdJavascriptDelegate;", "widget", "", "cancel", "", "evaluateJavascript", "Landroid/webkit/WebView;", "code", "loginSuccess", "payload", "onActivityResult", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "onBackPressed", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onNewIntent", "intent", "onSaveInstanceState", "outState", "setFailureResultData", "throwable", "", "setSuccessResultData", "token", "Lio/piano/android/id/models/PianoIdToken;", "socialLogin", "process", "process$id_debug", "Companion", "id_debug"})
public final class PianoIdActivity extends androidx.appcompat.app.AppCompatActivity implements io.piano.android.id.PianoIdJsInterface {
    private final io.piano.android.id.PianoIdJavascriptDelegate jsInterface = null;
    private java.lang.String widget;
    private boolean disableSignUp = false;
    @androidx.annotation.RestrictTo(value = {androidx.annotation.RestrictTo.Scope.LIBRARY})
    private io.piano.android.id.databinding.ActivityPianoIdBinding binding;
    private final io.piano.android.id.PianoIdClient client = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String KEY_WIDGET = "io.piano.android.id.PianoIdActivity.WIDGET";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String KEY_DISABLE_SIGN_UP = "io.piano.android.id.PianoIdActivity.DISABLE_SIGN_UP";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String JS_INTERFACE_NAME = "PianoIDMobileSDK";
    public static final int OAUTH_PROVIDER_REQUEST_CODE = 1;
    public static final io.piano.android.id.PianoIdActivity.Companion Companion = null;
    
    @androidx.annotation.VisibleForTesting()
    @java.lang.Deprecated()
    private static void getBinding$annotations() {
    }
    
    @android.annotation.SuppressLint(value = {"SetJavaScriptEnabled", "AddJavascriptInterface"})
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    protected void onNewIntent(@org.jetbrains.annotations.NotNull()
    android.content.Intent intent) {
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    @java.lang.Override()
    protected void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    @java.lang.Override()
    protected void onSaveInstanceState(@org.jetbrains.annotations.NotNull()
    android.os.Bundle outState) {
    }
    
    @java.lang.Override()
    protected void onDestroy() {
    }
    
    @java.lang.Override()
    public void socialLogin(@org.jetbrains.annotations.Nullable()
    java.lang.String payload) {
    }
    
    @java.lang.Override()
    public void loginSuccess(@org.jetbrains.annotations.Nullable()
    java.lang.String payload) {
    }
    
    @java.lang.Override()
    public void cancel() {
    }
    
    private final void setSuccessResultData(io.piano.android.id.models.PianoIdToken token) {
    }
    
    private final void setFailureResultData(java.lang.Throwable throwable) {
    }
    
    @kotlin.Suppress(names = {"NOTHING_TO_INLINE"})
    private final android.webkit.WebView evaluateJavascript(java.lang.String code) {
        return null;
    }
    
    public final void process$id_debug(@org.jetbrains.annotations.NotNull()
    android.content.Intent $this$process) {
    }
    
    public PianoIdActivity() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final android.content.Intent buildIntent$id_debug(@org.jetbrains.annotations.NotNull()
    android.content.Context context, boolean disableSignUp, @org.jetbrains.annotations.Nullable()
    java.lang.String widget) {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\'\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\u0004H\u0001\u00a2\u0006\u0002\b\u0010R\u000e\u0010\u0003\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0080T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"}, d2 = {"Lio/piano/android/id/PianoIdActivity$Companion;", "", "()V", "JS_INTERFACE_NAME", "", "KEY_DISABLE_SIGN_UP", "KEY_WIDGET", "OAUTH_PROVIDER_REQUEST_CODE", "", "buildIntent", "Landroid/content/Intent;", "context", "Landroid/content/Context;", "disableSignUp", "", "widget", "buildIntent$id_debug", "id_debug"})
    public static final class Companion {
        
        @org.jetbrains.annotations.NotNull()
        public final android.content.Intent buildIntent$id_debug(@org.jetbrains.annotations.NotNull()
        android.content.Context context, boolean disableSignUp, @org.jetbrains.annotations.Nullable()
        java.lang.String widget) {
            return null;
        }
        
        private Companion() {
            super();
        }
    }
}