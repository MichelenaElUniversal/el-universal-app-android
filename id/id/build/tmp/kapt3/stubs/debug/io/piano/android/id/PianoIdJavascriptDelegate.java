package io.piano.android.id;

import java.lang.System;

@kotlin.Metadata(mv = {1, 4, 0}, bv = {1, 0, 3}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0001\u00a2\u0006\u0002\u0010\u0003J\b\u0010\u0006\u001a\u00020\u0007H\u0017J\u0012\u0010\b\u001a\u00020\u00072\b\u0010\t\u001a\u0004\u0018\u00010\nH\u0017J\u0012\u0010\u000b\u001a\u00020\u00072\b\u0010\t\u001a\u0004\u0018\u00010\nH\u0017R\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00010\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\f"}, d2 = {"Lio/piano/android/id/PianoIdJavascriptDelegate;", "Lio/piano/android/id/PianoIdJsInterface;", "delegate", "(Lio/piano/android/id/PianoIdJsInterface;)V", "reference", "Ljava/lang/ref/WeakReference;", "cancel", "", "loginSuccess", "payload", "", "socialLogin", "id_debug"})
public final class PianoIdJavascriptDelegate implements io.piano.android.id.PianoIdJsInterface {
    private final java.lang.ref.WeakReference<io.piano.android.id.PianoIdJsInterface> reference = null;
    
    @java.lang.Override()
    @android.webkit.JavascriptInterface()
    public void socialLogin(@org.jetbrains.annotations.Nullable()
    java.lang.String payload) {
    }
    
    @java.lang.Override()
    @android.webkit.JavascriptInterface()
    public void loginSuccess(@org.jetbrains.annotations.Nullable()
    java.lang.String payload) {
    }
    
    @java.lang.Override()
    @android.webkit.JavascriptInterface()
    public void cancel() {
    }
    
    public PianoIdJavascriptDelegate(@org.jetbrains.annotations.NotNull()
    io.piano.android.id.PianoIdJsInterface delegate) {
        super();
    }
}