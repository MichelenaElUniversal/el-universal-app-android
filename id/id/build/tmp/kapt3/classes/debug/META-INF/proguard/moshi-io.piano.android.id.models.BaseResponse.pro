-if class io.piano.android.id.models.BaseResponse
-keepnames class io.piano.android.id.models.BaseResponse
-if class io.piano.android.id.models.BaseResponse
-keep class io.piano.android.id.models.BaseResponseJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
