-if class io.piano.android.id.models.SocialTokenResponse
-keepnames class io.piano.android.id.models.SocialTokenResponse
-if class io.piano.android.id.models.SocialTokenResponse
-keep class io.piano.android.id.models.SocialTokenResponseJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
