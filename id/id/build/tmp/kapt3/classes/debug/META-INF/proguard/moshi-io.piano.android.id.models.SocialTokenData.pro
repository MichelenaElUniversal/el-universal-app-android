-if class io.piano.android.id.models.SocialTokenData
-keepnames class io.piano.android.id.models.SocialTokenData
-if class io.piano.android.id.models.SocialTokenData
-keep class io.piano.android.id.models.SocialTokenDataJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
