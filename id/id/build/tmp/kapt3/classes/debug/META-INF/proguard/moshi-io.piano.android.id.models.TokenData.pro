-if class io.piano.android.id.models.TokenData
-keepnames class io.piano.android.id.models.TokenData
-if class io.piano.android.id.models.TokenData
-keep class io.piano.android.id.models.TokenDataJsonAdapter {
    public <init>(com.squareup.moshi.Moshi);
}
