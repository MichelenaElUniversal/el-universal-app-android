package com.msi.eluniversal.base

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.msi.eluniversal.injector.component.DaggerViewModelInjector
import com.msi.eluniversal.injector.component.ViewModelInjector
import com.msi.eluniversal.injector.module.NetworkModule
import com.msi.eluniversal.ui.viewModel.GeneralViewModel

abstract class BaseViewModel: ViewModel() {

    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val errorMessage: MutableLiveData<String?> = MutableLiveData()
    private val enabledElement: MutableLiveData<Boolean> = MutableLiveData()


    protected fun onRetrieveInfoStart() {
        loadingVisibility.value = View.VISIBLE
        enabledElement.value = false
        errorMessage.value = null
    }

    protected fun onRetrieveInfoFinish(){
        loadingVisibility.value = View.GONE
        enabledElement.value = true
    }

    protected fun onRetrieveInfoError(error: String?){
        errorMessage.value = error
    }
}