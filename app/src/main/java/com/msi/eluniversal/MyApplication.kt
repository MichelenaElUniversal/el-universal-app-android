package com.msi.eluniversal

import android.app.Application
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.media.app.NotificationCompat
import com.google.android.gms.ads.MobileAds
import com.google.firebase.FirebaseApp
import com.msi.eluniversal.utils.Constants
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.HiltAndroidApp
import io.piano.android.id.PianoId
import io.piano.android.id.facebook.FacebookOAuthProvider
import io.piano.android.id.google.GoogleOAuthProvider

@HiltAndroidApp
class MyApplication: Application() {
    val CHANNEL_ID = "Principales"
    override fun onCreate() {
        super.onCreate()
        Hawk.init(this).build()
        FirebaseApp.initializeApp(this)
        MobileAds.initialize(this)
        PianoId.init(PianoId.ENDPOINT_PRODUCTION, BuildConfig.PIANO_AID).with(GoogleOAuthProvider()).with(
            FacebookOAuthProvider())
        createNotificationChannel()
    }


    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.channel_name)
            val descriptionText = getString(R.string.channel_description)

            val channel = NotificationChannel(
                CHANNEL_ID,
                name,
                NotificationManager.IMPORTANCE_HIGH
            ).apply {
                description = descriptionText
            }

            // Register the channel
            val notificationManager: NotificationManager =
                getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }
}