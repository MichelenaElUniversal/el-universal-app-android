package com.msi.eluniversal

import android.annotation.SuppressLint
import android.content.Intent
import com.msi.eluniversal.ui.activity.PushNotificationDetailsActivity
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.msi.eluniversal.ui.activity.HomeActivity
import com.orhanobut.hawk.Hawk
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error


import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import androidx.work.Worker
import androidx.work.WorkerParameters
import org.jetbrains.anko.startActivity
import java.math.BigInteger

class MyFirebaseMessagingService : FirebaseMessagingService(), AnkoLogger {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Hawk.init(this).build()
        val message: String? = remoteMessage.data["cms_id"]

        if (message != null) {
            val type = remoteMessage.data["type"]

            val intent = when (type) {
                "a" -> {
                    Intent(this, HomeActivity::class.java)
                }
                "n" -> {
                    try {
                        Hawk.put("cms_id", message)
                        Intent(this, PushNotificationDetailsActivity::class.java)
                    } catch (e: NumberFormatException) {
                        error { "NOT A BIG INTEGER" }
                        Intent(this, HomeActivity::class.java)
                    }
                }
                else -> {
                    Intent(this, HomeActivity::class.java)
                }
            }

            intent.action = Intent.ACTION_MAIN
            intent.addCategory(Intent.CATEGORY_LAUNCHER)
            startActivity(intent)
        }
    }

    override fun onNewToken(token: String) {
        error { "Refreshed token: $token" }
    }
}

