package com.msi.eluniversal.injector.network.bases

import com.msi.eluniversal.injector.network.ElUniversalAPI
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import java.math.BigInteger
import javax.inject.Inject

class RemoteDataSource @Inject constructor(private val elUniversalAPI: ElUniversalAPI) {
    //suspend fun getDog() = dogService.getDog()
    suspend fun getSections() = elUniversalAPI.getSections()
    suspend fun getVideoSections() = elUniversalAPI.getVideoSections()
    suspend fun getContent(seccion: String) = elUniversalAPI.getContent(seccion = seccion)
    suspend fun getDetails(cms_id: String) = elUniversalAPI.getDetails(cms_id = cms_id)
    suspend fun getMxm() = elUniversalAPI.getMinuteXMinute()

    suspend fun getOpinion() = elUniversalAPI.getOpinion()
    suspend fun getVideoFeaturedContent() = elUniversalAPI.getVideoFeaturedContent()

    suspend fun getVideoContent(seccion: String) = elUniversalAPI.getVideoContent(seccion = seccion)

    suspend fun getGalleryContent() = elUniversalAPI.getGalleryContent()
    suspend fun getSearchContent(term: String) = elUniversalAPI.getSearchContent(seccion = term)
    suspend fun getAdContent() = elUniversalAPI.getAdContent()

    suspend fun getFavorites(requestBody: RequestBody) = elUniversalAPI.doGetFavorites(requestBody = requestBody)

    suspend fun saveNewFavorite(requestBody: RequestBody) = elUniversalAPI.postFavorite(requestBody = requestBody)

    //suspend fun postFavorite(requestBody: RequestBody) = elUniversalAPI.postFavorite(requestBody = requestBody)

    //suspend fun saveFavorite(requestBody: RequestBody) = elUniversalAPI.saveFavorite(requestBody = requestBody)
}
