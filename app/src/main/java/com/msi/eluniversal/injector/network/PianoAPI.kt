package com.msi.eluniversal.injector.network

import com.msi.eluniversal.models.Access
import com.msi.eluniversal.models.CheckAccessModel
import com.msi.eluniversal.models.ListCheckAccessModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface PianoAPI {
    //PianoCheckAccess
    @GET("/api/v3/anon/user/get")
    fun getPianoAccess(
        @Query("aid") aid: String = "dOm1nMaZpu",
        @Query("api_token") api_token: String = "zvmcMPjGhy9KsmjTBQ6753UTos2zlKTAXGqD4G8N",
        @Query("user_token") user_token: String = "RAB3DWX",
    ): Call<CheckAccessModel>

    @GET("/api/v3/access/list")
    fun getPianoListAccess(
        @Query("active") active: Boolean = true,
        @Query("aid") aid: String = "dOm1nMaZpu",
        @Query("api_token") api_token: String = "zvmcMPjGhy9KsmjTBQ6753UTos2zlKTAXGqD4G8N",
        @Query("limit") limit: Int = 100,
        @Query("user_token") user_token: String
    ): Call<ListCheckAccessModel>
}