package com.msi.eluniversal.injector.network.repository

import com.msi.eluniversal.injector.network.bases.BaseApiResponse
import com.msi.eluniversal.injector.network.bases.NetworkResult
import com.msi.eluniversal.injector.network.bases.RemoteDataSource
import com.msi.eluniversal.models.AdDataModel
import com.msi.eluniversal.models.AdModel
import com.msi.eluniversal.models.ContentModel
import com.msi.eluniversal.models.FavoriteContentsModel
import com.msi.eluniversal.models.GalleryContentModel
import com.msi.eluniversal.models.MinutoXMinutoContentModel
import com.msi.eluniversal.models.OpinionContentModel
import com.msi.eluniversal.models.PushNotificationContentModel
import com.msi.eluniversal.models.SaveFavoritesResponse
import com.msi.eluniversal.models.SectionsModel
import com.msi.eluniversal.models.VideoContentModel
import dagger.hilt.android.scopes.ActivityRetainedScoped
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import okhttp3.RequestBody
import java.math.BigInteger
import javax.inject.Inject

@ActivityRetainedScoped
class ElUniversalRepository @Inject constructor(
    private val remoteDataSource: RemoteDataSource
) : BaseApiResponse() {
    /*suspend fun getDog(): Flow<NetworkResult<DogResponse>> {
    return flow<NetworkResult<DogResponse>> {
        emit(safeApiCall { remoteDataSource.getDog() })
    }.flowOn(Dispatchers.IO)
}*/

    /** SECTION */

    suspend fun getSections(): Flow<NetworkResult<List<SectionsModel>>> {
        return flow {
            emit(safeApiCall { remoteDataSource.getSections() })
        }.flowOn(Dispatchers.IO)
    }



    suspend fun getVideoSections(): Flow<NetworkResult<List<SectionsModel>>> {
        return flow {
            emit(safeApiCall { remoteDataSource.getVideoSections() })
        }.flowOn(Dispatchers.IO)
    }

    /** CONTENT */

    suspend fun getContent(seccion: String): Flow<NetworkResult<List<ContentModel>>> {
        return flow {
            emit(safeApiCall { remoteDataSource.getContent(seccion) })
        }.flowOn(Dispatchers.IO)
    }

    /** DETAILS */

    suspend fun getDetails(cms_id: String): Flow<NetworkResult<PushNotificationContentModel>> {
        return flow {
            emit(safeApiCall { remoteDataSource.getDetails(cms_id) })
        }.flowOn(Dispatchers.IO)
    }

    /** MXM */

    suspend fun getMxm(): Flow<NetworkResult<List<MinutoXMinutoContentModel>>> {
        return flow {
            emit(safeApiCall { remoteDataSource.getMxm() })
        }.flowOn(Dispatchers.IO)
    }

    /** OPINION */

    suspend fun getOpinion(): Flow<NetworkResult<List<OpinionContentModel>>> {
        return flow {
            emit(safeApiCall { remoteDataSource.getOpinion() })
        }.flowOn(Dispatchers.IO)
    }

    /** VIDEOS */

    suspend fun getFeatured(): Flow<NetworkResult<List<VideoContentModel>>> {
        return flow {
            emit(safeApiCall { remoteDataSource.getVideoFeaturedContent() })
        }.flowOn(Dispatchers.IO)
    }

    suspend fun getVideos(seccion: String): Flow<NetworkResult<List<VideoContentModel>>> {
        return flow {
            emit(safeApiCall { remoteDataSource.getVideoContent(seccion) })
        }.flowOn(Dispatchers.IO)
    }

    /** GALERÍA */

    suspend fun getGallery(): Flow<NetworkResult<List<GalleryContentModel>>> {
        return flow {
            emit(safeApiCall { remoteDataSource.getGalleryContent() })
        }.flowOn(Dispatchers.IO)
    }

    /** SEARCH */

    suspend fun getSearch(term: String): Flow<NetworkResult<List<ContentModel>>> {
        return flow {
            emit(safeApiCall { remoteDataSource.getSearchContent(term) })
        }.flowOn(Dispatchers.IO)
    }

    /** ADS */

    suspend fun getAdContent(): Flow<NetworkResult<AdModel>> {
        return flow {
            emit(safeApiCall { remoteDataSource.getAdContent() })
        }.flowOn(Dispatchers.IO)
    }

    /** FAVORITES */
    suspend fun getFavorites(requestBody: RequestBody): Flow<NetworkResult<List<FavoriteContentsModel>>> {
        return flow {
            emit(safeApiCall { remoteDataSource.getFavorites(requestBody) })
        }.flowOn(Dispatchers.IO)
    }

    suspend fun saveNewFavorite(requestBody: RequestBody): Flow<NetworkResult<SaveFavoritesResponse>> {
        return flow {
            emit(safeApiCall { remoteDataSource.saveNewFavorite(requestBody) })
        }.flowOn(Dispatchers.IO)
    }

    /*suspend fun postFavoriteContent(requestBody: RequestBody): Flow<NetworkResult<List<FavoriteContentsModel>>> {
        return flow {
            emit(safeApiCall { remoteDataSource.postFavorite(requestBody) })
        }.flowOn(Dispatchers.IO)
    }

    suspend fun saveFavoriteContent(requestBody: RequestBody): Flow<NetworkResult<SaveFavoritesResponse>> {
        return flow {
            emit(safeApiCall { remoteDataSource.saveFavorite(requestBody) })
        }.flowOn(Dispatchers.IO)
    }*/
}
