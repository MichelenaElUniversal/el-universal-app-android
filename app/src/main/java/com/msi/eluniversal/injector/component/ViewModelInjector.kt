package com.msi.eluniversal.injector.component

import com.msi.eluniversal.injector.module.NetworkModule
import com.msi.eluniversal.ui.viewModel.GeneralViewModel
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [NetworkModule::class])
interface ViewModelInjector {

    fun inject(vm: GeneralViewModel)

    @Component.Builder
    interface Builder {
        fun build(): ViewModelInjector
        fun networkModule(networkModule: NetworkModule): Builder
    }
}