package com.msi.eluniversal.injector.network

import android.view.PixelCopy.Request
import com.msi.eluniversal.models.*
import com.msi.eluniversal.utils.Constants
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Path
import java.math.BigInteger

interface ElUniversalAPI {

    @GET("eluniversal/sections")
    suspend fun getSections(
        @Header("Authorization") Authorization: String = "EUL APIKey=KxOsNplAYNUB3hnTEt,Signature=9e0627060796e28acdef790f94ddb38f976b8101305940317da0077efb63683dcde211064d206819ae6c5c145ff19df87cf934499ae636fd26e6d53681a2d20f,timestamp=1574384320",
        @Header("Session-Id") SessionId: String = "3037976474466916"
    ): Response<List<SectionsModel>>

    @GET("eluniversal/sections/videos")
    suspend fun getVideoSections(
        @Header("Authorization") Authorization: String = "EUL APIKey=KxOsNplAYNUB3hnTEt,Signature=9e0627060796e28acdef790f94ddb38f976b8101305940317da0077efb63683dcde211064d206819ae6c5c145ff19df87cf934499ae636fd26e6d53681a2d20f,timestamp=1574384320",
        @Header("Session-Id") SessionId: String = "3037976474466916"
    ): Response<List<SectionsModel>>

    @GET("eluniversal/content/cover/{seccion}")
    suspend fun getContent(
        @Header("Authorization") Authorization: String = "EUL APIKey=KxOsNplAYNUB3hnTEt,Signature=9e0627060796e28acdef790f94ddb38f976b8101305940317da0077efb63683dcde211064d206819ae6c5c145ff19df87cf934499ae636fd26e6d53681a2d20f,timestamp=1574384320",
        @Header("Session-Id") SessionId: String = "3037976474466916",
        @Path("seccion") seccion: String
    ) : Response<List<ContentModel>>


    @GET("eluniversal/content/{cms_id}")
    suspend fun getDetails(
        @Header("Authorization") Authorization: String = "EUL APIKey=KxOsNplAYNUB3hnTEt,Signature=9e0627060796e28acdef790f94ddb38f976b8101305940317da0077efb63683dcde211064d206819ae6c5c145ff19df87cf934499ae636fd26e6d53681a2d20f,timestamp=1574384320",
        @Header("Session-Id") SessionId: String = "3037976474466916",
        @Path("cms_id") cms_id: String
    ) : Response<PushNotificationContentModel>

    @GET("eluniversal/content/minutexminute")
    suspend fun getMinuteXMinute(
        @Header("Authorization") Authorization: String = "EUL APIKey=KxOsNplAYNUB3hnTEt,Signature=9e0627060796e28acdef790f94ddb38f976b8101305940317da0077efb63683dcde211064d206819ae6c5c145ff19df87cf934499ae636fd26e6d53681a2d20f,timestamp=1574384320",
        @Header("Session-Id") SessionId: String = "3037976474466916"
    ) : Response<List<MinutoXMinutoContentModel>>

    @GET("eluniversal/content/cover/opinion")
    suspend fun getOpinion(
        @Header("Authorization") Authorization: String = "EUL APIKey=KxOsNplAYNUB3hnTEt,Signature=9e0627060796e28acdef790f94ddb38f976b8101305940317da0077efb63683dcde211064d206819ae6c5c145ff19df87cf934499ae636fd26e6d53681a2d20f,timestamp=1574384320",
        @Header("Session-Id") SessionId: String = "3037976474466916"
    ) : Response<List<OpinionContentModel>>

    @GET("eluniversal/content/videos/featured")
    suspend fun getVideoFeaturedContent(
        @Header("Authorization") Authorization: String = "EUL APIKey=KxOsNplAYNUB3hnTEt,Signature=9e0627060796e28acdef790f94ddb38f976b8101305940317da0077efb63683dcde211064d206819ae6c5c145ff19df87cf934499ae636fd26e6d53681a2d20f,timestamp=1574384320",
        @Header("Session-Id") SessionId: String = "3037976474466916"
    ): Response<List<VideoContentModel>>

    @GET("eluniversal/content/videos/recent/{seccion}")
    suspend fun getVideoContent(
        @Header("Authorization") Authorization: String = "EUL APIKey=KxOsNplAYNUB3hnTEt,Signature=9e0627060796e28acdef790f94ddb38f976b8101305940317da0077efb63683dcde211064d206819ae6c5c145ff19df87cf934499ae636fd26e6d53681a2d20f,timestamp=1574384320",
        @Header("Session-Id") SessionId: String = "3037976474466916",
        @Path("seccion") seccion: String
    ): Response<List<VideoContentModel>>

    @GET("eluniversal/content/cover/galerias")
    suspend fun getGalleryContent(
        @Header("Authorization") Authorization: String = "EUL APIKey=KxOsNplAYNUB3hnTEt,Signature=9e0627060796e28acdef790f94ddb38f976b8101305940317da0077efb63683dcde211064d206819ae6c5c145ff19df87cf934499ae636fd26e6d53681a2d20f,timestamp=1574384320",
        @Header("Session-Id") SessionId: String = "3037976474466916"
    ) : Response<List<GalleryContentModel>>

    @GET("eluniversal/content/search/{term}")
    suspend fun getSearchContent(
        @Header("Authorization") Authorization: String = "EUL APIKey=KxOsNplAYNUB3hnTEt,Signature=9e0627060796e28acdef790f94ddb38f976b8101305940317da0077efb63683dcde211064d206819ae6c5c145ff19df87cf934499ae636fd26e6d53681a2d20f,timestamp=1574384320",
        @Header("Session-Id") SessionId: String = "3037976474466916",
        @Path("term") seccion: String
    ) : Response<List<ContentModel>>

    @GET("https://api2.eluniversal.com.mx/publicity/show/eluniversal/android/smartphone")
    suspend fun getAdContent(
        @Header("Authorization") Authorization: String = "EUL APIKey=KxOsNplAYNUB3hnTEt,Signature=9e0627060796e28acdef790f94ddb38f976b8101305940317da0077efb63683dcde211064d206819ae6c5c145ff19df87cf934499ae636fd26e6d53681a2d20f,timestamp=1574384320",
        @Header("Session-Id") SessionId: String = "3037976474466916"

    ) : Response<AdModel>


    @POST("eluniversal/favorites/search")
    suspend fun doGetFavorites(
        @Header("Content-Type") contentType: String = "application/json",
        @Header("Authorization") Authorization: String = "EUL APIKey=KxOsNplAYNUB3hnTEt,Signature=9e0627060796e28acdef790f94ddb38f976b8101305940317da0077efb63683dcde211064d206819ae6c5c145ff19df87cf934499ae636fd26e6d53681a2d20f,timestamp=1574384320",
        @Header("Session-Id") SessionId: String = "3037976474466916",
        @Body requestBody: RequestBody
    ): Response<List<FavoriteContentsModel>>

    @POST("eluniversal/favorites")
    suspend fun postFavorite(
        @Header("Content-Type") contentType: String = "application/json",
        @Header("Authorization") Authorization: String = "EUL APIKey=KxOsNplAYNUB3hnTEt,Signature=9e0627060796e28acdef790f94ddb38f976b8101305940317da0077efb63683dcde211064d206819ae6c5c145ff19df87cf934499ae636fd26e6d53681a2d20f,timestamp=1574384320",
        @Header("Session-Id") SessionId: String = "3037976474466916",
        @Body requestBody: RequestBody
    ): Response<SaveFavoritesResponse>

    /*@POST("eluniversal/favorites/search")
    suspend fun postFavorite(
        @Header("Content-Type") contentType: String = "application/json",
        @Header("Authorization") Authorization: String = "EUL APIKey=KxOsNplAYNUB3hnTEt,Signature=9e0627060796e28acdef790f94ddb38f976b8101305940317da0077efb63683dcde211064d206819ae6c5c145ff19df87cf934499ae636fd26e6d53681a2d20f,timestamp=1574384320",
        @Header("Session-Id") SessionId: String = "3037976474466916",
        @Body requestBody: RequestBody
    ): Response<List<FavoriteContentsModel>>

    @POST("eluniversal/favorites")
    suspend fun saveFavorite(
        @Header("Content-Type") contentType: String = "application/json",
        @Header("Authorization") Authorization: String = "EUL APIKey=KxOsNplAYNUB3hnTEt,Signature=9e0627060796e28acdef790f94ddb38f976b8101305940317da0077efb63683dcde211064d206819ae6c5c145ff19df87cf934499ae636fd26e6d53681a2d20f,timestamp=1574384320",
        @Header("Session-Id") SessionId: String = "3037976474466916",
        @Body requestBody: RequestBody
    ): Response<SaveFavoritesResponse>*/

}