package com.msi.eluniversal.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.math.BigInteger

data class SitesModel(
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("api_name")
    val api_name: String? = null,
    @SerializedName("description")
    val description: String? = null,
    @SerializedName("host")
    val host: String? = null,
    @SerializedName("adobe_id")
    val adobe_id: String? = null
): Serializable

data class SectionsModel(
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("cms_id")
    val cms_id: BigInteger? = null,
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("api_name")
    val api_name: String? = null,
    @SerializedName("description")
    val description: String? = null,
    @SerializedName("color")
    val color: String? = null,
    @SerializedName("site_sections")
    val site_sections: String? = null
): Serializable

data class ContentModel(
    @SerializedName("type")
    val type: String? = null,
    @SerializedName("plus")
    val plus: Int? = null,
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("cms_id")
    val cms_id: BigInteger? = null,
    @SerializedName("section_id")
    val section_id: Int? = null,
    @SerializedName("author_id")
    val author_id: Int? = null,
    @SerializedName("title")
    val title: String? = null,
    @SerializedName("summary")
    val summary: String? = null,
    @SerializedName("tags")
    val tags: String? = null,
    @SerializedName("urlGallery")
    val urlGallery: String? = null,
    @SerializedName("publication_date")
    val publication_date: String? = null,
    @SerializedName("body")
    val body: String? = null,
    @SerializedName("url")
    val url: String? = null,
    @SerializedName("summary_author")
    val summary_author: SummaryAuthorModel? = null,
    @SerializedName("section")
    val section: ContentDetailsModel? = null,
    @SerializedName("photos")
    val photos: List<PhotoModel?>? = null
): Serializable

data class ContentDetailsModel(
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("color")
    val color: String? = null
): Serializable

data class MinutoXMinutoContentModel(
    @SerializedName("plus")
    val plus: Int? = null,
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("cms_id")
    val cms_id: BigInteger? = null,
    @SerializedName("section_id")
    val section_id: Int? = null,
    @SerializedName("title")
    val title: String? = null,
    @SerializedName("mxm_title")
    val mxm_title: String? = null,
    @SerializedName("external_url")
    val external_url: String? = null,
    @SerializedName("summary")
    val summary: String? = null,
    @SerializedName("publication_date")
    val publication_date: String? = null,
    @SerializedName("hour_mxm")
    val hour_mxm: String? = null,
    @SerializedName("urlGallery")
    val urlGallery: String? = null,
    @SerializedName("type")
    val type: String? = null,
    @SerializedName("body")
    val body: String? = null,
    @SerializedName("section")
    val section: ContentDetailsModel? = null
): Serializable

data class OpinionContentModel(
    @SerializedName("type")
    val type: String? = null,
    @SerializedName("plus")
    val plus: Int? = null,
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("cms_id")
    val cms_id: BigInteger? = null,
    @SerializedName("section_id")
    val section_id: Int? = null,
    @SerializedName("author_id")
    val author_id: Int? = null,
    @SerializedName("title")
    val title: String? = null,
    @SerializedName("summary")
    val summary: String? = null,
    @SerializedName("tags")
    val tags: String? = null,
    @SerializedName("urlGallery")
    val urlGallery: String? = null,
    @SerializedName("publication_date")
    val publication_date: String? = null,
    @SerializedName("body")
    val body: String? = null,
    @SerializedName("url")
    val url: String? = null,
    @SerializedName("summary_author")
    val summary_author: SummaryAuthorModel? = null,
    @SerializedName("section")
    val section: ContentDetailsModel? = null
): Serializable

data class SummaryAuthorModel(
    @SerializedName("cms_id")
    val cms_id: Int? = null,
    @SerializedName("title")
    val title: String? = null,
    @SerializedName("epigraph")
    val epigraph: String? = null,
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("image")
    val image: String? = null,
    @SerializedName("status")
    val status: Int? = null
): Serializable

data class VideoContentModel(
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("title")
    val title: String? = null,
    @SerializedName("summary")
    val summary: String? = null,
    @SerializedName("url")
    val url: String? = null,
    @SerializedName("id_youtube")
    val id_youtube: String? = null,
    @SerializedName("largeThumbnail")
    val largeThumbnail: String? = null,
    @SerializedName("mediumThumbnail")
    val mediumThumbnail: String? = null,
    @SerializedName("publishedAt")
    val publishedAt: String? = null,
    @SerializedName("section")
    val section: String? = null,
    @SerializedName("section_url")
    val section_url: String? = null
): Serializable


data class GalleryContentModel(
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("cms_id")
    val cms_id: BigInteger? = null,
    @SerializedName("section_id")
    val section_id: Int? = null,
    @SerializedName("title")
    val title: String? = null,
    @SerializedName("summary")
    val summary: String? = null,
    @SerializedName("tags")
    val tags: String? = null,
    @SerializedName("urlGallery")
    val urlGallery: String? = null,
    @SerializedName("publication_date")
    val publication_date: String? = null,
    @SerializedName("body")
    val body: String? = null,
    @SerializedName("url")
    val url: String? = null,
    @SerializedName("related")
    val related: Int? = null,
    @SerializedName("section")
    val section: ContentDetailsModel?,
    @SerializedName("photos")
    val photos: List<PhotoModel?>? = null
): Serializable

data class PhotoModel(
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("content_id")
    val content_id: Int? = null,
    @SerializedName("url")
    val url: String? = null,
    @SerializedName("title")
    val title: String? = null,
    @SerializedName("description")
    val description: String? = null,
    @SerializedName("foot")
    val foot: String? = null
): Serializable

data class AdModel(
    @SerializedName("status")
    val status: Int? = null,
    @SerializedName("message")
    val message: String? = null,
    @SerializedName("platform")
    val platform: String? = null,
    @SerializedName("site")
    val site: String? = null,
    @SerializedName("device")
    val device: String? = null,
    @SerializedName("data")
    val data: List<AdDataModel?>? = null
): Serializable

data class AdDataModel(
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("position")
    val position: String? = null,
    @SerializedName("active")
    val active: Boolean? = null,
    @SerializedName("ad-unit-id")
    val ad_unit_id: String? = null,
    @SerializedName("width")
    val width: String? = null,
    @SerializedName("height")
    val height: String? = null
): Serializable


data class PushNotificationContentModel(
    @SerializedName("type")
    val type: String? = null,
    @SerializedName("plus")
    val plus: Int? = null,
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("cms_id")
    val cms_id: BigInteger? = null,
    @SerializedName("section_id")
    val section_id: Int? = null,
    @SerializedName("author_id")
    val author_id: Int? = null,
    @SerializedName("title")
    val title: String? = null,
    @SerializedName("summary")
    val summary: String? = null,
    @SerializedName("tags")
    val tags: String? = null,
    @SerializedName("urlGallery")
    val urlGallery: String? = null,
    @SerializedName("publication_date")
    val publication_date: String? = null,
    @SerializedName("body")
    val body: String? = null,
    @SerializedName("url")
    val url: String? = null,
    @SerializedName("author")
    val author: AuthorModel? = null,
    @SerializedName("section")
    val section: ContentDetailsModel? = null,
    @SerializedName("photos")
    val photos: List<PhotoModel?>? = null
): Serializable

data class AuthorModel(
    @SerializedName("cms_id")
    val cms_id: Int? = null,
    @SerializedName("title")
    val title: String? = null,
    @SerializedName("bio")
    val bio: String? = null,
    @SerializedName("description")
    val description: String? = null,
    @SerializedName("epigraph")
    val epigraph: String? = null,
    @SerializedName("name")
    val name: String? = null,
    @SerializedName("type")
    val type: String? = null,
    @SerializedName("link")
    val link: String? = null,
    @SerializedName("image")
    val image: String? = null,
    @SerializedName("image_caption")
    val image_caption: String? = null,
    @SerializedName("image_thumbnail")
    val image_thumbnail: String? = null,
    @SerializedName("image_small")
    val image_small: String? = null,
    @SerializedName("image_detail")
    val image_detail: String? = null,
    @SerializedName("email")
    val email: String? = null,
    @SerializedName("twitter")
    val twitter: String? = null,
    @SerializedName("facebook")
    val facebook: String? = null,
    @SerializedName("status")
    val status: Int? = null,
    @SerializedName("plus_columnist")
    val plus_columnist: Int? = null
): Serializable

// FAVORITES

data class FavoriteContentsModel(
    @SerializedName("fav_id")
    val fav_id: Int? = null,
    @SerializedName("type")
    val type: String? = null,
    @SerializedName("plus")
    val plus: Int? = null,
    @SerializedName("id")
    val id: Int? = null,
    @SerializedName("cms_id")
    val cms_id: BigInteger? = null,
    @SerializedName("title")
    val title: String? = null,
    @SerializedName("url")
    val url: String? = null,
    @SerializedName("section_id")
    val section_id: Int?= null,
    @SerializedName("summary")
    val summary: String? = null,
    @SerializedName("urlImage")
    val urlImage: String? = null,
    @SerializedName("publication_date")
    val publication_date: String? = null,
    @SerializedName("author_name")
    val author_name: String? = null,
    @SerializedName("author_content")
    val author_content: String? = null,
    @SerializedName("author_image")
    val author_image: String? = null,
    @SerializedName("author_link")
    val author_link: String? = null,
    @SerializedName("section")
    val section: ContentDetailsModel? = null
): Serializable

data class SaveFavoritesResponse(
    @SerializedName("code")
    val code: Int? = null,
    @SerializedName("message")
    val message: String? = null,
    @SerializedName("error")
    val error: ErrorFavoritesResponse? = null

): Serializable

data class ErrorFavoritesResponse(
    @SerializedName("type")
    val type: String? = null,
    @SerializedName("message")
    val message: String? = null,

): Serializable

//PianoAccess - ListCheck

data class ListCheckAccessModel(
    @SerializedName("code")
    val code: Int? = null,
    @SerializedName("ts")
    val ts: Int? = null,
    @SerializedName("limit")
    val limit: Int? = null,
    @SerializedName("offset")
    val offset: Int? = null,
    @SerializedName("total")
    val total: Int? = null,
    @SerializedName("count")
    val count: Int? = null,
    @SerializedName("data")
    val data: List<AccessModel?>? = null
): Serializable

//PianoAccess - CHECK
data class CheckAccessModel(
    @SerializedName("code")
    val code: Int? = null,
    @SerializedName("ts")
    val ts: Int? = null,
    @SerializedName("user")
    val user: UserModel? = null
): Serializable

data class UserModel(
    @SerializedName("first_name")
    val first_name: String? = null,
    @SerializedName("last_name")
    val last_name: String? = null,
    @SerializedName("personal_name")
    val personal_name: String? = null,
    @SerializedName("email")
    val email: String? = null,
    @SerializedName("uid")
    val uid: String? = null,
    @SerializedName("create_date")
    val create_date: Int? = null
): Serializable

data class AccessModel(
    @SerializedName("access_id")
    val access_id: String? = null,
    @SerializedName("granted")
    val granted: Boolean? = null,
    @SerializedName("resource")
    val resource: Resource? = null,
    @SerializedName("user")
    val user: User? = null,
    @SerializedName("expire_date")
    val expire_date: Int? = null
): Serializable

data class Access (
    @SerializedName("access_id")
    val access_id: String,
    @SerializedName("parent_access_id")
    val parent_access_id: String,
    @SerializedName("granted")
    val granted: Boolean,
    @SerializedName("user")
    val user: User,
    @SerializedName("resource")
    val resource: Resource,
    @SerializedName("expire_date")
    val expire_date: Int? = null,
    @SerializedName("start_date")
    val start_date: Int? = null,
    @SerializedName("can_revoke_access")
    val can_revoke_access: Boolean,
    @SerializedName("term")
    val term: Term
): Serializable

data class User(
    @SerializedName("uid")
    val uid: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("first_name")
    val first_name: String,
    @SerializedName("last_name")
    val last_name: String,
    @SerializedName("personal_name")
    val personal_name: String
): Serializable

data class  Resource(
    @SerializedName("rid")
    val rid: String,
    @SerializedName("aid")
    val aid: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("image_url")
    val image_url: String
): Serializable

data class Term(
    @SerializedName("term_id")
    val term_id: String,
    @SerializedName("aid")
    val aid: String,
    @SerializedName("resource")
    val resource: Resource,
    @SerializedName("type")
    val type: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("description")
    val description: String
): Serializable