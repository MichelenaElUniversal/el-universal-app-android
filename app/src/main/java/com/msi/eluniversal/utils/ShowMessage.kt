package com.msi.eluniversal.utils

import android.view.View
import com.google.android.material.snackbar.Snackbar

class ShowMessage {
    fun View.showMessage(message: String) {
        Snackbar.make(this, message, Snackbar.LENGTH_LONG).show()
    }
}