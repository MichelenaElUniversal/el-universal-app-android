package com.msi.eluniversal.utils

import java.text.Normalizer
import java.util.*

fun String.quitarAcentosYMinusculas(): String {
    val normalizedString = Normalizer.normalize(this, Normalizer.Form.NFD)
    val regex = Regex("[^\\p{ASCII}]")
    return regex.replace(normalizedString, "").toLowerCase(Locale.getDefault())
}