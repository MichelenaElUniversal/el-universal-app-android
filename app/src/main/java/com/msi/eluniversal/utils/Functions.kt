package com.msi.eluniversal.utils

import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import com.msi.eluniversal.models.ContentDetailsModel
import com.msi.eluniversal.models.ContentModel
import com.msi.eluniversal.models.PhotoModel
import com.msi.eluniversal.models.SectionsModel
import com.msi.eluniversal.models.SummaryAuthorModel
import com.orhanobut.hawk.Hawk
import java.io.Serializable

inline fun <reified T : Fragment> instanceOf(vararg params: Pair<String, Any>) =
    T::class.java.newInstance().apply {
        arguments = bundleOf(*params)
    }

fun setDataForDetails(data: ContentModel?){
    Hawk.put("plus", data?.plus)
    Hawk.put("cms_id", data?.cms_id)
    Hawk.put("section_id", data?.section_id)
    Hawk.put("author_id", data?.author_id)
    Hawk.put("title", data?.title)
    Hawk.put("summary", data?.summary)
    Hawk.put("urlGallery", data?.urlGallery)
    Hawk.put("publication_date", data?.publication_date)
    Hawk.put("body", data?.body)
    Hawk.put("url", data?.url)
    Hawk.put("summary_author", data?.summary_author)
    Hawk.put("opinionImage", data?.summary_author?.image)
    Hawk.put("sectionName", data?.section?.name)
    Hawk.put("sectionColor", data?.section?.color)
    Hawk.put("otherImage", data?.photos?.first()?.url)
}

data class DataForDetails(
    val plus: Int?,
    val cms_id: String?,
    val title: String?,
    val summary: String?,
    val urlGallery: String?,
    val publication_date: String?,
    val body: String?,
    val url: String?,
    val summary_author_image: String?,
    val sectionColor: String?,
    val otherImage: String?,
): Serializable
