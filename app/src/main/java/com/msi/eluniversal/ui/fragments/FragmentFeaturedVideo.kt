package com.msi.eluniversal.ui.fragments

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.activity.OnBackPressedCallback
import androidx.activity.OnBackPressedDispatcher
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.youtube.player.YouTubePlayer
import com.msi.eluniversal.R
import com.msi.eluniversal.databinding.FragmentVideoBinding
import com.msi.eluniversal.models.VideoContentModel
import com.msi.eluniversal.ui.viewModel.GeneralViewModel
import com.google.android.youtube.player.YouTubeStandalonePlayer
import com.msi.eluniversal.utils.Constants
import com.orhanobut.hawk.Hawk
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.PlayerConstants
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.FullscreenListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView
import dagger.hilt.android.AndroidEntryPoint
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.frameLayout
import org.jetbrains.anko.support.v4.act

@AndroidEntryPoint
class FragmentFeaturedVideo: Fragment(), AnkoLogger {

    private lateinit var binding: FragmentVideoBinding
    private val viewModel by viewModels<GeneralViewModel>()
    private var items: MutableList<Any> = mutableListOf()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_video, container, false)
        binding.viewModel = viewModel
        viewModel.videoAdapter.setContext(this.requireContext())
        binding.rvVideo.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        binding.contentError.visibility = View.GONE
        viewModel.doGetFeaturedVideoContent()
        Hawk.init(this@FragmentFeaturedVideo.requireContext()).build()
        val youtubeContainer = activity?.findViewById<FrameLayout>(R.id.youtubeVideoContainer)


        viewModel.responseVideoFeaturedContentModel.observe(this.viewLifecycleOwner, Observer {
            for (item in it.data ?: arrayListOf()){
                items.add(item)
            }
            viewModel.videoAdapter.updateList(items)
        })
        viewModel.videoAdapter.setClickAction {
            when(it){
                is VideoContentModel -> {
                    val urlP = it.largeThumbnail!!.removeRange(0, 23)
                    val url = urlP.removeRange(11, 25)
                    Hawk.put("youtubePlayer", url)
                    val youtubeView = YouTubePlayerView(this@FragmentFeaturedVideo.requireContext())
                    viewLifecycleOwner.lifecycle.addObserver(youtubeView)
                    youtubeContainer?.addView(youtubeView)
                    youtubeView.matchParent()
                    youtubeView.visibility = View.GONE
                    youtubeView.enableAutomaticInitialization = false
                    youtubeContainer?.visibility = View.VISIBLE
                    val listener: YouTubePlayerListener = object : AbstractYouTubePlayerListener(){
                        override fun onReady(youTubePlayer: com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer) {
                            youTubePlayer.loadVideo(url, 0f)
                            youTubePlayer.play()

                        }


                        override fun onStateChange(
                            youTubePlayer: com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer,
                            state: PlayerConstants.PlayerState
                        ) {
                            if (state == PlayerConstants.PlayerState.ENDED) {
                                youtubeContainer?.visibility = View.GONE
                                youtubeContainer?.removeAllViews()
                                activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                            } else if (state == PlayerConstants.PlayerState.PLAYING) {
                                activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR
                                youTubePlayer.toggleFullscreen()
                            }
                        }
                    }

                    val fullscreenListener = object : FullscreenListener {
                        override fun onEnterFullscreen(
                            fullscreenView: View,
                            exitFullscreen: () -> Unit
                        ) {
                            youtubeContainer?.addView(fullscreenView)
                        }

                        override fun onExitFullscreen() {
                            youtubeView.release()
                            youtubeContainer?.removeAllViews()
                            youtubeContainer?.visibility = View.GONE
                            activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                        }
                    }


                    youtubeView.addFullscreenListener(fullscreenListener)
                    youtubeView.initialize(listener)


                    // BACK

                    val onBackPressedCallback = object : OnBackPressedCallback(true) {
                        override fun handleOnBackPressed() {
                            youtubeContainer?.visibility = View.GONE
                            youtubeView.release()
                            youtubeContainer?.removeAllViews()
                            activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                        }
                    }

                    requireActivity().onBackPressedDispatcher.addCallback(
                        this@FragmentFeaturedVideo.viewLifecycleOwner,
                        onBackPressedCallback
                    )


                }
            }
        }
        return binding.root
    }
}