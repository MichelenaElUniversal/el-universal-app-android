package com.msi.eluniversal.ui.activity

import android.content.pm.ActivityInfo
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.msi.eluniversal.R
import com.msi.eluniversal.databinding.ActivityWebviewBinding
import com.msi.eluniversal.ui.viewModel.GeneralViewModel
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.sdk27.coroutines.onClick

@AndroidEntryPoint
class WebViewActivity: AppCompatActivity(), AnkoLogger {
    private lateinit var binding: ActivityWebviewBinding
    private val viewModel by viewModels<GeneralViewModel>()

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_webview)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        Hawk.init(this).build()

        val url = Hawk.get("linkUrl", "")

        /*binding.btnBack.onClick {
            finish()
        }

        binding.urlText.text = url

        binding.webview.settings.loadsImagesAutomatically = true
        binding.webview.settings.javaScriptEnabled = true
        binding.webview.scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY
        binding.webview.loadUrl(url!!)*/

    }
}