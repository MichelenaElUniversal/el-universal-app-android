package com.msi.eluniversal.ui.adapters

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.msi.eluniversal.R
import com.msi.eluniversal.base.BaseViewModel
import com.msi.eluniversal.databinding.VideoItemBinding
import com.msi.eluniversal.models.VideoContentModel
import com.orhanobut.hawk.Hawk
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.sdk27.coroutines.onClick

class VideoAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>(), AnkoLogger {
    private lateinit var items: MutableList<Any>
    private lateinit var bindingItem: VideoItemBinding
    private lateinit var context: Context

    private var listenerClick: ((item: Any) -> Unit)? = null

    @Suppress("unused")
    fun setClickAction(listenerClick: (item: Any) -> Unit){
        this.listenerClick = listenerClick
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        bindingItem = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.video_item,
            parent,
            false
        )
        return VideoItemViewHolder(bindingItem)
    }

    override fun getItemCount(): Int {
        return if (::items.isInitialized) items.size else 0
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.setIsRecyclable(false)
        when(holder){
            is VideoItemViewHolder -> {
                holder.bind(items[position] as VideoContentModel)
                bindingItem.root.setOnClickListener { listenerClick?.invoke(items[position]) }
                Glide.with(context)
                    .load((items[position] as VideoContentModel).mediumThumbnail)
                    .into(bindingItem.imagenVideo)
                bindingItem.btnShare.onClick {
                    val shareIntent = Intent(Intent.ACTION_SEND)
                    shareIntent.setType("text/plain")
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "El Universal")
                    val shareMessage = Hawk.get("urlNoticia", "") + "\n\n"
                    shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                    context.startActivity(Intent.createChooser(shareIntent, "Escoje tu red social"))
                }
            }
        }
    }

    fun setContext(context: Context){
        this.context = context
    }

    fun updateList(items: List<Any>){
        this.items = items.toMutableList()
        notifyDataSetChanged()
    }

    class VideoItemViewHolder(private val binding: VideoItemBinding) :
        RecyclerView.ViewHolder(binding.root){
        private val viewModel = VideoItemViewModel()
        fun bind(item: VideoContentModel){
            binding.viewModel = viewModel
            viewModel.bind(item)
        }
    }
}

class VideoItemViewModel: BaseViewModel(), AnkoLogger {
    val tituloVideo: MutableLiveData<String?> = MutableLiveData()
    val summaryVideo: MutableLiveData<String?> = MutableLiveData()
    val tipoVideo: MutableLiveData<String?> = MutableLiveData()
    val fechaVideo: MutableLiveData<String> = MutableLiveData()
    val horaVideo: MutableLiveData<String> = MutableLiveData()

    fun bind(item: VideoContentModel){
        tituloVideo.value = item.title
        summaryVideo.value = item.summary
        tipoVideo.value = item.section
        fechaVideo.value = item.publishedAt!!.removeRange(10,16)
        horaVideo.value = item.publishedAt!!.removeRange(0, 11)
    }
}







