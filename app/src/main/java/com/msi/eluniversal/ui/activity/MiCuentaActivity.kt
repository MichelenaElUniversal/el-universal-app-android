package com.msi.eluniversal.ui.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.msi.eluniversal.R
import com.msi.eluniversal.databinding.ActivityMicuentaBinding
import com.msi.eluniversal.models.FavoriteContentsModel
import com.msi.eluniversal.ui.viewModel.GeneralViewModel
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint
import io.piano.android.id.PianoId
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.startActivityForResult
import java.math.BigInteger

@AndroidEntryPoint
class MiCuentaActivity: AppCompatActivity(), AnkoLogger {

    private val viewModel by viewModels<GeneralViewModel>()
    private lateinit var binding: ActivityMicuentaBinding
    public var items: MutableList<Any> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this@MiCuentaActivity, R.layout.activity_micuenta)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        binding.viewModel = viewModel
        binding.rvMyfavorites.layoutManager =
            LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        viewModel.miCuentaAdapter.setContext(this)
        Hawk.init(this).build()

        val json = """{ "piano_id": "${Hawk.get("access_id", "")}" }"""
        val requestBody = json.toRequestBody("application/json".toMediaTypeOrNull())
        viewModel.doGetFavorites(requestBody = requestBody)

        val storedToken = PreferenceManager.getDefaultSharedPreferences(this).getString("token", "")
        val storedPermission =
            PreferenceManager.getDefaultSharedPreferences(this).getBoolean("granted", false)

        binding.logout.onClick {
            PianoId.signOut(Hawk.get("token", "")) {
                PreferenceManager.getDefaultSharedPreferences(this@MiCuentaActivity).edit()
                    .putString("token", "").apply()
                PreferenceManager.getDefaultSharedPreferences(this@MiCuentaActivity)
                    .edit().putBoolean("granted", false).apply()
                Hawk.delete("access_id")
                finish()
                startActivityWithoutStack(this@MiCuentaActivity, HomeActivity::class.java)

            }
        }

        viewModel.responseFavoriteModel.observe(this@MiCuentaActivity, Observer {

            items.clear()
            for(i in it.data ?: emptyList()) {
                items.add(i)
            }

            viewModel.miCuentaAdapter.updateList(items)

            if (items.isEmpty()){
                binding.noContent.visibility = View.VISIBLE
                binding.rvMyfavorites.visibility = View.GONE
            } else {
                binding.noContent.visibility = View.GONE
                binding.rvMyfavorites.visibility = View.VISIBLE

            }
        })


        viewModel.miCuentaAdapter.setClickAction {
            when(it){
                is FavoriteContentsModel -> {
                    if (it.plus == 1 && storedToken != "" && storedPermission){
                        val intent = Intent(this, DetailsActivity::class.java)
                        intent.putExtra("cmsId", "${it.cms_id}")
                        startActivity(intent)
                    } else if (it.plus == 1 && !storedPermission){
                        val mDialogView =
                            LayoutInflater.from(this)
                                .inflate(R.layout.alert_suscription, null)
                        val mBuilder = AlertDialog.Builder(this)
                            .setView(mDialogView).create()
                        mBuilder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                        val btnCerrar =
                            mDialogView.findViewById<Button>(R.id.btn_cerrar)

                        btnCerrar.setOnClickListener {
                            mBuilder.dismiss()
                        }
                        mBuilder.show()
                    } else {
                        val intent = Intent(this, DetailsActivity::class.java)
                        intent.putExtra("cmsId", "${it.cms_id}")
                        startActivity(intent)
                    }

                }
            }
        }
    }

    fun startActivityWithoutStack(activity: MiCuentaActivity, topActivityClass:Class<out Activity>){
        val intent = Intent(activity, topActivityClass)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.putExtra("showMessage", "send")
        this.startActivity(intent)
    }
}