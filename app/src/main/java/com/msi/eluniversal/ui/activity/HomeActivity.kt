package com.msi.eluniversal.ui.activity

import android.Manifest
import android.content.*
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.MenuItem
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.messaging.FirebaseMessaging
import com.msi.eluniversal.R
import com.msi.eluniversal.databinding.ActivityHomeBinding
import com.msi.eluniversal.injector.network.PianoAPI
import com.msi.eluniversal.models.CheckAccessModel
import com.msi.eluniversal.models.ListCheckAccessModel
import com.msi.eluniversal.models.SectionsModel
import com.msi.eluniversal.ui.fragments.*
import com.msi.eluniversal.ui.viewModel.GeneralViewModel
import com.msi.eluniversal.utils.extension.replaceFragment
import com.msi.eluniversal.utils.instanceOf
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint
import io.piano.android.id.*
import io.piano.android.id.models.PianoIdAuthFailureResult
import io.piano.android.id.models.PianoIdAuthSuccessResult
import okhttp3.OkHttpClient
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.startActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


@AndroidEntryPoint
@Suppress("DEPRECATION")
class HomeActivity : AppCompatActivity(), AnkoLogger, NavigationView.OnNavigationItemSelectedListener {

    private val viewModel by viewModels<GeneralViewModel>()
    private lateinit var binding: ActivityHomeBinding
    private var items: MutableList<Any> = mutableListOf()
    var menuItems: MutableList<SectionsModel> = mutableListOf()


    private lateinit var drawerLayout: DrawerLayout
    private lateinit var navView: NavigationView

    private var broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val notConnected = intent.getBooleanExtra(
                ConnectivityManager
                    .EXTRA_NO_CONNECTIVITY, false
            )
            if (notConnected) {
                disconnected()
            } else {
                connected()
            }
        }
        private fun disconnected() {
            binding.fullscreenContent.visibility = View.GONE
            binding.noConnectivity.visibility = View.VISIBLE
        }

        private fun connected() {
            binding.noConnectivity.visibility = View.GONE
            binding.fullscreenContent.visibility = View.VISIBLE
        }
    }

    val authResult = registerForActivityResult(PianoIdAuthResultContract()) { r ->
        when (r) {
            null -> { /* user cancelled Authorization process */ }
            is PianoIdAuthSuccessResult -> {
                val token = r.token?.accessToken
                // GET PIANO
                error { "PIANO TOKEN: " + token }
                showMessage("Iniciaste sesión correctamente.")
                Hawk.put("token", token)

                val requestCheck = PianoServiceBuilder.buildService(PianoAPI::class.java)
                val callCheck = requestCheck.getPianoAccess(user_token = token!!)
                callCheck.enqueue(object : Callback<CheckAccessModel>{
                    override fun onResponse(
                        call: Call<CheckAccessModel>,
                        response: Response<CheckAccessModel>
                    ) {
                        error { "USER DATA: ${response.body()?.user?.uid}" }
                        PreferenceManager.getDefaultSharedPreferences(applicationContext).edit().putBoolean("granted", false).apply()
                        Hawk.put("access_id", response.body()?.user?.uid)
                        finish()
                        startActivity(intent)
                    }

                    override fun onFailure(call: Call<CheckAccessModel>, t: Throwable) {
                        error { "Throwable Message: ${t.message}" }
                        error { "Throwable Cause: ${t.cause}" }
                    }

                })

                val request = PianoServiceBuilder.buildService(PianoAPI::class.java)
                val call = request.getPianoListAccess(user_token = token!!)
                call.enqueue(object : Callback<ListCheckAccessModel> {
                    override fun onResponse(
                        call: Call<ListCheckAccessModel>,
                        response: Response<ListCheckAccessModel>
                    ) {
                        for (i in response.body()!!.data ?: arrayListOf()){
                            PreferenceManager.getDefaultSharedPreferences(applicationContext).edit().putBoolean("granted", i!!.granted!!).apply()
                            Hawk.put("access_id", i.access_id)
                        }
                        finish()
                        startActivity(intent)
                    }
                    override fun onFailure(call: Call<ListCheckAccessModel>, t: Throwable) {
                        error { "Throwable Message: ${t.message}" }
                        error { "Throwable Cause: ${t.cause}" }
                    }
                })
                PreferenceManager.getDefaultSharedPreferences(this).edit().putString("token", r.token?.accessToken).apply()
                navView.menu.getItem(0).subMenu?.getItem(0)?.title = "Mi Cuenta"


            }
            is PianoIdAuthFailureResult -> {
                val e = r.exception
                showMessage("Ocurrio un error inesperado: " + e)
                error { "ERROR PIANO" + e.cause + e.message }
                // Authorization failed, check e.cause for details
            }
        }
    }

    private val requestPermissionLauncher = registerForActivityResult(
        ActivityResultContracts.RequestPermission(),
    ) { isGranted: Boolean ->
        if (isGranted) {
            // FCM SDK (and your app) can post notifications.
        } else {
            // TODO: Inform user that that your app will not show notifications.
        }
    }

    private fun askNotificationPermission() {
        // This is only necessary for API level >= 33 (TIRAMISU)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.POST_NOTIFICATIONS) ==
                PackageManager.PERMISSION_GRANTED
            ) {
                // FCM SDK (and your app) can post notifications.
            } else if (shouldShowRequestPermissionRationale(Manifest.permission.POST_NOTIFICATIONS)) {
                // TODO: display an educational UI explaining to the user the features that will be enabled
                //       by them granting the POST_NOTIFICATION permission. This UI should provide the user
                //       "OK" and "No thanks" buttons. If the user selects "OK," directly request the permission.
                //       If the user selects "No thanks," allow the user to continue without notifications.
            } else {
                // Directly ask for the permission
                requestPermissionLauncher.launch(Manifest.permission.POST_NOTIFICATIONS)
            }
        }
    }


    @RequiresApi(Build.VERSION_CODES.TIRAMISU)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home)
        binding.viewModel = viewModel
        binding.menuVideo.visibility = View.GONE
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        binding.rvMenuHome.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        viewModel.doGetFeaturedVideoContent()
        viewModel.doGetSections()
        viewModel.doGetVideoSections()

        viewModel.doAdContent()
        Hawk.init(this).build()
        viewModel.responseAdModel.observe(this, Observer {
            for (item in it.data?.data ?: arrayListOf()){
                if (item!!.name == "splash-home"){
                    Hawk.put("idAd", item.ad_unit_id)
                    error { item.ad_unit_id }
                }
            }
        })

        //Shared Preferences para el Token de Piano
        val storedToken = PreferenceManager.getDefaultSharedPreferences(this).getString("token", "")
        val storedPermission = PreferenceManager.getDefaultSharedPreferences(this).getBoolean("granted", false)
        error { "Stored Token: $storedToken" }
        error { "Permission: $storedPermission" }
        if (storedToken != ""){
            Hawk.put("storedToken", "Mi Cuenta")
        } else Hawk.put("storedToken", "Iniciar Sesión")


        // Cerrar sesión

        val intent = intent
        val showMessageAction = intent.getStringExtra("showMessageAction")

        if (showMessageAction == "send"){
            showMessage("Cerraste sesión correctamente.")
        }

        //NavigationBar
        drawerLayout = binding.drawerLayout
        navView = binding.navView

        navView.setNavigationItemSelectedListener(this)
        navView.menu.getItem(0).subMenu?.getItem(0)?.title = Hawk.get("storedToken", "Iniciar Sesión")

        viewModel.responseSectionsModel.observe(this@HomeActivity, Observer{
            val excludedIds = setOf(20, 23, 24, 25, 27, 79, 80, 130, 161, 162, 167, 168, 169, 181, 217, 218, 263) //270 EDO MEX
            val originalList: List<SectionsModel> = it.data ?: emptyList()
            val filteredList = originalList.filter { item -> item.id !in excludedIds }
            menuItems.addAll(filteredList)
            for(i in menuItems){
                if(i.site_sections != null) {
                    if (i.description != "Suplemento") {
                        navView.menu.getItem(1).subMenu?.add(i.api_name)?.title = i.name
                    } else {
                        navView.menu.getItem(2).subMenu?.add(i.api_name)?.title = i.name
                    }
                }
            }
        })

        binding.menu.setOnClickListener {
            drawerLayout.openDrawer(GravityCompat.END)
        }

        binding.search.setOnClickListener {
            Hawk.put("idSection", "home")
            Hawk.put("colorSection", "053A65")
            startActivity<SearchActivity>()
            //Snackbar.make(binding.contentView, "Por los momentos esta opción no está disponible", Snackbar.LENGTH_LONG).show()
        }

        binding.logo.onClick {
            setHomeItem(this@HomeActivity)
        }

        val fragmentManager: FragmentManager = supportFragmentManager
        val fragmentTransaction: FragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.contentView, FragmentHome(),"FragmentHome")
        fragmentTransaction.addToBackStack(null)
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
        fragmentTransaction.commit()

        replaceFragment(instanceOf<FragmentHome>(), R.id.contentView)


        binding.bottomNavigation.setOnNavigationItemSelectedListener { item ->
            when(item.itemId){
                R.id.action_home -> {
                    binding.menuVideo.visibility = View.GONE
                    showFragment(instanceOf<FragmentHome>("type" to 0))
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.action_minuto_x_minuto -> {
                    binding.menuVideo.visibility = View.GONE
                    replaceFragment(instanceOf<FragmentMinutoXMinuto>(), R.id.contentView)
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.action_videos -> {
                    binding.menuVideo.visibility = View.VISIBLE
                    replaceFragment(instanceOf<FragmentFeaturedVideo>(), R.id.contentView)
                    binding.destacados.onClick {
                        replaceFragment(instanceOf<FragmentFeaturedVideo>(), R.id.contentView)
                    }
                    viewModel.responseVideoSectionsModel.observe(this, Observer {
                        for (i in it.data ?: arrayListOf()){
                            items.add(i)
                        }
                        viewModel.videoMenuAdapter.updateList(items)
                        viewModel.videoMenuAdapter.setClickAction {
                            when (it){
                                is SectionsModel -> {
                                    Hawk.put("idSeccion", it.api_name)
                                    replaceFragment(instanceOf<FragmentVideo>(), R.id.contentView)
                                }
                            }
                        }
                    })
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.action_gallery -> {
                    binding.menuVideo.visibility = View.GONE
                    replaceFragment(instanceOf<FragmentGallery>(), R.id.contentView)
                    return@setOnNavigationItemSelectedListener true
                }
                else -> false
            }
        }

        // FIREBASE
        askNotificationPermission()

        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w("FIREBASE TOKEN", "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            val token = task.result

            // Log and toast
            Log.d("FIREBASE TOKEN", token)
        })
    }

    override fun onResume() {
        super.onResume()
    }

    private fun showFragment(fragment: Fragment) {
        return replaceFragment(fragment, binding.contentView.id)
    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.signIn){
            if (item.title == "Iniciar Sesión") {
                authResult.launch(
                    PianoId.getInstance().signIn().disableSignUp()
                )
            }  else if (item.title == "Mi Cuenta"){
                startActivity<MiCuentaActivity>()
            }
        } else if (item.itemId == R.id.home) {
            binding.menuVideo.visibility = View.GONE
            startActivity<HomeActivity>()
        } else if (item.itemId == R.id.settings) {
            binding.menuVideo.visibility = View.GONE
            startActivity<SettingsActivity>()
        } else {
            for (i in menuItems) {
                if (i.name == item.title && i.name != "Opinión"){
                    binding.menuVideo.visibility = View.GONE
                    Hawk.put("idSection", i.api_name)
                    Hawk.put("nameSection", i.name)
                    error{"NAME: ${i.name} ${i.api_name}" }
                    startActivity<SectionActivity>()
                } else if (i.name == item.title && i.name == "Opinión") {
                    binding.menuVideo.visibility = View.GONE
                    startActivity<OpinionActivity>()
                }
            }
        }
        drawerLayout.closeDrawer(GravityCompat.END)
        return true
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        if (binding.youtubeVideoContainer.visibility == View.VISIBLE) {
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        } else {
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val selectedItemId = binding.bottomNavigation.selectedItemId
        if (R.id.action_home != selectedItemId && binding.youtubeVideoContainer.visibility != View.VISIBLE) {
            setHomeItem(this)
        } else if (binding.youtubeVideoContainer.visibility == View.VISIBLE ) {
            binding.youtubeVideoContainer.visibility = View.GONE
        }else {
            super.onBackPressed()
        }
    }

    private fun setHomeItem(activity: HomeActivity){
        val bottomNavigationView = activity.findViewById(R.id.bottom_navigation) as BottomNavigationView
        bottomNavigationView.selectedItemId = R.id.action_home
    }

    override fun onStart() {
        super.onStart()
        registerReceiver(broadcastReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(broadcastReceiver)
    }

    fun showMessage(message: String) {
        Snackbar.make(binding.root, message, Snackbar.LENGTH_LONG).show()
    }

    object PianoServiceBuilder {
        private val client = OkHttpClient.Builder().build()

        private val retrofit = Retrofit.Builder()
            .baseUrl(PianoId.ENDPOINT_PRODUCTION)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
        fun<T> buildService(service: Class<T>): T{
            return retrofit.create(service)
        }
    }
}

