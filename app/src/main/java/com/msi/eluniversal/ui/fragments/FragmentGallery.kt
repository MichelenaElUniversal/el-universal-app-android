package com.msi.eluniversal.ui.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.msi.eluniversal.R
import com.msi.eluniversal.databinding.FragmentGaleriaBinding
import com.msi.eluniversal.models.GalleryContentModel
import com.msi.eluniversal.ui.activity.GalleryActivity
import com.msi.eluniversal.ui.viewModel.GeneralViewModel
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error
import org.jetbrains.anko.support.v4.startActivity

@AndroidEntryPoint
class FragmentGallery: Fragment(), AnkoLogger {
    private lateinit var binding: FragmentGaleriaBinding
    private val viewModel by viewModels<GeneralViewModel>()
    private var items: MutableList<Any> = mutableListOf()

    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_galeria, container, false)
        binding.viewModel = viewModel
        binding.rvGaleria.layoutManager = LinearLayoutManager(this.requireContext(), RecyclerView.VERTICAL, false)
        viewModel.galleryAdapter.setContext(this.requireContext())
        Hawk.init(this@FragmentGallery.requireContext()).build()
        viewModel.doGetGalleryContent()

        viewModel.responseGalleryContentModel.observe(this.viewLifecycleOwner, Observer {
            error { "Llega" }
            for (item in it.data ?: arrayListOf()){
                items.add(item)
            }
            viewModel.galleryAdapter.updateList(items)
        })

        viewModel.galleryAdapter.setClickAction {
            when (it){
                is GalleryContentModel -> {
                    Hawk.put("idGallery", it.id)
                    startActivity<GalleryActivity>()
                }
            }
        }

        return binding.root
    }
}