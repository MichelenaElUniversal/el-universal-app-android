package com.msi.eluniversal.ui.adapters

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.msi.eluniversal.R
import com.msi.eluniversal.base.BaseViewModel
import com.msi.eluniversal.databinding.ItemMicuentaBinding
import com.msi.eluniversal.models.FavoriteContentsModel
import org.jetbrains.anko.AnkoLogger

class MiCuentaAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>(), AnkoLogger {

    lateinit var items: MutableList<Any>
    lateinit var bindingItem: ItemMicuentaBinding
    private lateinit var context: Context

    private var listenerClick: ((item: Any) -> Unit)? = null

    @Suppress("unused")
    fun setClickAction(listenerClick: (item: Any) -> Unit){
        this.listenerClick = listenerClick
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        bindingItem = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_micuenta,
            parent,
            false
        )
        return MiCuentaItemViewHolder(bindingItem)
    }

    override fun getItemCount(): Int {
        return if (::items.isInitialized) items.size else 0
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.setIsRecyclable(false)
        when(holder){
            is MiCuentaItemViewHolder -> {
                holder.bind(items[position] as FavoriteContentsModel)
                bindingItem.root.setOnClickListener { listenerClick?.invoke(items[position]) }
                if ((items[position] as FavoriteContentsModel).plus == 1){
                    bindingItem.plusIcon.visibility = View.VISIBLE
                }
                val requestOptions = RequestOptions()
                requestOptions.placeholder(R.drawable.logo_el_universal)
                Glide.with(context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load((items[position] as FavoriteContentsModel).urlImage)
                    .into(bindingItem.favoriteImage)
            }
        }
    }

    fun setContext(context: Context){
        this.context = context
    }

    fun updateList(items: List<Any>){
        this.items = items.toMutableList()
        notifyDataSetChanged()
    }

    class MiCuentaItemViewHolder(private val binding: ItemMicuentaBinding) : RecyclerView.ViewHolder(binding.root){
        private val viewModel = MiCuentaItemViewModel()
        fun bind(item: FavoriteContentsModel){
            binding.viewModel = viewModel
            viewModel.bind(item)
            when(item.section!!.name){
                "Nación" -> binding.tipoNoticia.setTextColor(Color.parseColor("#${item.section.color}"))
                "Estados" -> binding.tipoNoticia.setTextColor(Color.parseColor("#${item.section.color}"))
                "Metrópoli" -> binding.tipoNoticia.setTextColor(Color.parseColor("#${item.section.color}"))
                "Mundo" -> binding.tipoNoticia.setTextColor(Color.parseColor("#${item.section.color}"))
                "Cartera" -> binding.tipoNoticia.setTextColor(Color.parseColor("#${item.section.color}"))
                "Cultura" -> binding.tipoNoticia.setTextColor(Color.parseColor("#${item.section.color}"))
                "Destinos" -> binding.tipoNoticia.setTextColor(Color.parseColor("#${item.section.color}"))
                "Espectáculos" -> binding.tipoNoticia.setTextColor(Color.parseColor("#${item.section.color}"))
            }
        }
    }
}

class MiCuentaItemViewModel: BaseViewModel(), AnkoLogger {
    val tituloNoticia: MutableLiveData<String?> = MutableLiveData()
    val tipoNoticia: MutableLiveData<String?> = MutableLiveData()

    fun bind(item: FavoriteContentsModel){
        tituloNoticia.value = item.title
        tipoNoticia.value = item.section!!.name
    }
}