package com.msi.eluniversal.ui.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.ActivityInfo
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.Button
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.msi.eluniversal.R
import com.msi.eluniversal.databinding.ActivityOpinionBinding
import com.msi.eluniversal.injector.network.PianoAPI
import com.msi.eluniversal.models.ListCheckAccessModel
import com.msi.eluniversal.models.OpinionContentModel
import com.msi.eluniversal.models.SectionsModel
import com.msi.eluniversal.ui.viewModel.GeneralViewModel
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint
import io.piano.android.id.PianoId
import io.piano.android.id.PianoIdAuthResultContract
import io.piano.android.id.models.PianoIdAuthFailureResult
import io.piano.android.id.models.PianoIdAuthSuccessResult
import okhttp3.OkHttpClient
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error
import org.jetbrains.anko.startActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@AndroidEntryPoint
class OpinionActivity : AppCompatActivity(), AnkoLogger, NavigationView.OnNavigationItemSelectedListener {

    private lateinit var binding: ActivityOpinionBinding
    private val viewModel by viewModels<GeneralViewModel>()
    var menuItems: MutableList<SectionsModel> = mutableListOf()
    private var items: MutableList<Any> = mutableListOf()
    private var itemsFirstLoad: MutableList<Any> = mutableListOf()
    private var itemsArticulist: MutableList<Any> = mutableListOf()
    private var itemsColumnist: MutableList<Any> = mutableListOf()
    val PIANO_ID_REQUEST_CODE = 1

    //NavigationBar
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var navView: NavigationView

    private var broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val notConnected = intent.getBooleanExtra(
                ConnectivityManager
                    .EXTRA_NO_CONNECTIVITY, false
            )
            if (notConnected) {
                disconnected()
            } else {
                connected()
            }
        }
        private fun disconnected() {
            binding.fullscreenContent.visibility = View.GONE
            binding.noConnectivity.visibility = View.VISIBLE
        }

        private fun connected() {
            binding.noConnectivity.visibility = View.GONE
            binding.fullscreenContent.visibility = View.VISIBLE
        }
    }

    private val authResult = registerForActivityResult(PianoIdAuthResultContract()) { r ->
        when (r) {
            null -> { /* user cancelled Authorization process */ }
            is PianoIdAuthSuccessResult -> {
                val token = r.token?.accessToken
                error { "PIANO TOKEN: " + token }
                showMessage("Iniciaste sesión correctamente verdadero.")
                Hawk.put("token", token)
                val request = HomeActivity.PianoServiceBuilder.buildService(PianoAPI::class.java)
                val call = request.getPianoListAccess(user_token = token!!)
                call.enqueue(object : Callback<ListCheckAccessModel> {
                    override fun onResponse(
                        call: Call<ListCheckAccessModel>,
                        response: Response<ListCheckAccessModel>
                    ) {
                        error { "USER DATA: " + response.body()?.data }
                        for (i in response.body()!!.data ?: arrayListOf()){
                            PreferenceManager.getDefaultSharedPreferences(applicationContext).edit().putBoolean("granted", i!!.granted!!).apply()
                        }
                        finish()
                        startActivity(intent)
                    }
                    override fun onFailure(call: Call<ListCheckAccessModel>, t: Throwable) {
                        error { "Throwable Message: ${t.message}" }
                        error { "Throwable Cause: ${t.cause}" }
                    }
                })
                PreferenceManager.getDefaultSharedPreferences(this).edit().putString("token", r.token?.accessToken).apply()
                navView.menu.getItem(0).subMenu?.getItem(0)?.title = "Cerrar Sesión"

            }
            is PianoIdAuthFailureResult -> {
                val e = r.exception
                showMessage("Ocurrio un error inesperado: " + e)
                error { "ERROR PIANO" + e.cause + e.message }
                // Authorization failed, check e.cause for details
            }
        }
    }

    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_opinion)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_opinion)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        binding.viewModel = viewModel
        binding.rvOpinion.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        binding.rvOpinion.setHasFixedSize(true)
        binding.rvOpinion.setItemViewCacheSize(20)
        binding.rvOpinion.isDrawingCacheEnabled = true
        binding.rvOpinion.drawingCacheQuality = View.DRAWING_CACHE_QUALITY_HIGH
        viewModel.opinionAdapter.setHasStableIds(true)
        Hawk.init(this).build()
        viewModel.opinionAdapter.setContext(this)
        viewModel.doGetOpinion()
        viewModel.doGetSections()
        viewModel.doGetVideoSections()

        val storedToken = PreferenceManager.getDefaultSharedPreferences(this).getString("token", "")
        val storedPermission = PreferenceManager.getDefaultSharedPreferences(this).getBoolean("granted", false)
        if (storedToken != ""){
            Hawk.put("storedToken", "Mi Cuenta")
        } else Hawk.put("storedToken", "Iniciar Sesión")



        //NavigationBar
        drawerLayout = binding.drawerLayout
        navView = binding.navView
        navView.menu.getItem(0).subMenu?.getItem(0)?.title = Hawk.get("storedToken", "Iniciar Sesión")


        navView.setNavigationItemSelectedListener(this)

        binding.menu.setOnClickListener {
            drawerLayout.openDrawer(GravityCompat.END)
        }

        viewModel.responseSectionsModel.observe(this@OpinionActivity, Observer{
            val excludedIds = setOf(20, 23, 24, 25, 27, 79, 80, 130, 161, 162, 167, 168, 169, 181, 217, 218, 263) //270 EDO MEX
            val originalList: List<SectionsModel> = it.data ?: emptyList()
            val filteredList = originalList.filter { item -> item.id !in excludedIds }
            menuItems.addAll(filteredList)
            for(i in menuItems){
                if(i.site_sections != null) {
                    if (i.description != "Suplemento") {
                        navView.menu.getItem(1).subMenu?.add(i.api_name)?.title = i.name
                    } else {
                        navView.menu.getItem(2).subMenu?.add(i.api_name)?.title = i.name
                    }
                }
            }
        })

        viewModel.responseOpinionContentModel.observe(this, Observer {
            for (item in it.data ?: arrayListOf()){
                if (item.type == "articulist"){
                    itemsArticulist.add(item)
                } else if (item.type == "columnist"){
                    itemsColumnist.add(item)
                }
                binding.segmentedGroup.setOnCheckedChangeListener { _, checkedId ->
                    when (checkedId) {
                        R.id.articulists -> {
                            items.clear()
                            if (item.type == "articulist"){
                                items.add(item)
                            }
                            viewModel.opinionAdapter.updateList(itemsArticulist)
                        }
                        R.id.columnists -> {
                            viewModel.opinionAdapter.updateList(itemsColumnist)
                        }
                    }
                }
            }
            viewModel.opinionAdapter.updateList(itemsColumnist)
        })

        binding.segmentedGroup.setOnCheckedChangeListener { _, checkedId ->
            when (checkedId) {
                R.id.articulists -> {
                    items.clear()
                    viewModel.opinionAdapter.updateList(items)
                    /*viewModel.responseOpinionContentModel.observe(this, Observer {
                        for (item in it ?: arrayListOf()){
                            if (item.type == "articulist"){
                                items.add(item)
                            }
                        }
                        viewModel.opinionAdapter.updateList(items)
                    })*/
                }
                R.id.columnists -> {
                    items.clear()
                    viewModel.responseOpinionContentModel.observe(this, Observer {
                        for (item in it.data ?: arrayListOf()){
                            if (item in it.data ?: arrayListOf()){
                                if (item.type == "columnist"){
                                    items.add(item)
                                }
                            }
                        }
                        viewModel.opinionAdapter.updateList(items)
                    })
                }
            }
        }

        viewModel.opinionAdapter.setClickAction {
            when(it){
                is OpinionContentModel -> {
                    error { storedToken }
                    if (it.plus == 1 && storedToken != "" && storedPermission){
                        binding.loading.visibility = View.VISIBLE
                        val intent = Intent(this, DetailsActivity::class.java)
                        intent.putExtra("cmsId", "${it.cms_id}")
                        startActivity(intent)
                    } else if (it.plus == 1 && !storedPermission){
                        val mDialogView =
                            LayoutInflater.from(this)
                                .inflate(R.layout.alert_suscription, null)
                        val mBuilder = AlertDialog.Builder(this)
                            .setView(mDialogView).create()
                        mBuilder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                        val btnCerrar =
                            mDialogView.findViewById<Button>(R.id.btn_cerrar)

                        btnCerrar.setOnClickListener {
                            mBuilder.dismiss()
                        }
                        mBuilder.show()
                    } else {
                        binding.loading.visibility = View.VISIBLE
                        val intent = Intent(this, DetailsActivity::class.java)
                        intent.putExtra("cmsId", "${it.cms_id}")
                        startActivity(intent)
                        /*error { "JSON ORIGINAL: ${it }}" }
                        Hawk.put("cms_id", it.cms_id)
                        Hawk.put("tituloNoticia", it.title)
                        Hawk.put("tipoNoticia", it.section!!.name)
                        Hawk.put("summaryNoticia", it.summary)
                        Hawk.put("imagenNoticia", it.summary_author?.image)
                        Hawk.put("fechaNoticia", it.publication_date)
                        Hawk.put("horaNoticia", it.publication_date)
                        Hawk.put("bodyNoticia", it.body)
                        Hawk.put("colorNoticia", it.section.color)
                        Hawk.put("urlNoticia", it.url)

                        startActivity<DetailsActivity>()*/
                    }
                }
            }
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.signIn){
            if (item.title == "Iniciar Sesión") {
                authResult.launch(
                    PianoId.getInstance().signIn().disableSignUp()
                )
            } else if (item.title == "Mi Cuenta") {
                startActivity<MiCuentaActivity>()
            } else {
                PianoId.signOut(Hawk.get("token", "")) {
                    showMessage("Has cerrado sesión correctamente.")
                    PreferenceManager.getDefaultSharedPreferences(this).edit()
                        .putString("token", "").apply()
                    PreferenceManager.getDefaultSharedPreferences(applicationContext)
                        .edit().putBoolean("granted", false).apply()
                    item.title = Hawk.get("cancelaTag", "Iniciar Sesión")
                    finish()
                    startActivity(intent)
                }
            }
        } else if (item.itemId == R.id.home) {
            startActivity<HomeActivity>()
        } else if (item.itemId == R.id.settings) {
            startActivity<SettingsActivity>()
        } else {
            for (i in menuItems) {
                if (i.name == item.title && i.name != "Opinión"){
                    Hawk.put("idSection", i.api_name)
                    Hawk.put("nameSection", i.name)
                    startActivity<SectionActivity>()
                } else if (i.name == item.title && i.name == "Opinión") {
                    startActivity<OpinionActivity>()
                }
            }
        }
        drawerLayout.closeDrawer(GravityCompat.END)
        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivityWithoutStack(this, HomeActivity::class.java)
    }

    override fun onStart() {
        super.onStart()
        registerReceiver(broadcastReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(broadcastReceiver)
    }

    override fun onResume() {
        super.onResume()
        binding.loading.visibility = View.GONE
    }

    private fun showMessage(message: String) {
        Snackbar.make(binding.root, message, Snackbar.LENGTH_LONG).show()
    }

    private fun startActivityWithoutStack(activity: OpinionActivity, topActivityClass:Class<out Activity>){
        val intent = Intent(activity, topActivityClass)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        this.startActivity(intent)
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == PIANO_ID_REQUEST_CODE) {}
/*            when (resultCode) {
                RESULT_OK ->
                    error { "exito" }
                    *//*runCatching {
                        val token = data.getPianoIdTokenResult()
                        showMessage("Iniciaste sesión correctamente.")
                        Hawk.put("token", token?.accessToken)
                        //PianoAccess
                        val request = PianoServiceBuilder.buildService(PianoAPI::class.java)
                        val call = request.getPianoAccess(Authorization = "Bearer ${token!!.accessToken}")
                        call.enqueue(object : Callback<CheckAccessModel> {
                            override fun onResponse(
                                call: Call<CheckAccessModel>,
                                response: Response<CheckAccessModel>
                            ) {
                                PreferenceManager.getDefaultSharedPreferences(applicationContext).edit().putBoolean("granted", response.body()!!.access!!.granted!!).apply()
                                finish()
                                startActivity(intent)
                            }
                            override fun onFailure(call: Call<CheckAccessModel>, t: Throwable) {
                                error { "Throwable Message: ${t.message}" }
                                error { "Throwable Cause: ${t.cause}" }
                            }
                        })
                        PreferenceManager.getDefaultSharedPreferences(this).edit().putString("token", token?.accessToken).apply()
                        navView.menu.getItem(0).subMenu?.getItem(0)?.title = "Cerrar Sesión"

                    }.onFailure {
                        // Authorization failed, check exception cause for details*//*
                    }
                RESULT_CANCELED ->
                {
                    error { "cancela" }
                    Hawk.put("cancelaTag", "Iniciar Sesión")
                    // user cancelled Authorization process
                }
                PianoId.RESULT_ERROR ->
                    runCatching {
                        showMessage("Ocurrió un error!")

                    }.onFailure {
                        error { "Failure" }
                        // Authorization failed, check exception cause for details
                    }*/

        else super.onActivityResult(requestCode, resultCode, data)
    }

    object PianoServiceBuilder {
        private val client = OkHttpClient.Builder().build()

        private val retrofit = Retrofit.Builder()
            .baseUrl(PianoId.ENDPOINT_PRODUCTION)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
        fun<T> buildService(service: Class<T>): T{
            return retrofit.create(service)
        }
    }
}