package com.msi.eluniversal.ui.fragments

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.msi.eluniversal.R
import com.msi.eluniversal.databinding.FragmentMinutoXMinutoBinding
import com.msi.eluniversal.models.ContentModel
import com.msi.eluniversal.models.MinutoXMinutoContentModel
import com.msi.eluniversal.ui.activity.DetailsActivity
import com.msi.eluniversal.ui.activity.PushNotificationDetailsActivity
import com.msi.eluniversal.ui.viewModel.GeneralViewModel
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error
import org.jetbrains.anko.support.v4.startActivity

@AndroidEntryPoint
class FragmentMinutoXMinuto: Fragment(), AnkoLogger {

    private lateinit var binding: FragmentMinutoXMinutoBinding
    private val viewModel by viewModels<GeneralViewModel>()
    private var items: MutableList<Any> = mutableListOf()

    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_minuto_x_minuto, container, false)
        binding.viewModel = viewModel
        binding.loading.visibility = View.GONE
        binding.rvMinutoXMinuto.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        viewModel.minutoXMinutoAdapter.setContext(this.requireContext())
        viewModel.doGetMinutoXMinutoContent()
        Hawk.init(this@FragmentMinutoXMinuto.requireContext()).build()
        val storedToken = PreferenceManager.getDefaultSharedPreferences(this.requireContext()).getString("token", "")
        val storedPermission = PreferenceManager.getDefaultSharedPreferences(this.requireContext()).getBoolean("granted", false)
        error { "StoredPermissionFragment: $storedPermission" }

        viewModel.responseMinutoXMinutoContentModel.observe(this@FragmentMinutoXMinuto.viewLifecycleOwner, Observer {
            for (item in it.data ?: arrayListOf()){
                items.add(item)
            }
            viewModel.minutoXMinutoAdapter.updateList(items)
        })

        binding.swipeRefresh.setColorSchemeColors(resources.getColor(R.color.background_darkblue))
        binding.swipeRefresh.setOnRefreshListener {
            items.clear()
            binding.swipeRefresh.isRefreshing = true
            handleTime()
        }

        viewModel.minutoXMinutoAdapter.setClickAction {
            when(it){
                is MinutoXMinutoContentModel -> {
                    error { storedToken }
                    if (it.plus == 1 && storedToken != "" && storedPermission){
                        binding.loading.visibility = View.VISIBLE
                        val intent = Intent(this@FragmentMinutoXMinuto.requireContext(), DetailsActivity::class.java)
                        intent.putExtra("cmsId", "${it.cms_id}")
                        startActivity(intent)
                    } else if (it.plus == 1 && !storedPermission){
                        val mDialogView =
                            LayoutInflater.from(this.requireContext())
                                .inflate(R.layout.alert_suscription, null)
                        val mBuilder = AlertDialog.Builder(this.requireContext())
                            .setView(mDialogView).create()
                        mBuilder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                        val btnCerrar =
                            mDialogView.findViewById<Button>(R.id.btn_cerrar)

                        btnCerrar.setOnClickListener {
                            mBuilder.dismiss()
                        }
                        mBuilder.show()
                    } else {
                        binding.loading.visibility = View.VISIBLE
                        val intent = Intent(this@FragmentMinutoXMinuto.requireContext(), DetailsActivity::class.java)
                        intent.putExtra("cmsId", "${it.cms_id}")
                        startActivity(intent)
                    }
                }
            }
        }


        return binding.root
    }

    fun handleTime(){
        Handler().postDelayed({
            binding.swipeRefresh.isRefreshing = false
            viewModel.doGetMinutoXMinutoContent()
            viewModel.minutoXMinutoAdapter.updateList(items)
        }, 2000)
    }

    override fun onResume() {
        super.onResume()
        binding.loading.visibility = View.GONE
    }
}