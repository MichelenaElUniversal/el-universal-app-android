package com.msi.eluniversal.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.msi.eluniversal.R
import com.msi.eluniversal.base.BaseViewModel
import com.msi.eluniversal.databinding.GaleriaItemPageBinding
import com.msi.eluniversal.models.PhotoModel
import org.jetbrains.anko.AnkoLogger

class GallerySliderAdapter: RecyclerView.Adapter<GallerySliderAdapter.Pager2ViewHolder>(){

    private lateinit var items: MutableList<Any>
    private lateinit var bindingItem: GaleriaItemPageBinding
    private lateinit var context: Context

    private var listenerClick: ((item: Any) -> Unit)? = null

    @Suppress("unused")
    fun setClickAction(listenerClick: (item: Any) -> Unit){
        this.listenerClick = listenerClick
    }

    override fun getItemCount(): Int {
        return if (::items.isInitialized) items.size else 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Pager2ViewHolder {
        bindingItem = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.galeria_item_page,
            parent,
            false
        )
        return Pager2ViewHolder(bindingItem)
    }

    override fun onBindViewHolder(holder: Pager2ViewHolder, position: Int) {
        holder.setIsRecyclable(false)
        when(holder){
            is Pager2ViewHolder -> {
                holder.bind(items[position] as PhotoModel)
                bindingItem.root.setOnClickListener { listenerClick?.invoke(items[position]) }
            Glide.with(context)
                .load((items[position] as PhotoModel).url)
                .into(bindingItem.imagenGaleriaPage)
            }
        }
    }

    fun setContext(context: Context){
        this.context = context
    }

    fun updateList(items: List<Any>){
        this.items = items.toMutableList()
        notifyDataSetChanged()
    }

    class Pager2ViewHolder(private val binding: GaleriaItemPageBinding): RecyclerView.ViewHolder(binding.root){
        private val viewModel = GalleryPageItemViewModel()
        fun bind(item: PhotoModel){
            binding.viewModel = viewModel
            viewModel.bind(item)
        }
    }
}

class GalleryPageItemViewModel: BaseViewModel(), AnkoLogger {
    val tituloGaleriaPage: MutableLiveData<String?> = MutableLiveData()
    val descripcionGaleriaPage: MutableLiveData<String?> = MutableLiveData()

    fun bind(item: PhotoModel){
        tituloGaleriaPage.value = item.title
        descripcionGaleriaPage.value = item.description
    }
}


