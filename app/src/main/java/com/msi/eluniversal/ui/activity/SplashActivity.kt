package com.msi.eluniversal.ui.activity

import android.annotation.SuppressLint
import android.content.*
import android.content.ContentValues.TAG
import android.content.pm.ActivityInfo
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.Window
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.ads.AdError
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.FullScreenContentCallback
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.admanager.AdManagerAdRequest
import com.google.android.gms.ads.admanager.AdManagerInterstitialAd
import com.google.android.gms.ads.admanager.AdManagerInterstitialAdLoadCallback
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.ktx.Firebase
import com.msi.eluniversal.R
import com.msi.eluniversal.databinding.ActivitySplashBinding
import com.msi.eluniversal.ui.viewModel.GeneralViewModel
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.ktx.messaging
import com.msi.eluniversal.utils.Constants.Companion.PIANO_AID
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint
import io.piano.android.id.PianoId
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error
import org.jetbrains.anko.startActivity
import java.math.BigInteger

@SuppressLint("CustomSplashScreen")
@Suppress("DEPRECATION")
class SplashActivity : AppCompatActivity(), AnkoLogger {

    private lateinit var binding: ActivitySplashBinding
    private var mAdManagerInterstitialAd: AdManagerInterstitialAd? = null
    private var adIsLoading: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.requestFeature(Window.FEATURE_NO_TITLE)
        Firebase.messaging.isAutoInitEnabled = true
        binding = DataBindingUtil.setContentView(this,
            R.layout.activity_splash
        )
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        Firebase.messaging.isAutoInitEnabled = true
        FirebaseAnalytics.getInstance(applicationContext).setAnalyticsCollectionEnabled(true)
        Firebase.messaging.subscribeToTopic("android")
            .addOnCompleteListener{ task ->
                var msg = "Firebase Topic Subscribed"
                if (!task.isSuccessful) {
                    msg = "Firebase Topic Subscribe failed"
                }
                Log.d(TAG, msg)

            }
        Hawk.init(this).build()
        MobileAds.initialize(this) {loadAd() }
        val adRequest = AdManagerAdRequest.Builder().build()
        AdManagerInterstitialAd.load(
            this,
            "/178068052/eluniversal_phone_app/android_phone/portada_takeover1",
            adRequest,
            object : AdManagerInterstitialAdLoadCallback(){
                override fun onAdFailedToLoad(adError: LoadAdError) {
                    Log.d(TAG, "${adError}")
                    mAdManagerInterstitialAd = null
                }

                override fun onAdLoaded(interstitialAd: AdManagerInterstitialAd) {
                    Log.d(TAG, "Ad was loaded.")
                    mAdManagerInterstitialAd = interstitialAd
                }

            })
        PianoId.init(PianoId.ENDPOINT_SANDBOX, PIANO_AID)
        if (intent.extras != null) {
            for (key in intent.extras!!.keySet()) {
                val value = intent.extras!!.get(key)
                if (key == "cms_id") {
                    error { "CMS_ID: $value" }
                } else if (key == "type"){
                    error { "ALERT TYPE: $value" }
                }
            }

            val typeValue = intent.getStringExtra("type")
            if (typeValue != null) {
                if (typeValue == "n") {
                    handlePushNotification()
                } else if (typeValue == "a") {
                    handleHomeActivity()
                }
            } else {
                handleTime()
            }
        }
    }

    private fun handlePushNotification() {
        val cmsId = intent.getStringExtra("cms_id")
        error { "CMS_ID A NOTIFICACION: $cmsId" }
        val detailsIntent = Intent(this@SplashActivity, DetailsActivity::class.java)
        detailsIntent.putExtra("cmsId", cmsId)
        startActivity(detailsIntent)
        finish()
    }

    private fun handleHomeActivity() {
        if (mAdManagerInterstitialAd != null) {
            mAdManagerInterstitialAd?.show(this)
        } else {
            val homeIntent = Intent(this@SplashActivity, HomeActivity::class.java)
            startActivity(homeIntent)
        }

        mAdManagerInterstitialAd?.fullScreenContentCallback = object : FullScreenContentCallback() {
            override fun onAdClicked() {
                // Called when a click is recorded for an ad.
                Log.d(TAG, "Ad was clicked.")
            }

            override fun onAdDismissedFullScreenContent() {
                // Called when ad is dismissed.
                Log.d(TAG, "Ad dismissed fullscreen content.")
                val intent = Intent(this@SplashActivity, HomeActivity::class.java)
                startActivity(intent)
            }

            override fun onAdImpression() {
                // Called when an impression is recorded for an ad.
                Log.d(TAG, "Ad recorded an impression.")
            }

            override fun onAdShowedFullScreenContent() {
                // Called when ad is shown.
                Log.d(TAG, "Ad showed fullscreen content.")
            }
        }

        MobileAds.initialize(this) { initializationStatus ->
            // Load an ad.
            loadAd()
        }
        finish()
    }


    fun handleTime(){
        Handler().postDelayed({
                if (mAdManagerInterstitialAd != null) {
                    mAdManagerInterstitialAd?.show(this)
                } else {
                    val intent = Intent(this@SplashActivity, HomeActivity::class.java)
                    startActivity(intent)
                }

                mAdManagerInterstitialAd?.fullScreenContentCallback = object: FullScreenContentCallback(){
                    override fun onAdClicked() {
                        // Called when a click is recorded for an ad.
                        Log.d(TAG, "Ad was clicked.")
                    }

                    override fun onAdDismissedFullScreenContent() {
                        // Called when ad is dismissed.
                        Log.d(TAG, "Ad dismissed fullscreen content.")
                        val intent = Intent(this@SplashActivity, HomeActivity::class.java)
                        startActivity(intent)
                    }

                    override fun onAdImpression() {
                        // Called when an impression is recorded for an ad.
                        Log.d(TAG, "Ad recorded an impression.")
                    }

                    override fun onAdShowedFullScreenContent() {
                        // Called when ad is shown.
                        Log.d(TAG, "Ad showed fullscreen content.")
                    }

            }
            mAdManagerInterstitialAd?.show(this)
            finish()
        }, 4500)
    }

    private fun loadAd() {
        if (adIsLoading || mAdManagerInterstitialAd != null) { return }
        adIsLoading = true

        val adRequest = AdManagerAdRequest.Builder().build()

        AdManagerInterstitialAd.load(
            this,
            "/178068052/eluniversal_phone_app/android_phone/portada_takeover1",
            adRequest,
            object : AdManagerInterstitialAdLoadCallback() {
                override fun onAdFailedToLoad(adError: LoadAdError) {
                    Log.d(TAG, "$adError")
                    mAdManagerInterstitialAd = null
                    val error =
                        "domain: ${adError.domain}, code: ${adError.code}, " + "message: ${adError.message}"
                }
                override fun onAdLoaded(interstitialAd: AdManagerInterstitialAd) {
                    Log.d(TAG, "Ad was loaded.")
                    mAdManagerInterstitialAd = interstitialAd
                    adIsLoading = false
                }
            })
    }



    private var broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val notConnected = intent.getBooleanExtra(
                ConnectivityManager
                    .EXTRA_NO_CONNECTIVITY, false
            )
            if (notConnected) {
                disconnected()
            } else {
                connected()
            }
        }
        private fun disconnected() {
            binding.fullscreenContent.visibility = View.GONE
            binding.noConnectivity.visibility = View.VISIBLE
        }

        private fun connected() {
            binding.noConnectivity.visibility = View.GONE
            binding.fullscreenContent.visibility = View.VISIBLE
            binding.logoSplash.visibility = View.VISIBLE
            handleTime()
        }
    }


        override fun onStart() {
        super.onStart()
        registerReceiver(broadcastReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(broadcastReceiver)
    }
}