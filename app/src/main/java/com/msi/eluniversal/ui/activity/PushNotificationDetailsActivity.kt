package com.msi.eluniversal.ui.activity

import android.content.*
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.Network
import android.net.Uri
import android.net.http.SslError
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.speech.tts.TextToSpeech
import android.view.View
import android.webkit.SslErrorHandler
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewModelScope
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdSize
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.admanager.AdManagerAdRequest
import com.google.android.gms.ads.admanager.AdManagerAdView
import com.google.android.material.snackbar.Snackbar
import com.msi.eluniversal.R
import com.msi.eluniversal.databinding.ActivityDetailsBinding
import com.msi.eluniversal.injector.network.ElUniversalAPI
import com.msi.eluniversal.injector.network.bases.NetworkResult
import com.msi.eluniversal.models.ContentModel
import com.msi.eluniversal.models.FavoriteContentsModel
import com.msi.eluniversal.ui.viewModel.GeneralViewModel
import com.msi.eluniversal.utils.quitarAcentosYMinusculas
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.textColor
import java.math.BigInteger
import java.util.*
import javax.annotation.Nullable


@AndroidEntryPoint
class PushNotificationDetailsActivity : AppCompatActivity(), AnkoLogger,
    TextToSpeech.OnInitListener {

    private lateinit var binding: ActivityDetailsBinding
    private val viewModel by viewModels<GeneralViewModel>()
    private lateinit var adBottom: AdManagerAdView
    private lateinit var adTop: AdManagerAdView

    private lateinit var textToSpeech: TextToSpeech
    private var isTtsInitialized = false
    private var isTtsPlaying = false

    private var isSoundOn = true
    private lateinit var soundIcon: ImageView

    private var favItems: MutableList<FavoriteContentsModel> = mutableListOf()
    private var favState = false
    private var favCount = 0

    private var broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val notConnected = intent.getBooleanExtra(
                ConnectivityManager
                    .EXTRA_NO_CONNECTIVITY, false
            )
            if (notConnected) {
                disconnected()
            } else {
                connected()
            }
        }

        private fun disconnected() {
            binding.fullscreenContent.visibility = View.GONE
            binding.noConnectivity.visibility = View.VISIBLE
        }

        private fun connected() {
            binding.noConnectivity.visibility = View.GONE
            binding.fullscreenContent.visibility = View.VISIBLE

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_details)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        binding.viewModel = viewModel
        Hawk.init(this).build()
        viewModel.miCuentaAdapter.setContext(this)

        val cms_idBigInt = Hawk.get<BigInteger>("cms_id", BigInteger.ZERO)
        val cms_id = cms_idBigInt.toString()
        viewModel.doGetPushDetails(cms_id = cms_id)

        MobileAds.initialize(this) {}
        val section = Hawk.get("tipoNoticia", "").quitarAcentosYMinusculas()
        val adRequest = AdManagerAdRequest.Builder().build()
        adTop = AdManagerAdView(this)
        adTop.setAdSize(AdSize.MEDIUM_RECTANGLE)
        //adTop.adUnitId = "/178068052/eluniversal_phone_app/$section/portada_top1"
        adTop.adUnitId = "/178068052/eluniversal_phone_app/home/portada_top1"
        binding.topContainer.addView(adTop)
        adTop.loadAd(adRequest)


        adTop.adListener = object : AdListener() {
            override fun onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            override fun onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }

            override fun onAdFailedToLoad(adError: LoadAdError) {
                // Code to be executed when an ad request fails.
                error { adError }
            }

            override fun onAdImpression() {
                // Code to be executed when an impression is recorded
                // for an ad.
            }

            override fun onAdLoaded() {
                // Code to be executed when an ad finishes loading.
                error { "ADLOADED" }
            }

            override fun onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }
        }

        adBottom = AdManagerAdView(this)
        adBottom.setAdSize(AdSize.BANNER)
        adBottom.adUnitId = "/178068052/eluniversal_phone_app/$section/noticia_top1"
        binding.bottomContainer.addView(adBottom)
        adBottom.loadAd(adRequest)

        adBottom.adListener = object: AdListener() {
            override fun onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            override fun onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }

            override fun onAdFailedToLoad(adError : LoadAdError) {
                // Code to be executed when an ad request fails.
                error { adError }
            }

            override fun onAdImpression() {
                // Code to be executed when an impression is recorded
                // for an ad.
            }

            override fun onAdLoaded() {
                // Code to be executed when an ad finishes loading.
                error { "ADLOADED" }
            }

            override fun onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }
        }


        textToSpeech = TextToSpeech(this, this)
        restartTextToSpeech()

        binding.tts.setOnClickListener {
            toggleTts()
        }

        if(Hawk.get("access_id", "") != "" || Hawk.get("access_id", "") != null) {
            binding.favoriteStatus.visibility = View.VISIBLE
            val json = """{ "piano_id": "${Hawk.get("access_id", "")}" }"""
            val requestBody = json.toRequestBody("application/json".toMediaTypeOrNull())
            viewModel.doGetFavorites(requestBody = requestBody)
        } else {
            binding.favoriteStatus.visibility = View.GONE
        }

        viewModel.doGetPushDetails(cms_id = cms_id)

        viewModel.responseFavoriteModel.observe(this@PushNotificationDetailsActivity, Observer { response ->
            favItems.clear()
            for (i in response.data ?: arrayListOf()) {
                favItems.add(i)
            }
            if (favItems.any { it.cms_id == Hawk.get("cms_id", BigInteger.ZERO) }) {
                favState = true
                val iconResource = R.drawable.ic_favorite_filled
                binding.favoriteStatus.setImageResource(iconResource)
                binding.favoriteStatus.setColorFilter(ContextCompat.getColor(applicationContext, R.color.red))
            } else {
                favState = false
                val iconResource = R.drawable.ic_favorite_outlined
                binding.favoriteStatus.setImageResource(iconResource)
                binding.favoriteStatus.setColorFilter(ContextCompat.getColor(applicationContext, R.color.grey))
            }
        })

        binding.favoriteStatus.onClick {
            when(favState){
                true -> {
                    showMessage("El contenido ya se encuentra agregado en tus favoritos.")
                }
                false -> {
                    val json = """{ "piano_id": "${Hawk.get("access_id", "")}", "cms_id": ${Hawk.get("cms_id", BigInteger.ZERO)} }"""
                    val requestBody = json.toRequestBody("application/json".toMediaTypeOrNull())
                    viewModel.postFavorite(requestBody = requestBody)
                }
            }
        }

        viewModel.responseSaveFavoriteModel.observe(this@PushNotificationDetailsActivity, Observer {
            if(it.data?.code == 1) {
                showMessage("${it.data.message}")
                favState = true
                favCount = 1
                val iconResource = R.drawable.ic_favorite_filled
                binding.favoriteStatus.setImageResource(iconResource)
                binding.favoriteStatus.setColorFilter(ContextCompat.getColor(applicationContext, R.color.red))
            } else {
                showMessage("${it.data?.error?.message}")
            }
        })



        binding.fullscreenContent.visibility = View.GONE
        binding.loading.visibility = View.VISIBLE


        viewModel.responsePushContentModel.observe(this@PushNotificationDetailsActivity, Observer {

            Hawk.put("tituloNoticia", it.data?.title).toString()
            binding.tituloNoticia.text = it.data?.title

            Hawk.put("summaryNoticia", it.data?.summary).toString()
            binding.sumarioNoticia.text = it.data?.summary

            Hawk.put("sectionNoticia", it.data?.section?.name)
            binding.tipoNoticia.text = it.data?.section?.name
            binding.tipoNoticia.setTextColor(Color.parseColor("#${it.data?.section?.color}"))

            if (it.data?.section?.name == "Opinión") {
                binding.topContainer.visibility = View.GONE
            }

            Hawk.put("hourNoticia", it.data?.publication_date?.removeRange(0, 11))
            binding.horaNoticia.text = it.data?.publication_date?.removeRange(0, 11)

            Hawk.put("fechaNoticia", it.data?.publication_date?.removeRange(10, 16))
            binding.fechaNoticia.text = it.data?.publication_date?.removeRange(10, 16)

            if (it.data?.urlGallery == null) {
                Hawk.put("imagenNoticia", it.data?.photos?.first()?.url)
            } else {
                Hawk.put("imagenNoticia", it.data.urlGallery)
            }


            Hawk.put("urlNoticia", it.data?.url)

            Glide.with(this)
                .load(it.data?.photos?.first()?.url)
                .placeholder(R.drawable.logo_el_universal)
                .apply(
                    RequestOptions()
                        .error(R.drawable.logo_el_universal)
                        .centerCrop()
                )
                .listener(object : RequestListener<Drawable?> {
                    override fun onLoadFailed(
                        @Nullable e: GlideException?,
                        model: Any,
                        target: Target<Drawable?>,
                        isFirstResource: Boolean
                    ): Boolean {
                        // on load failed
                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: Target<Drawable?>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        // on load success
                        return false
                    }
                })
                .transition(withCrossFade())
                .into(binding.imagenNoticia)


            Hawk.put("bodyNoticia", it.data?.body)

            binding.bodyNoticia.settings.javaScriptEnabled = true
            binding.bodyNoticia.settings.domStorageEnabled = true
            binding.bodyNoticia.webViewClient = object : WebViewClient() {
                //Intercepts SslError and proceed
                override fun onReceivedSslError(
                    view: WebView?,
                    handler: SslErrorHandler?,
                    error: SslError?
                ) {
                    val builder: AlertDialog.Builder =
                        AlertDialog.Builder(this@PushNotificationDetailsActivity)
                    builder.setMessage(R.string.ssl_cert)
                    builder.setPositiveButton(
                        "Continuar",
                        DialogInterface.OnClickListener { dialog, which ->
                            handler?.proceed()
                        })
                    builder.setNegativeButton(
                        "Cancelar",
                        DialogInterface.OnClickListener { dialog, which ->
                            handler?.cancel()
                        })
                    val dialog: AlertDialog = builder.create()
                    dialog.show()
                }

                override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                    return if (url.startsWith("http://") || url.startsWith("https://")) {
                        view.context.startActivity(
                            Intent(Intent.ACTION_VIEW, Uri.parse(url))
                        )
                        true
                    } else {
                        false
                    }
                }
            }
            openWebView()


            /*binding.bodyNoticia.loadDataWithBaseURL(null ,Hawk.get("bodyNoticia", ""), "text/html", "UTF-8", null)
        error { Hawk.get("bodyNoticia", "") }
        */
            handleTime()
        })

        /*val requestSaveBody = RequestBody.create(
            "application/json".toMediaTypeOrNull(), """{ "piano_id" : "${Hawk.get("access_id", "")}", "cms_id" : ${Hawk.get("cms_id", "")} }"""
        )

        binding.favoriteStatus.onClick {
            viewModel.doSaveFavorite(requestBody = requestSaveBody)
        }


        viewModel.responseSaveFavoritesResponse.observe(this@PushNotificationDetailsActivity, Observer {
            error { "ITEM DATA: " + it.message }
            if (it.data?.code == 1) {
                showMessage("${it.data?.message}")
                favState = true
                favCount = 1
            } else if (favState && favCount == 1) {
                showMessage("El contenido que intentas agregar ya fue agreagado anteriormente. Puede ser eliminado desde la sección Mi Cuenta.")
            } else {
                showMessage("Ocurrió un error inesperado con tu tarea, estamos trabajando en ello.")
            }
        })*/


        binding.logo.onClick {
            startActivity<HomeActivity>()
            finish()
        }

        binding.share.onClick {
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.setType("text/plain")
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "El Universal")
            val shareMessage = Hawk.get("urlNoticia", "") + "\n\n"
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
            startActivity(Intent.createChooser(shareIntent, "Escoje tu red social"))
        }
    }

    override fun onInit(status: Int) {
        if (status == TextToSpeech.SUCCESS) {
            isTtsInitialized = true
            val result = textToSpeech.setLanguage(Locale("es", "MEX "))
            if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                // Manejar según sea necesario si el idioma no está disponible
            }
        } else {
            // Manejar según sea necesario si la inicialización falla
        }
    }

    private fun toggleTts() {
        if (isTtsInitialized) {
            if (isTtsPlaying) {
                // Si está sonando, detén la reproducción
                textToSpeech.stop()
                isTtsPlaying = false
                binding.tts.setImageResource(R.drawable.ic_mute)
            } else {
                // Si no está sonando, inicia la reproducción
                val textToRead = Hawk.get("tituloNoticia", "") + Hawk.get("summaryNoticia", "") + Hawk.get("bodyNoticia", "")
                textToSpeech.speak(textToRead, TextToSpeech.QUEUE_FLUSH, null, null)
                isTtsPlaying = true
                binding.tts.setImageResource(R.drawable.ic_volume)
            }
        }
    }

    private fun stopTextToSpeech() {
        if (::textToSpeech.isInitialized && textToSpeech.isSpeaking) {
            textToSpeech.stop()
        }
    }

    private fun restartTextToSpeech() {
        stopTextToSpeech()

        // Re-crear la instancia de TextToSpeech
        textToSpeech = TextToSpeech(this, this)
    }

    //
    private fun openWebView() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            binding.bodyNoticia.settings.layoutAlgorithm =
                WebSettings.LayoutAlgorithm.TEXT_AUTOSIZING
        } else {
            binding.bodyNoticia.settings.layoutAlgorithm = WebSettings.LayoutAlgorithm.NORMAL
        }
        val data = "<div> ${Hawk.get("bodyNoticia", "")} </div>"
        binding.bodyNoticia.loadDataWithBaseURL(
            Hawk.get("urlNoticia", ""),
            getHtmlData(data, this),
            "text/html",
            "utf-8",
            null
        )
        Glide.with(applicationContext)
            .load(Hawk.get("imagenNoticia", ""))
            .placeholder(R.drawable.logo_el_universal)
            .into(binding.imagenNoticia)
    }

    private fun getHtmlData(bodyHtml: String, context: Context): String {
        val textColor = if (isDarkTheme(context)) "#FFFFFF" else "#000000" // Color del texto
        val backgroundColor = if (isDarkTheme(context)) "#000000" else "#FFFFFF" // Color de fondo

        val head =
            "<head><style>body{color: $textColor; background-color: $backgroundColor;} img{display: inline;height: auto;max-width: 100%;}</style></head>"
        return "<html>${head}<body><big>${bodyHtml}</big></body></html>"
    }


    fun handleTime() {
        Handler().postDelayed({
            binding.fullscreenContent.visibility = View.VISIBLE
            binding.loading.visibility = View.GONE
        }, 2000)
    }

    private fun isDarkTheme(context: Context): Boolean {
        val currentNightMode =
            context.resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK
        return currentNightMode == Configuration.UI_MODE_NIGHT_YES
    }


    fun showMessage(message: String) {
        Snackbar.make(binding.root, message, Snackbar.LENGTH_LONG).show()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        Hawk.delete("cms_id")
        Hawk.delete("imagenNoticia")
        if (isTtsInitialized) {
            textToSpeech.stop()
            textToSpeech.shutdown()
        }
        finish()
    }

    override fun onStart() {
        super.onStart()
        registerReceiver(broadcastReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(broadcastReceiver)
    }

    override fun onDestroy() {
        Hawk.delete("cms_id")
        if (isTtsInitialized) {
            textToSpeech.stop()
            textToSpeech.shutdown()
        }
        super.onDestroy()
    }

}