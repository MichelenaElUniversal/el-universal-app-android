package com.msi.eluniversal.ui.adapters

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.ads.admanager.AdManagerAdRequest
import com.google.android.gms.ads.admanager.AdManagerAdView
import com.msi.eluniversal.R
import com.msi.eluniversal.databinding.AdSectionItemBinding
import com.msi.eluniversal.databinding.HomeItemBinding
import com.msi.eluniversal.models.ContentModel
import com.msi.eluniversal.databinding.AdItemBinding
import org.jetbrains.anko.AnkoLogger


class SectionAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>(), AnkoLogger{
    private lateinit var items: MutableList<Any>
    private lateinit var bindingItem: HomeItemBinding
    private lateinit var bindingAd: AdSectionItemBinding
    private lateinit var context: Context
    private lateinit var mPublisherAdView: AdManagerAdView

    private var listenerClick: ((item: Any) -> Unit)? = null

    @Suppress("unused")
    fun setClickAction(listenerClick: (item: Any) -> Unit){
        this.listenerClick = listenerClick
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when (viewType) {
            0 -> {
                bindingItem = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.home_item,
                    parent,
                    false
                )
                return HomeItemViewHolder(bindingItem)
            }
            1 -> {
                bindingAd = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.ad_section_item,
                    parent,
                    false
                )
                return AdItemViewHolder(bindingAd)
            }
            else -> {
                bindingAd = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.ad_item,
                    parent,
                    false
                )
                return AdItemViewHolder(bindingAd)
            }
        }
    }

    override fun getItemCount(): Int {
        return if (::items.isInitialized) items.size else 0
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.setIsRecyclable(false)
        when(holder){
            is AdItemViewHolder -> {
                holder.bind("")
                mPublisherAdView = bindingAd.publisherAdView
                mPublisherAdView.loadAd(AdManagerAdRequest.Builder().build())
            }
            is HomeItemViewHolder -> {
                holder.bind(items[position] as ContentModel)
                bindingItem.root.setOnClickListener { listenerClick?.invoke(items[position]) }
                if ((items[position] as ContentModel).plus == 1){
                    bindingItem.plusIcon.visibility = View.VISIBLE
                }

                val requestOptions = RequestOptions()
                requestOptions.placeholder(R.drawable.logo_el_universal)
                Glide.with(context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load((items[position] as ContentModel).urlGallery)
                    .into(bindingItem.imagenNoticia)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position % 6 == 3){
            1
        } else {
            0
        }
        //return if (position == items.size) 1 else 0
    }


    fun setContext(context: Context){
        this.context = context
    }

    fun updateList(items: List<Any>){
        this.items = items.toMutableList()
        notifyDataSetChanged()
    }

    class HomeItemViewHolder(private val binding: HomeItemBinding) : RecyclerView.ViewHolder(binding.root){
        private val viewModel = HomeItemViewModel()
        fun bind(item: ContentModel) {
            binding.viewModel = viewModel
            viewModel.bind(item)
            when (item.section!!.name) {
                "Nación" -> binding.tipoNoticia.setTextColor(Color.parseColor("#${item.section.color}"))
                "Estados" -> binding.tipoNoticia.setTextColor(Color.parseColor("#${item.section.color}"))
                "Metrópoli" -> binding.tipoNoticia.setTextColor(Color.parseColor("#${item.section.color}"))
                "Mundo" -> binding.tipoNoticia.setTextColor(Color.parseColor("#${item.section.color}"))
                "Cartera" -> binding.tipoNoticia.setTextColor(Color.parseColor("#${item.section.color}"))
                "Cultura" -> binding.tipoNoticia.setTextColor(Color.parseColor("#${item.section.color}"))
                "Destinos" -> binding.tipoNoticia.setTextColor(Color.parseColor("#${item.section.color}"))
                "Espectáculos" -> binding.tipoNoticia.setTextColor(Color.parseColor("#${item.section.color}"))
            }
        }
    }

    class AdItemViewHolder(private val binding: AdSectionItemBinding) : RecyclerView.ViewHolder(binding.root){
        private val viewModel = HomeItemViewModel()
        fun bind(item: String){
            viewModel.bind(item)
        }
    }
}