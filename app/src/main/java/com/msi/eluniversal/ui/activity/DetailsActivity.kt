package com.msi.eluniversal.ui.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.content.*
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.Uri
import android.net.http.SslError
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.RemoteException
import android.preference.PreferenceManager
import android.provider.ContactsContract.Data
import android.speech.tts.TextToSpeech
import android.view.LayoutInflater
import android.view.View
import android.webkit.*
import android.widget.Button
import android.widget.ImageView
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdSize
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.admanager.AdManagerAdRequest
import com.google.android.gms.ads.admanager.AdManagerAdView
import com.google.android.material.snackbar.Snackbar
import com.msi.eluniversal.R
import com.msi.eluniversal.databinding.ActivityDetailsBinding
import com.msi.eluniversal.models.ContentModel
import com.msi.eluniversal.models.FavoriteContentsModel
import com.msi.eluniversal.models.PushNotificationContentModel
import com.msi.eluniversal.models.SectionsModel
import com.msi.eluniversal.ui.viewModel.GeneralViewModel
import com.msi.eluniversal.utils.DataForDetails
import com.msi.eluniversal.utils.quitarAcentosYMinusculas
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.startActivity
import java.math.BigInteger
import java.net.URI
import java.net.URL
import java.util.*
import javax.annotation.Nullable


@AndroidEntryPoint
class DetailsActivity : AppCompatActivity(), AnkoLogger, TextToSpeech.OnInitListener {
    private val viewModel by viewModels<GeneralViewModel>()
    private lateinit var binding: ActivityDetailsBinding
    private lateinit var adBottom: AdManagerAdView
    private lateinit var adTop: AdManagerAdView

    private lateinit var textToSpeech: TextToSpeech
    private var isTtsPlaying = false
    private var textToRead = ""


    private var favItems: MutableList<FavoriteContentsModel> = mutableListOf()
    private var favState = false
    private var favCount = 0

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_details)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        binding.viewModel = viewModel
        Hawk.init(this).build()
        binding.loading.visibility = View.VISIBLE
        MobileAds.initialize(this) {}
        textToSpeech = TextToSpeech(this, this)

        val storedPermission = PreferenceManager.getDefaultSharedPreferences(this).getBoolean("granted", false)
        val storedToken = PreferenceManager.getDefaultSharedPreferences(this).getString("token", "")

        if(Hawk.get("access_id", "") != "" || Hawk.get("access_id", "") != null) {
            binding.favoriteStatus.visibility = View.VISIBLE
            val json = """{ "piano_id": "${Hawk.get("access_id", "")}" }"""
            val requestBody = json.toRequestBody("application/json".toMediaTypeOrNull())
            viewModel.doGetFavorites(requestBody = requestBody)
        } else {
            binding.favoriteStatus.visibility = View.GONE
        }

        val cmsId: String? = intent.getStringExtra("cmsId")
        if (cmsId != null) {
            viewModel.doGetPushDetails(cmsId)

        }

        viewModel.responseFavoriteModel.observe(this@DetailsActivity, Observer { response ->
            favItems.clear()
            for (i in response.data ?: arrayListOf()){
                favItems.add(i)
            }

            val hasMatchingItem = favItems.any { it.cms_id == cmsId?.toBigInteger() }

            if (hasMatchingItem) {
                favState = true
                val iconResource = R.drawable.ic_favorite_filled
                binding.favoriteStatus.setImageResource(iconResource)
                binding.favoriteStatus.setColorFilter(ContextCompat.getColor(applicationContext, R.color.red))
            } else {
                favState = false
                val iconResource = R.drawable.ic_favorite_outlined
                binding.favoriteStatus.setImageResource(iconResource)
                binding.favoriteStatus.setColorFilter(ContextCompat.getColor(applicationContext, R.color.grey))
            }
        })

        viewModel.responsePushContentModel.observe(this@DetailsActivity, Observer {

            /** DATA */

            val data = it.data

            /** TEXT ATTRIBUTES */

            binding.tituloNoticia.text = data?.title
            binding.sumarioNoticia.text = data?.summary
            binding.horaNoticia.text = data?.publication_date?.removeRange(0,11)
            binding.fechaNoticia.text = data?.publication_date?.removeRange(10,16)
            binding.tipoNoticia.text = data?.section?.name
            binding.tipoNoticia.setTextColor(Color.parseColor("#${data?.section?.color}"))

            /** IMAGE */

            val urlGallery = data?.urlGallery
            val firstPhotoUrl = data?.photos?.firstOrNull()?.url
            val authorImage = data?.author?.image

            val imageUrl = when {
                !urlGallery.isNullOrBlank() -> urlGallery
                !firstPhotoUrl.isNullOrBlank() -> firstPhotoUrl
                authorImage != null -> authorImage
                else -> null
            }

            Glide.with(this)
                .load(imageUrl)
                .placeholder(R.drawable.logo_el_universal)
                .apply(
                    RequestOptions()
                        .error(R.drawable.logo_el_universal)
                        .centerCrop()
                )
                .listener(object : RequestListener<Drawable?> {
                    override fun onLoadFailed(
                        @Nullable e: GlideException?,
                        model: Any,
                        target: Target<Drawable?>,
                        isFirstResource: Boolean
                    ): Boolean {
                        // on load failed
                        return false
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: Target<Drawable?>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        // on load success
                        return false
                    }
                })
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(binding.imagenNoticia)

            /** BODY */

            binding.bodyNoticia.settings.javaScriptEnabled = true
            binding.bodyNoticia.settings.domStorageEnabled = true
            binding.bodyNoticia.webViewClient = object : WebViewClient() {
                //Intercepts SslError and proceed
                override fun onReceivedSslError(
                    view: WebView?,
                    handler: SslErrorHandler?,
                    error: SslError?
                ) {
                    val builder: AlertDialog.Builder =
                        AlertDialog.Builder(this@DetailsActivity)
                    builder.setMessage(R.string.ssl_cert)
                    builder.setPositiveButton(
                        "Continuar",
                        DialogInterface.OnClickListener { dialog, which ->
                            handler?.proceed()
                        })
                    builder.setNegativeButton(
                        "Cancelar",
                        DialogInterface.OnClickListener { dialog, which ->
                            handler?.cancel()
                        })
                    val dialog: AlertDialog = builder.create()
                    dialog.show()
                }

                override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                    return if (url.startsWith("http://") || url.startsWith("https://")) {
                        view.context.startActivity(
                            Intent(Intent.ACTION_VIEW, Uri.parse(url))
                        )
                        true
                    } else {
                        false
                    }
                }
            }
            openWebView(data?.url, data?.body!!)

            /** CHECK USER STATUS */

            if (data.plus == 1 && !storedPermission) {
                onlyPlusUsers()
            } else {
                error { "Usuario regular" }
            }

            /** FAVORITES ON CLICK */

            if (storedToken != "") {
                binding.favoriteStatus.visibility = View.VISIBLE
            } else {
                binding.favoriteStatus.visibility = View.GONE
            }

            binding.favoriteStatus.onClick {
            when(favState){
                    true -> {
                        showMessage("El contenido ya se encuentra agregado en tus favoritos.")
                    }
                    false -> {
                        val json = """{ "piano_id": "${Hawk.get("access_id", "")}", "cms_id": ${data.cms_id?.toInt()} }"""
                        val requestBody = json.toRequestBody("application/json".toMediaTypeOrNull())
                        viewModel.postFavorite(requestBody = requestBody)
                    }
                }
            }

            viewModel.responseSaveFavoriteModel.observe(this@DetailsActivity, Observer { response ->
                if(response.data?.code == 1) {
                    showMessage("${response.data.message}")
                    favState = true
                    favCount = 1
                    val iconResource = R.drawable.ic_favorite_filled
                    binding.favoriteStatus.setImageResource(iconResource)
                    binding.favoriteStatus.setColorFilter(ContextCompat.getColor(applicationContext, R.color.red))
                } else {
                    showMessage("${response.data?.error?.message}")
                }
            })


            /** CLICK ON LOGO AND SHARE */

            binding.logo.onClick {
                startActivity<HomeActivity>()
                finish()
            }

            binding.share.onClick {
                val shareIntent = Intent(Intent.ACTION_SEND)
                shareIntent.type = "text/plain"
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "El Universal")
                val shareMessage = data.url + "\n\n"
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                startActivity(Intent.createChooser(shareIntent, "Escoje tu red social"))
            }

            /** TEXT TO SPEECH */

            textToRead = "${data.summary}"

            binding.tts.setOnClickListener {
                if (isTtsPlaying) {
                    binding.tts.setImageResource(R.drawable.ic_mute)
                    isTtsPlaying = false
                    textToSpeech.stop()
                }  else {
                    binding.tts.setImageResource(R.drawable.ic_volume)
                    isTtsPlaying = true
                    textToSpeech.speak(data.title + data.summary + data.body, TextToSpeech.QUEUE_FLUSH, null, null)
                }
            }

            /** SETUP ADS*/

            val section = data.section?.name?.quitarAcentosYMinusculas()

            if (data.plus == 1 || data.section?.name == "Opinión") {
                binding.topContainer.visibility = View.GONE
                adBottom = AdManagerAdView(this)
                adBottom.setAdSize(AdSize.BANNER)
                adBottom.adUnitId = "/178068052/eluniversal_phone_app/$section/noticia_right1"
                val adRequest = AdManagerAdRequest.Builder().build()
                binding.bottomContainer.addView(adBottom)
                adBottom.loadAd(adRequest)
            } else {
                binding.topContainer.visibility = View.VISIBLE
                adTop = AdManagerAdView(this)
                val adRequest = AdManagerAdRequest.Builder().build()
                adTop.adUnitId = "/178068052/eluniversal_phone_app/home/portada_top1"
                adTop.setAdSize(AdSize.MEDIUM_RECTANGLE)
                binding.topContainer.addView(adTop)
                adTop.loadAd(adRequest)

                adBottom = AdManagerAdView(this)
                adBottom.adUnitId = "/178068052/eluniversal_phone_app/$section/noticia_top1"
                adBottom.setAdSize(AdSize.BANNER)
                binding.bottomContainer.addView(adBottom)
                adBottom.loadAd(adRequest)
            }
            binding.loading.visibility = View.GONE
        })
    }


    /** HANDLE HTML & JAVASCRIPT */

    private fun openWebView(url: String?, body: String?) {
        binding.bodyNoticia.settings.layoutAlgorithm =
            WebSettings.LayoutAlgorithm.TEXT_AUTOSIZING
        val data = "<div> $body </div>"
        binding.bodyNoticia.loadDataWithBaseURL(
            url,
            getHtmlData(data, this),
            "text/html",
            "utf-8",
            null
        )
        binding.loading.visibility = View.GONE
    }

    private fun getHtmlData(bodyHtml: String, context: Context): String {
        val textColor = if (isDarkTheme(context)) "#FFFFFF" else "#000000" // Color del texto
        val backgroundColor = if (isDarkTheme(context)) "#000000" else "#FFFFFF" // Color de fondo

        val head =
            "<head><style>body{color: $textColor; background-color: $backgroundColor;} img{display: inline;height: auto;max-width: 100%;}</style></head>"
        return "<html>${head}<body><big>${bodyHtml}</big></body></html>"
    }

    // HANDLE DARK MODE

    private fun isDarkTheme(context: Context): Boolean {
        val currentNightMode =
            context.resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK
        return currentNightMode == Configuration.UI_MODE_NIGHT_YES
    }


    override fun onInit(status: Int) {
        if (status == TextToSpeech.SUCCESS) {
            binding.loading.visibility = View.GONE
            val result = textToSpeech.setLanguage(Locale("es", "MX"))
            if (result == TextToSpeech.LANG_MISSING_DATA ||
                result == TextToSpeech.LANG_NOT_SUPPORTED
            ) {
                error { "Lenguaje no soportado" }
            }
        } else {
            error { "Inicialización fallida" }
        }
    }

    /** BROADCAST RECEIVER */

    private var broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val notConnected = intent.getBooleanExtra(
                ConnectivityManager
                    .EXTRA_NO_CONNECTIVITY, false
            )
            if (notConnected) {
                disconnected()
            } else {
                connected()
            }
        }
        private fun disconnected() {
            binding.fullscreenContent.visibility = View.GONE
            binding.noConnectivity.visibility = View.VISIBLE
        }

        private fun connected() {
            binding.noConnectivity.visibility = View.GONE
            binding.fullscreenContent.visibility = View.VISIBLE

        }
    }

    override fun onStart() {
        super.onStart()
        registerReceiver(broadcastReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(broadcastReceiver)
    }

    override fun onDestroy() {
        if (::textToSpeech.isInitialized) {
            textToSpeech.stop()
            textToSpeech.shutdown()
        }
        super.onDestroy()
    }

    /** RESTRICTION - ONLY PLUS USERS */
    private fun onlyPlusUsers() {
        val mDialogView =
            LayoutInflater.from(this)
                .inflate(R.layout.alert_suscription, null)
        val mBuilder = AlertDialog.Builder(this)
            .setView(mDialogView).create()
        mBuilder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val btnCerrar =
            mDialogView.findViewById<Button>(R.id.btn_cerrar)

        btnCerrar.setOnClickListener {
            mBuilder.dismiss()
            backHome()
        }
        mBuilder.show()
    }

    private fun backHome() {
        finish()
    }

    /** SIMPLE SNACKBAR */

    fun showMessage(message: String) {
        Snackbar.make(binding.root, message, Snackbar.LENGTH_LONG).show()
    }
}