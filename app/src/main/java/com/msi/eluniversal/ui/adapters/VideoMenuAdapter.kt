package com.msi.eluniversal.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.msi.eluniversal.R
import com.msi.eluniversal.base.BaseViewModel
import com.msi.eluniversal.databinding.HomeVideoItemBinding
import com.msi.eluniversal.models.SectionsModel
import org.jetbrains.anko.AnkoLogger
import java.util.*

class VideoMenuAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>(), AnkoLogger {
    private lateinit var items: MutableList<Any>
    private lateinit var bindingItem: HomeVideoItemBinding
    private lateinit var context: Context

    private var listenerClick: ((item: Any) -> Unit)? = null

    @Suppress("unused")
    fun setClickAction(listenerClick: (item: Any) -> Unit){
        this.listenerClick = listenerClick
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        bindingItem = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.home_video_item,
            parent,
            false
        )
        return VideoMenuItemViewHolder(bindingItem)
    }

    override fun getItemCount(): Int {
        return if (::items.isInitialized) items.size else 0
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.setIsRecyclable(false)
        when(holder){
            is VideoMenuItemViewHolder -> {
                holder.bind(items[position] as SectionsModel)
                bindingItem.root.setOnClickListener { listenerClick?.invoke(items[position]) }
            }
        }
    }

    fun setContext(context: Context){
        this.context = context
    }

    fun updateList(items: List<Any>){
        this.items = items.toMutableList()
        notifyDataSetChanged()
    }

    class VideoMenuItemViewHolder(private val binding: HomeVideoItemBinding) :
        RecyclerView.ViewHolder(binding.root){
        private val viewModel = VideoMenuItemViewModel()
        fun bind(item: SectionsModel){
            binding.viewModel = viewModel
            viewModel.bind(item)
        }
    }
}

class VideoMenuItemViewModel: BaseViewModel(), AnkoLogger {
    val tipoNoticia: MutableLiveData<String> = MutableLiveData()

    fun bind(item: SectionsModel){
        tipoNoticia.value = item.name!!.toUpperCase(Locale.ROOT)
    }
}