package com.msi.eluniversal.ui.fragments

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.preference.PreferenceManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.msi.eluniversal.R
import com.msi.eluniversal.databinding.FragmentHomeBinding
import com.msi.eluniversal.injector.network.bases.NetworkResult
import com.msi.eluniversal.models.ContentModel
import com.msi.eluniversal.ui.activity.DetailsActivity
import com.msi.eluniversal.ui.activity.PlusDetailsActivity
import com.msi.eluniversal.ui.activity.PushNotificationDetailsActivity
import com.msi.eluniversal.ui.viewModel.GeneralViewModel
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint
import okhttp3.OkHttpClient
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error
import org.jetbrains.anko.support.v4.startActivity

@AndroidEntryPoint
class FragmentHome: Fragment(), AnkoLogger{

    private lateinit var binding: FragmentHomeBinding
    private val viewModel by viewModels<GeneralViewModel>()
    private var items: MutableList<Any> = mutableListOf()
    var isLoading = false
    val limit = 10
    var page = 0

    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        binding.viewModel = viewModel
        binding.loading.visibility = View.VISIBLE
        binding.rvHome.isClickable = false
        val layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        binding.rvHome.layoutManager = layoutManager
        viewModel.homeAdapter.setContext(this.requireContext())
        viewModel.doGetContent(seccion = "home")
        Hawk.init(this@FragmentHome.requireContext()).build()
        val storedToken = PreferenceManager.getDefaultSharedPreferences(this.requireContext()).getString("token", "")
        val storedPermission = PreferenceManager.getDefaultSharedPreferences(this.requireContext()).getBoolean("granted", false)
        error { "StoredPermissionFragment: $storedPermission" }
        binding.loading.visibility = View.VISIBLE

        OkHttpClient.Builder()
            .addInterceptor { chain ->
                val request = chain.request()
                // Log o depuración de la URL antes de realizar la petición
                Log.e("Request URL:", "${request.url}")
                chain.proceed(request)
            }
            .build()



       /* viewModel.responseContentModel.observe(this@FragmentHome.viewLifecycleOwner) {response ->
            when (response){
                is NetworkResult.Success -> {
                    Log.e("FUNCIONA SUCCESS", "${response.data}")
                }

                is NetworkResult.Error -> {
                    Log.e("NO FUNCIONA ERROR", "${response.message}")
                }

                is NetworkResult.Loading -> {
                    Log.e("CARGANDO FUNCIONA", "${response}")
                }
            }
        }*/

        viewModel.responseContentModel.observe(this@FragmentHome.viewLifecycleOwner, Observer {
            items.clear()
            for (item in it.data ?: arrayListOf()){
                items.add(item)
            }
            viewModel.homeAdapter.updateList(items)
            binding.loading.visibility = View.GONE
            binding.swipeRefresh.isRefreshing = false
        })

        binding.swipeRefresh.setColorSchemeColors(resources.getColor(R.color.background_darkblue))
        binding.swipeRefresh.setOnRefreshListener {
            items.clear()
            binding.swipeRefresh.isRefreshing = true
            handleTime()
        }

        viewModel.homeAdapter.setClickAction {
            when(it){
                is ContentModel -> {
                    error { storedToken }
                    if (it.plus == 1 && storedToken != "" && storedPermission){
                        binding.loading.visibility = View.VISIBLE
                        val intent = Intent(this@FragmentHome.requireContext(), DetailsActivity::class.java)
                        intent.putExtra("cmsId", "${it.cms_id}")
                        startActivity(intent)
                    } else if (it.plus == 1 && !storedPermission){
                        val mDialogView =
                            LayoutInflater.from(this.requireContext())
                                .inflate(R.layout.alert_suscription, null)
                        val mBuilder = AlertDialog.Builder(this.requireContext())
                            .setView(mDialogView).create()
                        mBuilder.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                        val btnCerrar =
                            mDialogView.findViewById<Button>(R.id.btn_cerrar)

                        btnCerrar.setOnClickListener {
                            mBuilder.dismiss()
                        }
                        mBuilder.show()
                    } else {
                        binding.loading.visibility = View.VISIBLE
                        val intent = Intent(this@FragmentHome.requireContext(), DetailsActivity::class.java)
                        intent.putExtra("cmsId", "${it.cms_id}")
                        startActivity(intent)
                    }
                }
            }
        }

        return binding.root
    }

    fun handleInitLoading(){
        Handler().postDelayed({
            binding.loading.visibility = View.GONE
            binding.rvHome.isEnabled = true
        }, 9000)
    }

    fun handleTime(){
        Handler().postDelayed({
            binding.swipeRefresh.isRefreshing = false
            viewModel.doGetContent(seccion = "home")
            viewModel.homeAdapter.updateList(items)
        }, 2000)
    }

    override fun onResume() {
        super.onResume()
        handleInitLoading()
        binding.loading.visibility = View.GONE
    }
}