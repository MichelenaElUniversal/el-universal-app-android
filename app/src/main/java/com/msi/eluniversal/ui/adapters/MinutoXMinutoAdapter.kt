package com.msi.eluniversal.ui.adapters

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.msi.eluniversal.R
import com.msi.eluniversal.base.BaseViewModel
import com.msi.eluniversal.databinding.MinutoXMinutoItemBinding
import com.msi.eluniversal.models.MinutoXMinutoContentModel
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error

class MinutoXMinutoAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>(), AnkoLogger{
    private lateinit var items: MutableList<Any>
    private lateinit var bindingItem: MinutoXMinutoItemBinding
    private lateinit var context: Context

    private var listenerClick: ((item: Any) -> Unit)? = null

    @Suppress("unused")
    fun setClickAction(listenerClick: (item: Any) -> Unit){
        this.listenerClick = listenerClick
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        bindingItem = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.minuto_x_minuto_item,
            parent,
            false
        )
        return MinutoXMinutoItemViewHolder(bindingItem)
    }

    override fun getItemCount(): Int {
        return if (::items.isInitialized) items.size else 0
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.setIsRecyclable(false)
        when(holder){
            is MinutoXMinutoItemViewHolder -> {
                holder.bind(items[position] as MinutoXMinutoContentModel)
                if ((items[position] as MinutoXMinutoContentModel).plus == 1) {
                    bindingItem.plusIcon.visibility = View.VISIBLE
                }
                bindingItem.root.setOnClickListener { listenerClick?.invoke(items[position]) }
            }
        }
    }

    fun setContext(context: Context){
        this.context = context
    }

    fun updateList(items: List<Any>){
        this.items = items.toMutableList()
        notifyDataSetChanged()
    }

    class MinutoXMinutoItemViewHolder(private val binding: MinutoXMinutoItemBinding) : RecyclerView.ViewHolder(binding.root){
        private val viewModel = MinutoXMinutoItemViewModel()
        fun bind(item: MinutoXMinutoContentModel){
            binding.viewModel = viewModel
            viewModel.bind(item)
            when(item.section!!.name){
                "Nación" -> binding.tipoNoticia.setTextColor(Color.parseColor("#${item.section.color}"))
                "Estados" -> binding.tipoNoticia.setTextColor(Color.parseColor("#${item.section.color}"))
                "Metrópoli" -> binding.tipoNoticia.setTextColor(Color.parseColor("#${item.section.color}"))
                "Mundo" -> binding.tipoNoticia.setTextColor(Color.parseColor("#${item.section.color}"))
                "Cartera" -> binding.tipoNoticia.setTextColor(Color.parseColor("#${item.section.color}"))
                "Cultura" -> binding.tipoNoticia.setTextColor(Color.parseColor("#${item.section.color}"))
                "Destinos" -> binding.tipoNoticia.setTextColor(Color.parseColor("#${item.section.color}"))
                "Espectáculos" -> binding.tipoNoticia.setTextColor(Color.parseColor("#${item.section.color}"))
            }
        }
    }
}

class MinutoXMinutoItemViewModel: BaseViewModel(), AnkoLogger {
    val tituloNoticia: MutableLiveData<String?> = MutableLiveData()
    val tipoNoticia: MutableLiveData<String?> = MutableLiveData()
    val horaNoticia: MutableLiveData<String?> = MutableLiveData()

    fun bind(item: MinutoXMinutoContentModel){
        tituloNoticia.value = item.title
        tipoNoticia.value = item.section!!.name
        horaNoticia.value = item.hour_mxm
    }
}
