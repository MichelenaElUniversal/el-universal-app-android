package com.msi.eluniversal.ui.activity

import android.app.Activity
import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.ActivityInfo
import android.net.ConnectivityManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.view.MenuItem
import android.view.View
import androidx.activity.viewModels
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.ViewModelProvider
import com.msi.eluniversal.R
import com.msi.eluniversal.databinding.ActivityInfoBinding
import com.msi.eluniversal.ui.viewModel.GeneralViewModel
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.startActivity
import java.lang.Exception


@AndroidEntryPoint
class InfoActivity : AppCompatActivity(), AnkoLogger  {
    private lateinit var binding: ActivityInfoBinding
    private val viewModel by viewModels<GeneralViewModel>()

    private var broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val notConnected = intent.getBooleanExtra(
                ConnectivityManager
                    .EXTRA_NO_CONNECTIVITY, false
            )
            if (notConnected) {
                disconnected()
            } else {
                connected()
            }
        }
        private fun disconnected() {
            binding.fullscreenContent.visibility = View.GONE
            binding.noConnectivity.visibility = View.VISIBLE
        }

        private fun connected() {
            binding.noConnectivity.visibility = View.GONE
            binding.fullscreenContent.visibility = View.VISIBLE
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_info)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        binding.viewModel = viewModel


        binding.telegram.setOnClickListener {
            try {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse("telegram://ElUniversalOnline"))
                startActivity(intent)
            } catch (e: Exception){
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://t.me/ElUniversalOnline")))
            }
        }
        binding.facebook.setOnClickListener {
            try {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse("facebook://ElUniversalOnline"))
                startActivity(intent)
            } catch (e: Exception){
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/ElUniversalOnline")))
            }
        }
        binding.twitter.setOnClickListener {
            try {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse("twitter://El_Universal_Mx"))
                startActivity(intent)
            } catch (e: Exception){
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/El_Universal_Mx")))
            }
        }
        binding.youtube.setOnClickListener {
            try {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse("youtube://ElUniversalMex"))
                startActivity(intent)
            } catch (e: Exception){
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/ElUniversalMex")))
            }
        }
        binding.instagram.setOnClickListener {
            try {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse("instagram://eluniversalmx"))
                startActivity(intent)
            } catch (e: Exception){
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/eluniversalmx/")))
            }
        }
        binding.pinterest.setOnClickListener {
            try {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse("pinterest://eluniversalmx"))
                startActivity(intent)
            } catch (e: Exception){
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://www.pinterest.com.mx/eluniversalmx/")))
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun onStart() {
        super.onStart()
        registerReceiver(broadcastReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(broadcastReceiver)
    }
}