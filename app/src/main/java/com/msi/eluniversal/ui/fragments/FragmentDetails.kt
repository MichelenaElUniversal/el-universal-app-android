package com.msi.eluniversal.ui.fragments

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.google.android.gms.ads.admanager.AdManagerAdRequest
import com.google.android.gms.ads.admanager.AdManagerAdView
import com.msi.eluniversal.BuildConfig
import com.msi.eluniversal.R
import com.msi.eluniversal.databinding.FragmentDetailsBinding
import com.msi.eluniversal.ui.viewModel.GeneralViewModel
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.textSizeDimen

@AndroidEntryPoint
class FragmentDetails: Fragment(), AnkoLogger{

    private lateinit var binding: FragmentDetailsBinding
    private val viewModel by viewModels<GeneralViewModel>()
    private lateinit var mPublisherAdView: AdManagerAdView
    private var items: MutableList<Any> = mutableListOf()

    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_details, container, false)
        mPublisherAdView = binding.publisherAdView
        mPublisherAdView.loadAd(AdManagerAdRequest.Builder().build())

        binding.tituloNoticia.text = Hawk.get("tituloNoticia", "")
        binding.tipoNoticia.text = Hawk.get("tipoNoticia", "")
        binding.tipoNoticia.setTextColor(Color.parseColor("#${Hawk.get("colorNoticia", "")}"))
        binding.sumarioNoticia.text = Hawk.get("summaryNoticia", "")
        binding.fechaNoticia.text = Hawk.get("fechaNoticia", "").removeRange(10,16)
        binding.horaNoticia.text = Hawk.get("horaNoticia", "").removeRange(0, 11)
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            binding.bodyNoticia.text = Html.fromHtml(Hawk.get("bodyNoticia", ""), Html.FROM_HTML_MODE_COMPACT)
        }
        Glide.with(requireContext())
            .load(Hawk.get("imagenNoticia", ""))
            .into(binding.imagenNoticia)

        binding.share.onClick {
            val shareIntent = Intent(Intent.ACTION_SEND)
            shareIntent.setType("text/plain")
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "El Universal")
            val shareMessage = Hawk.get("urlNoticia", "") + BuildConfig.APPLICATION_ID + "\n\n"
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
            startActivity(Intent.createChooser(shareIntent, "Escoje tu red social"))
        }

        return binding.root
    }
}

