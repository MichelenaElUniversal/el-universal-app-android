package com.msi.eluniversal.ui.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.widget.ViewPager2
import com.msi.eluniversal.R
import com.msi.eluniversal.databinding.FragmentGalleryPagerBinding
import com.msi.eluniversal.ui.adapters.GallerySliderAdapter
import com.msi.eluniversal.ui.viewModel.GeneralViewModel
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint
import me.relex.circleindicator.CircleIndicator3
import org.jetbrains.anko.AnkoLogger

@AndroidEntryPoint
class FragmentGalleryPager: Fragment(), AnkoLogger {
    private lateinit var binding: FragmentGalleryPagerBinding
    private val viewModel by viewModels<GeneralViewModel>()
    private var items: MutableList<Any> = mutableListOf()

    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_gallery_pager, container, false)
        binding.viewModel = viewModel
        viewModel.gallerySliderAdapter.setContext(this@FragmentGalleryPager.requireContext())
        viewModel.doGetGalleryContent()
        binding.viewPager.orientation = ViewPager2.ORIENTATION_HORIZONTAL
        val viewPager: ViewPager2 = binding.viewPager
        viewPager.adapter = GallerySliderAdapter()

        viewModel.responseGalleryContentModel.observe(this@FragmentGalleryPager.viewLifecycleOwner, Observer {
            for (item in it.data ?: arrayListOf()){
                for (i in item.photos ?: arrayListOf()){
                    if (i!!.content_id == Hawk.get("idGallery", -1)){
                        items.add(i as Any)
                    }
                }
            }
            viewModel.gallerySliderAdapter.updateList(items)
            val indicator: CircleIndicator3 = binding.indicator
            indicator.setViewPager(viewPager)
        })
        return binding.root
    }
}