package com.msi.eluniversal.ui.activity

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.widget.ViewPager2
import com.msi.eluniversal.R
import com.msi.eluniversal.databinding.ActivityGalleryBinding
import com.msi.eluniversal.ui.adapters.GallerySliderAdapter
import com.msi.eluniversal.ui.viewModel.GeneralViewModel
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint
import me.relex.circleindicator.CircleIndicator3

@AndroidEntryPoint
class GalleryActivity : AppCompatActivity() {

    private val viewModel by viewModels<GeneralViewModel>()
    private lateinit var binding: ActivityGalleryBinding
    private var items: MutableList<Any> = mutableListOf()

    private var broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val notConnected = intent.getBooleanExtra(
                ConnectivityManager
                    .EXTRA_NO_CONNECTIVITY, false
            )
            if (notConnected) {
                disconnected()
            } else {
                connected()
            }
        }
        private fun disconnected() {
            binding.fullscreenContent.visibility = View.GONE
            binding.noConnectivity.visibility = View.VISIBLE
        }

        private fun connected() {
            binding.noConnectivity.visibility = View.GONE
            binding.fullscreenContent.visibility = View.VISIBLE

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_gallery)
        binding.viewModel = viewModel
        viewModel.gallerySliderAdapter.setContext(this)
        viewModel.doGetGalleryContent()
        binding.viewPager.orientation = ViewPager2.ORIENTATION_HORIZONTAL
        val viewPager: ViewPager2 = binding.viewPager
        viewPager.adapter = GallerySliderAdapter()

        viewModel.responseGalleryContentModel.observe(this, Observer {
            for (item in it.data ?: arrayListOf()){
                for (i in item.photos ?: arrayListOf()){
                    if (i!!.content_id == Hawk.get("idGallery", -1)){
                        items.add(i as Any)
                    }
                }
            }
            viewModel.gallerySliderAdapter.updateList(items)
            val indicator: CircleIndicator3 = binding.indicator
            indicator.setViewPager(viewPager)
        })

        viewModel.errorMessage.observe(this, Observer { message ->
            // Muestra el mensaje de error al usuario

        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun onStart() {
        super.onStart()
        registerReceiver(broadcastReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(broadcastReceiver)
    }
}