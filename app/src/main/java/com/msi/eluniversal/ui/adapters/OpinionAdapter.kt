package com.msi.eluniversal.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.ads.admanager.AdManagerAdRequest
import com.google.android.gms.ads.admanager.AdManagerAdView
import com.msi.eluniversal.R
import com.msi.eluniversal.base.BaseViewModel
import com.msi.eluniversal.databinding.AdSectionItemBinding
import com.msi.eluniversal.databinding.ItemOpinionBinding
import com.msi.eluniversal.models.OpinionContentModel
import org.jetbrains.anko.AnkoLogger

class OpinionAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>(), AnkoLogger {
    private lateinit var items: MutableList<Any>
    private lateinit var bindingItem: ItemOpinionBinding
    private lateinit var bindingAd: AdSectionItemBinding
    private lateinit var context: Context
    private lateinit var mPublisherAdView: AdManagerAdView

    private var listenerClick: ((item: Any) -> Unit)? = null

    @Suppress("unused")
    fun setClickAction(listenerClick: (item: Any) -> Unit){
        this.listenerClick = listenerClick
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when (viewType) {
            0 -> {
                bindingItem = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.item_opinion,
                    parent,
                    false
                )
                return OpinionItemViewHolder(bindingItem)
            }
            1 -> {
                bindingAd = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.ad_section_item,
                    parent,
                    false
                )
                return AdItemViewHolder(bindingAd)
            }
            else -> {
                bindingAd = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.ad_item,
                    parent,
                    false
                )
                return AdItemViewHolder(bindingAd)
            }
        }
    }

    override fun getItemCount(): Int {
        return if (::items.isInitialized) items.size + 1 else 0
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.setIsRecyclable(false)
        when(holder){
            is AdItemViewHolder -> {
                holder.bind("")
                mPublisherAdView = bindingAd.publisherAdView
                mPublisherAdView.loadAd(AdManagerAdRequest.Builder().build())
            }
            is OpinionItemViewHolder -> {
                holder.bind(items[position] as OpinionContentModel)
                bindingItem.root.setOnClickListener { listenerClick?.invoke(items[position]) }
                if ((items[position] as OpinionContentModel).plus == 1){
                    bindingItem.plusIcon.visibility = View.VISIBLE
                }

                val requestOptions = RequestOptions()
                requestOptions.placeholder(R.drawable.logo_el_universal)
                Glide.with(context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load((items[position] as OpinionContentModel).summary_author?.image)
                    .placeholder(R.drawable.logo_el_universal)
                    .centerInside()
                    .into(bindingItem.imageReporter)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        /*return if (position % 6 == 3){
            1
        } else {
            0
        }*/
        return if (position == items.size) 1 else 0
    }


    fun setContext(context: Context){
        this.context = context
    }

    fun updateList(items: List<Any>){
        this.items = items.toMutableList()
        notifyDataSetChanged()
    }

    class OpinionItemViewHolder(private val binding: ItemOpinionBinding) : RecyclerView.ViewHolder(binding.root){
        private val viewModel = OpinionItemViewModel()
        fun bind(item: OpinionContentModel) {
            binding.viewModel = viewModel
            viewModel.bind(item)
        }
    }

    class AdItemViewHolder(private val binding: AdSectionItemBinding) : RecyclerView.ViewHolder(binding.root){
        private val viewModel = OpinionItemViewModel()
        fun bind(item: String){
            viewModel.bind(item)
        }
    }
}

class OpinionItemViewModel: BaseViewModel(), AnkoLogger {
    val ad: MutableLiveData<String> = MutableLiveData()
    val tituloNoticia: MutableLiveData<String?> = MutableLiveData()
    val nombreReportero: MutableLiveData<String?> = MutableLiveData()

    fun bind(item: String){
        ad.value = item
    }

    fun bind(item: OpinionContentModel){
        tituloNoticia.value = item.title
        nombreReportero.value = item.summary_author?.title
    }
}