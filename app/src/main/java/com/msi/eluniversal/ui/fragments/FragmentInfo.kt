package com.msi.eluniversal.ui.fragments

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.msi.eluniversal.R
import com.msi.eluniversal.databinding.FragmentInfoBinding
import com.msi.eluniversal.ui.viewModel.GeneralViewModel
import dagger.hilt.android.AndroidEntryPoint
import org.jetbrains.anko.AnkoLogger
import java.lang.Exception

@AndroidEntryPoint
class FragmentInfo: Fragment(), AnkoLogger {

    private lateinit var binding: FragmentInfoBinding
    private val viewModel by viewModels<GeneralViewModel>()

    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_info, container, false)
        binding.viewModel = viewModel

        binding.telegram.setOnClickListener {
            try {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse("telegram://ElUniversalOnline"))
                startActivity(intent)
            } catch (e: Exception){
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://t.me/ElUniversalOnline")))
            }
        }
        binding.facebook.setOnClickListener {
            try {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse("facebook://ElUniversalOnline"))
                startActivity(intent)
            } catch (e: Exception){
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/ElUniversalOnline")))
            }
        }
        binding.twitter.setOnClickListener {
            try {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse("twitter://El_Universal_Mx"))
                startActivity(intent)
            } catch (e: Exception){
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/El_Universal_Mx")))
            }
        }
        binding.youtube.setOnClickListener {
            try {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse("youtube://ElUniversalMex"))
                startActivity(intent)
            } catch (e: Exception){
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/ElUniversalMex")))
            }
        }
        binding.instagram.setOnClickListener {
            try {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse("instagram://eluniversalmx"))
                startActivity(intent)
            } catch (e: Exception){
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/eluniversalmx/")))
            }
        }
        binding.pinterest.setOnClickListener {
            try {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse("pinterest://eluniversalmx"))
                startActivity(intent)
            } catch (e: Exception){
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://www.pinterest.com.mx/eluniversalmx/")))
            }
        }

        return binding.root
    }
}