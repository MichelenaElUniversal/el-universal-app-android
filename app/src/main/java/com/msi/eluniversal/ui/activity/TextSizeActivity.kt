package com.msi.eluniversal.ui.activity

import android.annotation.SuppressLint
import android.content.*
import android.content.pm.ActivityInfo
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.View
import android.widget.SeekBar
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.msi.eluniversal.R
import com.msi.eluniversal.databinding.ActivityTextSizeBinding
import com.msi.eluniversal.ui.viewModel.GeneralViewModel
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint
import org.jetbrains.anko.AnkoLogger

@Suppress("DEPRECATION")
@AndroidEntryPoint
class TextSizeActivity: AppCompatActivity(), AnkoLogger {
    private lateinit var binding: ActivityTextSizeBinding
    private val viewModel by viewModels<GeneralViewModel>()

    private var broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val notConnected = intent.getBooleanExtra(
                ConnectivityManager
                    .EXTRA_NO_CONNECTIVITY, false
            )
            if (notConnected) {
                disconnected()
            } else {
                connected()
            }
        }
        private fun disconnected() {
            binding.fullscreenContent.visibility = View.GONE
            binding.noConnectivity.visibility = View.VISIBLE
        }

        private fun connected() {
            binding.noConnectivity.visibility = View.GONE
            binding.fullscreenContent.visibility = View.VISIBLE
        }
    }

    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_text_size)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_text_size)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        binding.viewModel = viewModel

        //SeekBar

        binding.textSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                binding.textSizeBody.textSize = progress.toFloat()
                Hawk.put("textSize", progress)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
                binding.textSizeBody.textSize = 16F
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }
        })
    }

    override fun onStart() {
        super.onStart()
        registerReceiver(broadcastReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(broadcastReceiver)
    }

    override fun onResume() {
        super.onResume()
        binding.loading.visibility = View.GONE
        binding.textSeekBar.progress = Hawk.get("textSize", 16)
    }
}
