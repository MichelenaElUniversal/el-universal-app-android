package com.msi.eluniversal.ui.adapters

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat.startActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.msi.eluniversal.R
import com.msi.eluniversal.base.BaseViewModel
import com.msi.eluniversal.databinding.GaleriaItemBinding
import com.msi.eluniversal.models.GalleryContentModel
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.sdk27.coroutines.onClick

class GalleryAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>(), AnkoLogger {
    private lateinit var items: MutableList<Any>
    private lateinit var bindingItem: GaleriaItemBinding
    private lateinit var context: Context

    private var listenerClick: ((item: Any) -> Unit)? = null

    @Suppress("unused")
    fun setClickAction(listenerClick: (item: Any) -> Unit){
        this.listenerClick = listenerClick
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        bindingItem = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.galeria_item,
            parent,
            false
        )
        return GalleryItemViewHolder(bindingItem)
    }

    override fun getItemCount(): Int {
        return if (::items.isInitialized) items.size else 0
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.setIsRecyclable(false)
        when(holder){
            is GalleryItemViewHolder -> {
                holder.bind(items[position] as GalleryContentModel)
                bindingItem.root.setOnClickListener { listenerClick?.invoke(items[position]) }
                bindingItem.btnShare.onClick {
                    val shareIntent = Intent(Intent.ACTION_SEND)
                    shareIntent.setType("text/plain")
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "El Universal - Galería")
                    val shareMessage = (items[position] as GalleryContentModel).url + "\n\n"
                    shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                    startActivity(context, Intent.createChooser(shareIntent, "Escoje tu red social"), Bundle.EMPTY)
                }
                val requestOptions = RequestOptions()
                requestOptions.placeholder(R.drawable.logo_el_universal)
                Glide.with(context)
                    .applyDefaultRequestOptions(requestOptions)
                    .load((items[position] as GalleryContentModel).urlGallery)
                    .centerCrop()
                    .into(bindingItem.imagenGaleria)
            }
        }
    }

    fun setContext(context: Context){
        this.context = context
    }

    fun updateList(items: List<Any>){
        this.items = items.toMutableList()
        notifyDataSetChanged()
    }

    class GalleryItemViewHolder(private val binding: GaleriaItemBinding): RecyclerView.ViewHolder(binding.root){
        private val viewModel = GalleryItemViewModel()
        fun bind(item: GalleryContentModel){
            binding.viewModel = viewModel
            viewModel.bind(item)
        }
    }
}

class GalleryItemViewModel: BaseViewModel(), AnkoLogger{
    val tituloGaleria: MutableLiveData<String?> = MutableLiveData()
    val fechaGaleria: MutableLiveData<String> = MutableLiveData()

    fun bind(item: GalleryContentModel){
        tituloGaleria.value = item.title
        fechaGaleria.value = item.publication_date!!.removeRange(10,16)
    }
}