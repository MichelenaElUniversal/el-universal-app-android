package com.msi.eluniversal.ui.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.ActivityInfo
import android.net.ConnectivityManager
import android.os.Bundle
import android.os.Handler
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.msi.eluniversal.R
import com.msi.eluniversal.databinding.ActivitySearchBinding
import com.msi.eluniversal.models.ContentModel
import com.msi.eluniversal.ui.viewModel.GeneralViewModel
import com.orhanobut.hawk.Hawk
import dagger.hilt.android.AndroidEntryPoint

import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error
import org.jetbrains.anko.sdk27.coroutines.onClick
import org.jetbrains.anko.startActivity
import java.util.*

@AndroidEntryPoint
class SearchActivity : AppCompatActivity(), AnkoLogger {

    private lateinit var binding: ActivitySearchBinding
    private val viewModel by viewModels<GeneralViewModel>()
    private var items: MutableList<Any> = mutableListOf()

    private var broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val notConnected = intent.getBooleanExtra(
                ConnectivityManager
                    .EXTRA_NO_CONNECTIVITY, false
            )
            if (notConnected) {
                disconnected()
            } else {
                connected()
            }
        }
        private fun disconnected() {
            binding.fullscreenContent.visibility = View.GONE
            binding.noConnectivity.visibility = View.VISIBLE
        }

        private fun connected() {
            binding.noConnectivity.visibility = View.GONE
            binding.fullscreenContent.visibility = View.VISIBLE
        }
    }

    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        binding.viewModel = viewModel
        binding.rvSearch.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        //viewModel.doGetContent(seccion = Hawk.get("idSection", ""))
        viewModel.homeAdapter.setContext(this)

        binding.btnSearch.onClick {
            binding.errorSearch.visibility = View.GONE
            items.clear()
            Hawk.put("inputText", binding.searchEditText.text.toString())
            hideKeyboard(this@SearchActivity)
            if (Hawk.get("inputText", "") != ""){
                viewModel.doGetSearchContent(term = Hawk.get("inputText", ""))
                binding.loading.visibility = View.VISIBLE
                Handler().postDelayed({
                    viewModel.responseContentModel.observe(this@SearchActivity, Observer {
                        for (item in it.data ?: arrayListOf()){
                            items.add(item)
                        }
                        viewModel.homeAdapter.updateList(items)
                        binding.loading.visibility = View.GONE
                        error { "Item count: ${viewModel.homeAdapter.itemCount}" }
                        error { "Item size: ${items.size}" }
                        /*if (items.size == 0){
                            items.clear()

                        } else {
                            binding.errorSearch.visibility = View.GONE
                        }
                        binding.loading.visibility = View.GONE*/
                    })
                }, 2000)
            } else {
                binding.loading.visibility = View.GONE
                binding.errorSearch.visibility = View.VISIBLE
            }
        }

        fun EditText.onSubmit(func: () -> Unit) {
            setOnEditorActionListener { _, actionId, _ ->

                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    func()
                }
                true
            }
        }

        binding.swipeRefresh.setOnRefreshListener {
            items.clear()
            binding.swipeRefresh.isRefreshing = true
            handleTime()
        }

        binding.searchEditText.onSubmit {
            binding.errorSearch.visibility = View.GONE
            items.clear()
            Hawk.put("inputText", binding.searchEditText.text.toString())
            hideKeyboard(this@SearchActivity)
            if (Hawk.get("inputText", "") != ""){
                viewModel.doGetSearchContent(term = Hawk.get("inputText", ""))
                binding.loading.visibility = View.VISIBLE
                Handler().postDelayed({
                    viewModel.responseContentModel.observe(this@SearchActivity, Observer {
                        for (item in it.data ?: arrayListOf()){
                            items.add(item)
                        }
                        viewModel.homeAdapter.updateList(items)
                        binding.loading.visibility = View.GONE
                        error { "Item count: ${viewModel.homeAdapter.itemCount}" }
                        error { "Item size: ${items.size}" }
                        /*if (items.size == 0){
                            items.clear()

                        } else {
                            binding.errorSearch.visibility = View.GONE
                        }
                        binding.loading.visibility = View.GONE*/
                    })
                }, 2000)
            } else {
                binding.loading.visibility = View.GONE
                binding.errorSearch.visibility = View.VISIBLE
            }
        }

        viewModel.homeAdapter.setClickAction {
            when(it){
                is ContentModel -> {
                    binding.loading.visibility = View.VISIBLE
                    val intent = Intent(this, DetailsActivity::class.java)
                    intent.putExtra("cmsId", "${it.cms_id}")
                    startActivity(intent)
                }
            }
        }

        /*viewModel.errorMessage.observe(this, Observer {
            if (it != null){
                items.clear()
                binding.errorSearch.visibility = View.VISIBLE
            }
        })*/
    }

    fun handleTime(){
        Handler().postDelayed({
            binding.swipeRefresh.isRefreshing = false
            viewModel.doGetSearchContent(term = Hawk.get("inputText", ""))
            viewModel.homeAdapter.updateList(items)
        }, 2000)
    }

    fun hideKeyboard(activity: SearchActivity) {
        val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view = activity.currentFocus
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun onStart() {
        super.onStart()
        registerReceiver(broadcastReceiver, IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(broadcastReceiver)
    }
}