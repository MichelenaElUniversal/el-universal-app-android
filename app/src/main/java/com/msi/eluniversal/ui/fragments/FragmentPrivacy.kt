package com.msi.eluniversal.ui.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.msi.eluniversal.R
import com.msi.eluniversal.databinding.FragmentPrivacyBinding
import com.msi.eluniversal.ui.viewModel.GeneralViewModel
import dagger.hilt.android.AndroidEntryPoint
import org.jetbrains.anko.AnkoLogger

@AndroidEntryPoint
class FragmentPrivacy: Fragment(), AnkoLogger {
    private lateinit var binding: FragmentPrivacyBinding
    private val viewModel by viewModels<GeneralViewModel>()

    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_privacy, container, false)
        binding.viewModel = viewModel

        return binding.root
    }
}