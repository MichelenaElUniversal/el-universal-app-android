package com.msi.eluniversal.ui.fragments

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.youtube.player.YouTubePlayer
import com.google.android.youtube.player.YouTubePlayerView
import com.msi.eluniversal.R
import com.msi.eluniversal.databinding.FragmentVideoBinding
import com.msi.eluniversal.models.VideoContentModel
import com.msi.eluniversal.ui.viewModel.GeneralViewModel
import com.google.android.youtube.player.YouTubeStandalonePlayer
import com.msi.eluniversal.utils.Constants
import com.orhanobut.hawk.Hawk
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.PlayerConstants
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.FullscreenListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerListener
import dagger.hilt.android.AndroidEntryPoint
import org.jetbrains.anko.AnkoLogger

@AndroidEntryPoint
class FragmentVideo: Fragment(), AnkoLogger{

    private lateinit var binding: FragmentVideoBinding
    private val viewModel by viewModels<GeneralViewModel>()
    private var items: MutableList<Any> = mutableListOf()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_video, container, false)
        binding.viewModel = viewModel
        viewModel.videoAdapter.setContext(this.requireContext())
        binding.rvVideo.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        binding.contentError.visibility = View.GONE
        viewModel.doGetVideoContent(seccion = Hawk.get("idSeccion", ""))
        Hawk.init(this@FragmentVideo.requireContext()).build()
        val youtubeContainer = activity?.findViewById<FrameLayout>(R.id.youtubeVideoContainer)

        viewModel.responseVideoFeaturedContentModel.observe(this.viewLifecycleOwner, Observer {
                for (item in it.data ?: arrayListOf()) {
                    items.add(item)
                }
                viewModel.videoAdapter.updateList(items)
        })
        viewModel.videoAdapter.setClickAction {
            when(it){
                is VideoContentModel -> {
                    val youtubeId = it.id_youtube
                    Hawk.put("youtubePlayer", youtubeId)
                    val youtubeView =
                        com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView(
                            this@FragmentVideo.requireContext()
                        )
                    viewLifecycleOwner.lifecycle.addObserver(youtubeView)
                    youtubeContainer?.addView(youtubeView)

                    youtubeView.matchParent()
                    youtubeView.visibility = View.GONE
                    youtubeView.enableAutomaticInitialization = false
                    youtubeContainer?.visibility = View.VISIBLE
                    val listener: YouTubePlayerListener = object : AbstractYouTubePlayerListener(){
                        override fun onReady(youTubePlayer: com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer) {
                            youTubePlayer.loadVideo(youtubeId!!, 0f)
                            youTubePlayer.play()
                            activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
                        }


                        override fun onStateChange(
                            youTubePlayer: com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer,
                            state: PlayerConstants.PlayerState
                        ) {
                            if (state == PlayerConstants.PlayerState.ENDED) {
                                youtubeContainer?.visibility = View.GONE
                                activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                                youtubeContainer?.removeAllViews()
                            } else if (state == PlayerConstants.PlayerState.PLAYING) {
                                youTubePlayer.toggleFullscreen()
                                activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR
                            }
                        }
                    }



                    val fullscreenListener = object : FullscreenListener {
                        override fun onEnterFullscreen(
                            fullscreenView: View,
                            exitFullscreen: () -> Unit
                        ) {
                            youtubeContainer?.addView(fullscreenView)
                        }

                        override fun onExitFullscreen() {
                            youtubeView.release()
                            youtubeContainer?.removeAllViews()
                            youtubeContainer?.visibility = View.GONE
                            activity?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                        }
                    }



                    youtubeView.addFullscreenListener(fullscreenListener)
                    youtubeView.initialize(listener)

                    // BACK

                    val onBackPressedCallback = object : OnBackPressedCallback(true) {
                        override fun handleOnBackPressed() {
                            youtubeContainer?.visibility = View.GONE
                            youtubeView.release()
                            youtubeContainer?.removeAllViews()
                        }
                    }

                    requireActivity().onBackPressedDispatcher.addCallback(
                        this@FragmentVideo.viewLifecycleOwner,
                        onBackPressedCallback
                    )


                }
            }
        }
        return binding.root
    }
}