package com.msi.eluniversal.ui.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.msi.eluniversal.injector.network.bases.NetworkResult
import com.msi.eluniversal.injector.network.repository.ElUniversalRepository
import com.msi.eluniversal.models.AdModel
import com.msi.eluniversal.models.ContentModel
import com.msi.eluniversal.models.FavoriteContentsModel
import com.msi.eluniversal.models.GalleryContentModel
import com.msi.eluniversal.models.MinutoXMinutoContentModel
import com.msi.eluniversal.models.OpinionContentModel
import com.msi.eluniversal.models.PushNotificationContentModel
import com.msi.eluniversal.models.SaveFavoritesResponse
import com.msi.eluniversal.models.SectionsModel
import com.msi.eluniversal.models.VideoContentModel
import com.msi.eluniversal.ui.adapters.GalleryAdapter
import com.msi.eluniversal.ui.adapters.GallerySliderAdapter
import com.msi.eluniversal.ui.adapters.HomeAdapter
import com.msi.eluniversal.ui.adapters.MiCuentaAdapter
import com.msi.eluniversal.ui.adapters.MinutoXMinutoAdapter
import com.msi.eluniversal.ui.adapters.OpinionAdapter
import com.msi.eluniversal.ui.adapters.SectionAdapter
import com.msi.eluniversal.ui.adapters.VideoAdapter
import com.msi.eluniversal.ui.adapters.VideoMenuAdapter
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import okhttp3.RequestBody
import java.lang.Exception
import javax.inject.Inject

@HiltViewModel
class GeneralViewModel @Inject constructor(
    private val repository: ElUniversalRepository,
    application: Application
): AndroidViewModel(application) {

    val isLoading = MutableLiveData<Boolean>()
    val errorMessage: MutableLiveData<String> = MutableLiveData()

    /** SECTION */
    val responseSectionsModel: MutableLiveData<NetworkResult<List<SectionsModel>>> =
        MutableLiveData()

    fun doGetSections() = viewModelScope.launch {
        isLoading.value = true
        try {
            repository.getSections().collect() { values ->
                responseSectionsModel.value = values
            }
        } catch (e: Exception) {
            errorMessage.value = "Error al obtener los datos: ${e.message}"
        } finally {
            isLoading.value = false
        }
    }


    val responseVideoSectionsModel: MutableLiveData<NetworkResult<List<SectionsModel>>> =
        MutableLiveData()
    val sectionAdapter: SectionAdapter = SectionAdapter()

    fun doGetVideoSections() = viewModelScope.launch {
        isLoading.value = true
        try {
            repository.getVideoSections().collect() { values ->
                responseVideoSectionsModel.value = values
            }
        } catch (e: Exception) {
            errorMessage.value = "Error al obtener los datos: ${e.message}"
        } finally {
            isLoading.value = false
        }
    }

    /** CONTENT */

    val responseContentModel: MutableLiveData<NetworkResult<List<ContentModel>>> = MutableLiveData()
    val homeAdapter: HomeAdapter = HomeAdapter()

    fun doGetContent(seccion: String) = viewModelScope.launch {
        isLoading.value = true
        try {
            repository.getContent(seccion).collect() { values ->
                responseContentModel.value = values
            }
        } catch (e: Exception) {
            errorMessage.value = "Error al obtener los datos: ${e.message}"
        } finally {
            isLoading.value = false
        }

    }

    /** DETAILS */



    val responsePushContentModel: MutableLiveData<NetworkResult<PushNotificationContentModel>> =
        MutableLiveData()

    fun doGetPushDetails(cms_id: String) = viewModelScope.launch {
        isLoading.value = true
        try {
            repository.getDetails(cms_id).collect() { values ->
                responsePushContentModel.value = values
            }
        } catch(e: Exception) {
            errorMessage.value = "Error al obtener los datos: ${e.message}"
        } finally {
            isLoading.value = false
        }


    }

    /** MXM */

    val responseMinutoXMinutoContentModel: MutableLiveData<NetworkResult<List<MinutoXMinutoContentModel>>> =
        MutableLiveData()
    val minutoXMinutoAdapter: MinutoXMinutoAdapter = MinutoXMinutoAdapter()

    fun doGetMinutoXMinutoContent() = viewModelScope.launch {
        isLoading.value = true
        try {
            repository.getMxm().collect() { values ->
                responseMinutoXMinutoContentModel.value = values
            }
        } catch(e: Exception) {
            errorMessage.value = "Error al obtener los datos: ${e.message}"
        } finally {
            isLoading.value = false
        }
    }

    /** OPINION */

    val responseOpinionContentModel: MutableLiveData<NetworkResult<List<OpinionContentModel>>> =
        MutableLiveData()
    val opinionAdapter: OpinionAdapter = OpinionAdapter()

    fun doGetOpinion() = viewModelScope.launch {
        isLoading.value = true
        try {
            repository.getOpinion().collect() { values ->
                responseOpinionContentModel.value = values
            }
        } catch(e: Exception) {
            errorMessage.value = "Error al obtener los datos: ${e.message}"
        } finally {
            isLoading.value = false
        }
    }

    /** VIDEOS */

    val responseVideoFeaturedContentModel: MutableLiveData<NetworkResult<List<VideoContentModel>>> =
        MutableLiveData()
    val videoAdapter: VideoAdapter = VideoAdapter()
    val videoMenuAdapter: VideoMenuAdapter = VideoMenuAdapter()

    fun doGetFeaturedVideoContent() = viewModelScope.launch {

        isLoading.value = true
        try {
            repository.getFeatured().collect() { values ->
                responseVideoFeaturedContentModel.value = values
            }
        } catch(e: Exception) {
            errorMessage.value = "Error al obtener los datos: ${e.message}"
        } finally {
            isLoading.value = false
        }


    }

    fun doGetVideoContent(seccion: String) = viewModelScope.launch {

        isLoading.value = true
        try {
            repository.getVideos(seccion).collect() { values ->
                responseVideoFeaturedContentModel.value = values
            }
        } catch(e: Exception) {
            errorMessage.value = "Error al obtener los datos: ${e.message}"
        } finally {
            isLoading.value = false
        }


    }


    /** GALERÍA */

    val responseGalleryContentModel: MutableLiveData<NetworkResult<List<GalleryContentModel>>> =
        MutableLiveData()
    val galleryAdapter: GalleryAdapter = GalleryAdapter()
    val gallerySliderAdapter: GallerySliderAdapter = GallerySliderAdapter()

    fun doGetGalleryContent() = viewModelScope.launch {

        isLoading.value = true
        try {
            repository.getGallery().collect() { values ->
                responseGalleryContentModel.value = values
            }
        } catch(e: Exception) {
            errorMessage.value = "Error al obtener los datos: ${e.message}"
        } finally {
            isLoading.value = false
        }


    }

    /** SEARCH */

    fun doGetSearchContent(term: String) = viewModelScope.launch {

        isLoading.value = true
        try {
            repository.getSearch(term).collect() { values ->
                responseContentModel.value = values
            }
        } catch(e: Exception) {
            errorMessage.value = "Error al obtener los datos: ${e.message}"
        } finally {
            isLoading.value = false
        }


    }


    /** ADS */

    val responseAdModel: MutableLiveData<NetworkResult<AdModel>> = MutableLiveData()

    fun doAdContent() = viewModelScope.launch {

        isLoading.value = true
        try {
            repository.getAdContent().collect() { values ->
                responseAdModel.value = values
            }
        } catch(e: Exception) {
            errorMessage.value = "Error al obtener los datos: ${e.message}"
        } finally {
            isLoading.value = false
        }


    }

    /** FAVORITES */

    val miCuentaAdapter: MiCuentaAdapter = MiCuentaAdapter()

    val responseFavoriteModel: MutableLiveData<NetworkResult<List<FavoriteContentsModel>>> =
        MutableLiveData()
    val responseSaveFavoriteModel: MutableLiveData<NetworkResult<SaveFavoritesResponse>> =
        MutableLiveData()

    fun doGetFavorites(requestBody: RequestBody) = viewModelScope.launch {
        isLoading.value = true
        try {
            repository.getFavorites(requestBody).collect() { values ->
                responseFavoriteModel.value = values
            }
        } catch(e: Exception) {
            errorMessage.value = "Error al obtener los datos: ${e.message}"
        } finally {
            isLoading.value = false
        }


    }

    fun postFavorite(requestBody: RequestBody) = viewModelScope.launch {
        isLoading.value = true
        try {
            repository.saveNewFavorite(requestBody).collect() { values ->
                responseSaveFavoriteModel.value = values
            }
        } catch(e: Exception) {
            errorMessage.value = "Error al obtener los datos: ${e.message}"
        } finally {
            isLoading.value = false
        }
    }
}


